//
//  BaseView.swift
//  HCP Adult
//
//  Created by Bình Phạm on 7/6/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import Foundation

protocol BaseView : NSObjectProtocol {
    func showLoading(_ message : String)
    func hideLoading()
}
