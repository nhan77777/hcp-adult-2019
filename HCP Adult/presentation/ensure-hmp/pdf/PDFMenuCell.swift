//
//  PDFMenuCell.swift
//  EnsureHMP
//
//  Created by Bình Phạm on 5/3/18.
//

import UIKit

class PDFMenuCell: UICollectionViewCell {
    
    @IBOutlet weak var widthContraint: NSLayoutConstraint!
    @IBOutlet weak var lbTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.translatesAutoresizingMaskIntoConstraints = false
        widthContraint.constant = 118
    }
    
}
