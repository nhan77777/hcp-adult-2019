//
//  EnsurePage6VC.swift
//  HCP Adult
//
//  Created by Le Nhan on 1/7/19.
//  Copyright © 2019 Bình Phạm. All rights reserved.
//

import UIKit
import Async

class EnsurePage6VC: BaseDetailVC, DetailView {
    
    @IBOutlet var imgIcons: [UIImageView]!
    
    @IBOutlet weak var imgBg: UIImageView!
    
    func loadImages() {
        loadResourceToImage("ensure_bg_page_6", imgBg)
    }
    
    func resetAnimation() {
        for ic in imgIcons {
            ic.layer.removeAllAnimations()
        }
    }
    
    func startAnimation() {
        for ic in imgIcons {
            ic.twinkle()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadImages()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        resetAnimation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Async.main(after: 0.1, {
            self.startAnimation()
        })
    }

}
