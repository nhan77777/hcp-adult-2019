//
//  Page2VC.swift
//  Glucerna
//
//  Created by Long Quách Phi on 3/22/18.
//  Copyright © 2018 Fractal. All rights reserved.
//

import UIKit

class Page8VC: BaseDetailVC, DetailView {
    
    @IBOutlet weak var imgBack: UIImageView!
    @IBOutlet weak var imgTopBar: UIImageView!
    @IBOutlet var viewTwinkle: [UIView]!
    @IBOutlet weak var imgBackground: UIImageView!
    var isHiden = true
    
    func loadImages() {
        loadResourceToImage("glu_bg_page8", imgBackground)
        loadResourceToImage("glu_ic_page8", imgTopBar)
    }
    
    func resetAnimation() {
    }
    
    func startAnimation() {
        viewTwinkle[0].twinkle()
        viewTwinkle[1].twinkle()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pageID = "page37"
        loadImages()
        setupAction()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        imgBack.isHidden = isHiden
        resetAnimation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        startAnimation()
    }
    
    func setupAction(){
        imgBack.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onBtnBack(_:))))
        imgBack.isUserInteractionEnabled = true
    }

    @objc func onBtnBack(_ sender : UITapGestureRecognizer){
        dismiss(animated: true, completion: nil)
    }
    
}

