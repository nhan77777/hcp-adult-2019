//
//  SurveyRecord.swift
//  HCP Adult
//
//  Created by Bình Phạm on 7/16/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import Foundation
import EVReflection

class SurveyRecord : EVObject {
    public var clientid: String?
    public var hcpid: String?
    public var appid: String?
    public var usersysid: String?
    public var consent: [String]?
    public var feedback: [FeedbackRecord]?
    public var createAt: String?
    public var appver: String?
    public var signatureUrl: String?
    public var startTick : Int = 0
    
}
