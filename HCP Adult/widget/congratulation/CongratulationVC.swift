//
//  CongratulationVC.swift
//  HCP Adult
//
//  Created by Le Nhan on 1/11/19.
//  Copyright © 2019 Bình Phạm. All rights reserved.
//

import UIKit
import Async

class CongratulationVC: UIViewController {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var lbMessage: UILabel!
    @IBOutlet weak var imgFireCracker: UIImageView!
    
    private var fountainFireWork = FountainFireworkController()
    private var basicFireWork = ClassicFireworkController()
    var callBack: (() ->())!
    
    var isCorrect = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        contentView.layer.cornerRadius = contentView.frame.height / 6
        contentView.layer.borderWidth = 3
        contentView.layer.borderColor = UIColor.orange.cgColor
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if isCorrect {
            
            imgFireCracker.image = UIImage(named: "firecracker")
            lbMessage.text = "Chúc mừng bạn đã trả lời đúng!"
            
        } else {
            
            imgFireCracker.image = UIImage(named: "sad")
            lbMessage.text = "Bạn trả lời chưa chính xác!"
        }
    }
    
    func addFirework(){
        fountainFireWork.addFirework (sparks: 20, above: imgFireCracker, sparkSize: CGSize(width: 9, height: 9), scale: 51, offsetY: 0, animationDuration: 1)
        
        basicFireWork.addFireworks(count: 2, sparks: 8, around: imgFireCracker, sparkSize: CGSize(width: 9, height: 9), scale: 40, maxVectorChange: 30, animationDuration: 0.5, canChangeZIndex: true)
        
        basicFireWork.addFireworks(count: 3, sparks: 8, around: lbMessage, sparkSize: CGSize(width: 9, height: 9), scale: 40, maxVectorChange: 30, animationDuration: 0.5, canChangeZIndex: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if isCorrect {
            
            for index in 0...1 {
                if index == 1 {
                    DispatchQueue.main.asyncAfter(deadline: .now() + Double(index)) {
                        self.dismiss(animated: true, completion: {
                            self.callBack()
                        })
                    }
                } else {
                    DispatchQueue.main.asyncAfter(deadline: .now() + Double(index)) {
                        self.addFirework()
                    }
                }
            }
            
        } else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.dismiss(animated: true, completion: {
                    self.callBack()
                })
            }
        }
        
    }
}
