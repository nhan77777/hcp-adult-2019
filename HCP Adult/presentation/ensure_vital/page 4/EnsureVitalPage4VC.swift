//
//  EnsureVitalPage4VC.swift
//  HCP Adult
//
//  Created by Long Quách Phi on 11/23/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import UIKit
import Async

class EnsureVitalPage4VC: BaseDetailVC, DetailView {
    
    @IBOutlet var imgTitleCollection: [UIImageView]!
    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var icMilk: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadImages()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        resetAnimation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Async.main(after: 0.3){
            self.startAnimation()
        }
    }
    
    func loadImages() {
        loadResourceToImage("bg_vital_page_4", imgBG)
        loadResourceToImage("ic_vital_page_4", icMilk)
        for icon in imgTitleCollection {
            switch icon.tag {
            case 1: loadResourceToImage("ic_vital_title_1", icon)
            case 2: loadResourceToImage("ic_vital_title_2", icon)
            case 3: loadResourceToImage("ic_vital_title_3", icon)
            case 4: loadResourceToImage("ic_vital_title_4", icon)
            default:
                loadResourceToImage("ic_vital_title_1", icon)
            }
        }
    }
    
    func resetAnimation() {
        icMilk.layer.removeAllAnimations()
    }
    
    func startAnimation() {
        icMilk.twinkle()
        boucingViewForever(icMilk, 0.8)
        for image in imgTitleCollection {
            image.startGlowingWithColor(color: UIColor(hexString: "#f38dff")!, intensity: 0.5)
        }
    }
    
}
