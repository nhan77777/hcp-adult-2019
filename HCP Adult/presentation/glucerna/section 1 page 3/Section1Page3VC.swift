//
//  Section1Page3VC.swift
//  HCP Adult
//
//  Created by Long Quách Phi on 6/8/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import UIKit

class Section1Page3VC: BaseDetailVC, DetailView {
    
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet var imgIC: [UIImageView]!
    @IBOutlet var viewFooter: [UIView]!
    
    func loadImages() {
        loadResourceToImage("glu_bg_section1_page3", imgBackground)
    }
    
    func resetAnimation() {
        for i in self.imgIC{
            showHightlight(content: i, color: UIColor.clear, radius: 0)
        }
    }
    
    func startAnimation() {
        for i in self.imgIC{
            showHightlight(content: i, color: UIColor.yellow, radius: 5)
        }
        UIView.animate(withDuration: 0.65, delay: 0.0, options: [.repeat, .autoreverse], animations: {
            for i in self.imgIC{
                i.layer.opacity = 0.7
            }
        }, completion: nil)
        /*
        UIView.animateAndChain(withDuration: 0.5, delay: 0.0, options: [], animations: {
            for i in self.imgIC{
                self.showHightlight(content: i, color: UIColor.yellow, radius: 3)
            }
        }, completion: nil).animateAndChain(withDuration: 0.5, delay: 0.0, options: [], animations: {
            for i in self.imgIC{
                self.showHightlight(content: i, color: UIColor.clear, radius: 0)
            }
        }, completion: nil)*/
        
    }
    
    func setupActionFooter(){
        for i in 0...viewFooter.count - 1{
            viewFooter[i].addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onViewTapFooter(_:))))
            viewFooter[i].isUserInteractionEnabled = true
        }
    }
    
    @objc func onViewTapFooter(_ sender : UITapGestureRecognizer){
        showPDFView(filename: "glu_2")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pageID = "page28"
        loadImages()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        resetAnimation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        startAnimation()
    }
    
}

