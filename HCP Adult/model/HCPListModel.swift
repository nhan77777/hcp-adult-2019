import Foundation
import ObjectMapper

public struct HCPListModel: Mappable {
    
    private struct SerializationKeys {
        static let message = "hcp"
    }
    
    public var data: [HCPModel]?
    
    public init?(map: Map){
        
    }
    
    public mutating func mapping(map: Map) {
        data <- map[SerializationKeys.message]
    }
    
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = data { dictionary[SerializationKeys.message] = value.map { $0.dictionaryRepresentation() } }
        return dictionary
    }
    
}
