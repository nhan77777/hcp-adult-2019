//
//  ChoiceHCPView.swift
//  HCP Adult
//
//  Created by Bình Phạm on 6/6/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import Foundation
protocol ChoiceHCPView : BaseView {
    func onGetHCP(_ data : [HCPModel])
    func onGetHCPFailed(_ message : String)
    func pushDurationSuccess(_ data: ReturnDurationModel)
    func pushDurationFailed(_ message : String)
    func pushHCPSurveySuccess(_ data: ReturnDurationModel)
    func pushHCPSurveyFailed(_ message : String)
    func checkVersionSuccess(_ data: ResponseCheckVersion)
    func checkVersionFailed(_ message : String)
}
