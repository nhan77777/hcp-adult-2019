//
//  Utils.swift
//  SimilacNVE
//
//  Created by Bình Phạm on 11/20/17.
//  Copyright © 2017 Fractal. All rights reserved.
//

import Foundation
import UIKit
import AssetsLibrary

public class CacheKey {
    public static let userdata = "userdata"
    public static let selectedHCP = "selectedHCP"
    public static let baseUrl = "baseUrl"
    public static let DURATION_RECORD = "duration_record"
    public static let HCP_IC  = "id"
    public static let URL_IMAGECONSENT = "url_imageconsent"
    public static let HCP_SURVEYS_RECORD = "hcp_survey_record"
    public static let HCP_DATA_CACHE = "hcp_data_cache"
    public static let FLOWABLE = "flowable"
    
}

enum AppStoryboard : String {
    case Main = "Main"
    var instance : UIStoryboard {
        return UIStoryboard(name: self.rawValue, bundle: Bundle.main)
    }
    func viewController<T : UIViewController>(viewControllerClass : T.Type) -> T {
        let storyboardID = (viewControllerClass as UIViewController.Type).storyboardID
        return instance.instantiateViewController(withIdentifier: storyboardID) as! T
    }
    func initialViewController() -> UIViewController? {
        return instance.instantiateInitialViewController()
    }
}

extension UIViewController {
    class var storyboardID : String {
        return "\(self)"
    }
    static func instantiate(appStoryboard : AppStoryboard) -> Self {
        return appStoryboard.viewController(viewControllerClass: self)
    }
}


class SimpleTimer {
    typealias Tick = ()->Void
    var timer:Timer?
    var interval:TimeInterval /*in seconds*/
    var repeats:Bool
    var tick:Tick
    
    init( interval:TimeInterval, repeats:Bool = false, onTick:@escaping Tick){
        self.interval = interval
        self.repeats = repeats
        self.tick = onTick
    }
    func start(){
        timer = Timer.scheduledTimer(timeInterval: interval, target: self, selector: #selector(update), userInfo: nil, repeats: true)
    }
    func stop(){
        if(timer != nil){timer!.invalidate()}
    }
    /**
     * This method must be in the public or scope
     */
    @objc func update() {
        tick()
    }
}

extension UIColor {
    
    var hexString:String? {
        if let components = self.cgColor.components {
            let r = components[0]
            let g = components[1]
            let b = components[2]
            return  String(format: "%02X%02X%02X", (Int)(r * 255), (Int)(g * 255), (Int)(b * 255))
        }
        return nil
    }
    
    public convenience init?(hexString: String) {
        let r, g, b, a: CGFloat
        
        if hexString.hasPrefix("#") {
            let start = hexString.index(hexString.startIndex, offsetBy: 1)
            let hexColor = hexString.substring(from: start)
            
            if hexColor.characters.count == 6 {
                let scanner = Scanner(string: hexColor)
                var hexNumber: UInt64 = 0
                
                if scanner.scanHexInt64(&hexNumber) {
                    
                    r = CGFloat((hexNumber & 0xff0000) >> 16) / 255
                    g = CGFloat((hexNumber & 0xff00) >> 8) / 255
                    b = CGFloat(hexNumber & 0xff) / 255
                    a = 1.0
                    self.init(red: r, green: g, blue: b, alpha: a)
                    return
                }
            }
        }
        
        return nil
    }
    
}

extension String {
    
    var html2Attributed: NSAttributedString? {
        do {
            guard let data = data(using: String.Encoding.utf8) else {
                return nil
            }
            return try NSAttributedString(data: data,
                                          options: [.documentType: NSAttributedString.DocumentType.html,
                                                    .characterEncoding: String.Encoding.utf8.rawValue],
                                          documentAttributes: nil)
        } catch {
            print("error: ", error)
            return nil
        }
    }
    
    var htmlAttributed: (NSAttributedString?, NSDictionary?) {
        do {
            guard let data = data(using: String.Encoding.utf8) else {
                return (nil, nil)
            }
            
            var dict:NSDictionary?
            dict = NSMutableDictionary()
            
            return try (NSAttributedString(data: data,
                                           options: [.documentType: NSAttributedString.DocumentType.html,
                                                     .characterEncoding: String.Encoding.utf8.rawValue],
                                           documentAttributes: &dict), dict)
        } catch {
            print("error: ", error)
            return (nil, nil)
        }
    }
    
    func htmlAttributed(using font: UIFont, color: UIColor) -> NSAttributedString? {
        do {
            let htmlCSSString = "<style>" +
                "html *" +
                "{" +
                "font-size: \(font.pointSize)pt !important;" +
                "color: #\(color.hexString!) !important;" +
                "font-family: \(font.familyName), Helvetica !important;" +
            "}</style> \(self)"
            
            guard let data = htmlCSSString.data(using: String.Encoding.utf8) else {
                return nil
            }
            
            return try NSAttributedString(data: data,
                                          options: [.documentType: NSAttributedString.DocumentType.html,
                                                    .characterEncoding: String.Encoding.utf8.rawValue],
                                          documentAttributes: nil)
        } catch {
            print("error: ", error)
            return nil
        }
    }
    
    func htmlAttributed(family: String?, size: CGFloat, color: UIColor) -> NSAttributedString? {
        do {
            let htmlCSSString = "<style>" +
                "html *" +
                "{" +
                "font-size: \(size)pt !important;" +
                "color: #\(color.hexString!) !important;" +
                "font-family: \(family ?? "Helvetica"), Helvetica !important;" +
            "}</style> \(self)"
            
            guard let data = htmlCSSString.data(using: String.Encoding.utf8) else {
                return nil
            }
            
            return try NSAttributedString(data: data,
                                          options: [.documentType: NSAttributedString.DocumentType.html,
                                                    .characterEncoding: String.Encoding.utf8.rawValue],
                                          documentAttributes: nil)
        } catch {
            print("error: ", error)
            return nil
        }
    }
}

extension Collection where Iterator.Element == [String:AnyObject] {
    func toJSONString(options: JSONSerialization.WritingOptions = .prettyPrinted) -> String {
        if let arr = self as? [[String:AnyObject]],
            let dat = try? JSONSerialization.data(withJSONObject: arr, options: options),
            let str = String(data: dat, encoding: String.Encoding.utf8) {
            return str
        }
        return "[]"
    }
}

class Utils{
    
    
    public static let APP_ID = "210001"
    
    func convertToDictionary(text: String) -> Any? {
        
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? Any
            } catch {
                print(error.localizedDescription)
            }
        }
        
        return nil
        
    }
    
    private static let second = 60
    
    public static func getTimeFromMili(mili : Int) -> String{
        return "\(mili/60):\(mili%60)"
    }
    
    public static func getFromCache(key : String) -> String? {
        return UserDefaults.standard.string(forKey: key)
    }
    
    public static func saveToCache(key : String, value : String){
        UserDefaults.standard.set(value, forKey: key)
    }
    
    public static func updateInstruction(page : String){
        UserDefaults.standard.set(true, forKey: page)
    }
    
    public static func isInstructed(page : String) -> Bool{
        return !UserDefaults.standard.bool(forKey: page)
    }
    
    public static func clearFromCache(key : String){
        UserDefaults.standard.removeObject(forKey: key)
    }
    
    public static func getDurationData() -> [DurationRecord]{
        let json = getFromCache(key: CacheKey.DURATION_RECORD)
        if let j = json {
            return [DurationRecord](json : j)
        } else {
            return []
        }
    }
    
    static let WEEK = 604800000
    
    public static func saveDurations(durations : [DurationRecord]) {
        var inTimeDuration : [DurationRecord] = []
        let now = Int(Date().timeIntervalSince1970*1000)
        for duration in durations {
            if (now - duration.startTick) <= WEEK{
                inTimeDuration.append(duration)
            }
        }
        saveToCache(key: CacheKey.DURATION_RECORD, value: inTimeDuration.toJsonString())
    }
    
    public static func getLastDuration() -> DurationRecord? {
        let data = getDurationData()
        if (data.count > 0){
            return data[data.count-1]
        } else {
            return nil
        }
    }
    
    public static func saveLastDuration(duration : DurationRecord) {
        var data = getDurationData()
        if (data.count>0){
            data[data.count-1] = duration
            saveDurations(durations: data)
        }
    }
    
    /////
    
    public static func saveFlowableData(arr : [Int]){
        UserDefaults.standard.setValue(arr, forKey: CacheKey.FLOWABLE)
    }
    
    public static func getFlowableData(flow: Int) -> [MenuItem] {
        var menuItemArray: [MenuItem] = []
        for i in 0...4 {
            if i != flow {
                menuItemArray.append(MenuItem(title: "", type: i))
            }
        }
        return menuItemArray
    }
    
    public static func getFlowableIndex(_ fromFlow : Int) -> [Int] {
        if let flowable = UserDefaults.standard.value(forKey: CacheKey.FLOWABLE) as? [Int] {
            var flows = flowable
            for i in 0...flows.count - 1 {
                if flows[i] == fromFlow {
                    flows.remove(at: i)
                    break
                }
            }
            return flows
        } else {
            return []
        }
    }
    
    public static func getSurveyRecords() -> [SurveyRecord]{
        let json = getFromCache(key: CacheKey.HCP_SURVEYS_RECORD)
        if let j = json {
            return [SurveyRecord](json : j)
        } else {
            return []
        }
    }
    
    public static func saveSurveyRecords(records : [SurveyRecord]) {
        var inTimeRecord : [SurveyRecord] = []
        let now = Int(Date().timeIntervalSince1970*1000)
        for record in records {
            if (now - record.startTick) <= WEEK{
                inTimeRecord.append(record)
            }
        }
        saveToCache(key: CacheKey.HCP_SURVEYS_RECORD, value: inTimeRecord.toJsonString())
    }
    
    public static func getLastHCPSurvey() -> SurveyRecord? {
        let data = getSurveyRecords()
        if (data.count > 0){
            return data[data.count-1]
        } else {
            return nil
        }
    }
    
    public static func saveSurveyRecord(record : SurveyRecord) {
        var data = getSurveyRecords()
        data.append(record)
        saveSurveyRecords(records: data)
    }
    
    public static func saveSurveyRecord(_ id : String, _ record : SurveyRecord) {
        var data = getSurveyRecords()
        for i in 0...data.count - 1{
            if (data[i].clientid?.elementsEqual(id))! {
                data[i] = record
                break
            }
        }
        saveSurveyRecords(records: data)
    }
    
    public static func removeHCPSurvey(id : String){
        var data = getSurveyRecords()
        var index = -1
        
        if (data.count > 0){
            for i in 0...data.count-1 {
                if (data[i].clientid?.elementsEqual(id))!{
                    index = i
                    break
                }
            }
            if index>=0 {
                data.remove(at: index)
                saveSurveyRecords(records: data)
            }
        }
    }
    
    public static func exportSignToBase64(_ image : UIImage) -> String{
        let imageData:NSData = image.pngData()! as NSData
        let str64 = imageData.base64EncodedString(options: .lineLength64Characters)
        return str64
    }
    
    public static func saveImage(image: UIImage,fileName: String) -> String {
        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let url = documentsURL.appendingPathComponent("\(fileName).png")
        let result:NSData = image.pngData()! as NSData
        result.write(to: url, atomically: true)
        return url.path
    }
    
    //////
    public static func clearDuration(_ id : String) {
        var data = getDurationData()
        var index = -1
        if (data.count > 0){
            for i in 0...data.count-1 {
                if (data[i].clientid?.elementsEqual(id))!{
                    index = i
                    break
                }
            }
            if index>=0 {
                data.remove(at: index)
                saveDurations(durations: data)
            }
        }
    }
    
}
