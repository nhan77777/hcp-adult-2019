//
//  DurationRecord.swift
//  HCP Adult
//
//  Created by Bình Phạm on 7/6/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import Foundation
import Alamofire
import EVReflection
import CoreLocation

class DurationRecord : EVObject {
    public var appid: String?
    public var usersysid: String?
    public var hospid: String?
    public var hcpid: String?
    public var endtime: String?
    public var starttime: String?
    public var appver: String?
    public var clientid: String?
    public var startTick : Int = 0
    public var longitude : String?
    public var latitude : String?
    public var durations : Dictionary<String,Double> = [:]
    
    override func setValue(_ value: Any!, forUndefinedKey key: String) {
        switch key {
        case "durations":
            if let dict = value as? NSDictionary {
                self.durations = [:]
                for (key, value) in dict {
                    self.durations[key as? String ?? ""] = value as? Double
                }
            }
        default:
            self.addStatusMessage(.IncorrectKey, message: "SetValue for key '\(key)' should be handled.")
        }
    }
}
