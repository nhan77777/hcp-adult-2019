//
//  ProsurePage6VC.swift
//  HCP Adult
//
//  Created by Bình Phạm on 7/26/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import UIKit
import Async

class ProsurePage6VC: BaseDetailVC, DetailView {
    
    @IBOutlet weak var imgBg: UIImageView!
    @IBOutlet var imgIcons: [UIImageView]!
    
    func loadImages() {
        loadResourceToImage("pro_bg_page_6", imgBg)
        for i in 0...imgIcons.count - 2 {
            loadResourceToImage("pro_ic_page_1_\(i)", imgIcons[i])
        }
        loadResourceToImage("pro_ic_page_6_0", imgIcons[imgIcons.count-1])
    }
    
    func resetAnimation() {
        for ic in imgIcons {
            ic.layer.removeAllAnimations()
            ic.stopGlowing()
        }
    }
    
    func startAnimation() {
        imgIcons[0].twinkle()
        imgIcons[imgIcons.count - 1].startGlowingWithColor(color: UIColor.yellow, fromIntensity: 0.2, toIntensity: 0.8, repeat: true)
    }
    
    @objc func onImageTap(_ sender : UITapGestureRecognizer){
        let image = sender.view
        image?.startGlowingWithColor(color: UIColor.yellow, fromIntensity: 0.2, toIntensity: 0.5, repeat: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadImages()
        for i in 1...3 {
            imgIcons[i].isUserInteractionEnabled = true
            imgIcons[i].addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onImageTap(_:))))
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Async.main(after: 0.1, {
            self.startAnimation()
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
