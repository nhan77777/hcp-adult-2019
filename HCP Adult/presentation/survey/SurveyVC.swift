//
//  SurveyVC.swift
//  HCP Adult
//
//  Created by Bình Phạm on 6/8/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import UIKit
import SwiftSpinner
import CollectionViewCenteredFlowLayout

class SurveyVC: UIViewController {
    
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbSurveyTitle: UILabel!
    @IBOutlet var viewHCPInfos: [HCPInfoView]!
    
    @IBOutlet var viewAnswers: [UIView]!
    @IBOutlet var contentAnswer: [UIView]!
    @IBOutlet weak var constControlTop: NSLayoutConstraint!
    @IBOutlet weak var clvQuestion: UICollectionView!
    private var feedbacks : [Feedback] = []
    private var arrAnswers : [String] = []
    private var hcpSurvey = HCPSurvey(JSONString: "{}")
    private var surveyRecord : SurveyRecord?
    
    @IBAction func onSaveTap(_ sender: Any) {
        prepareSurveyRecord()
        let mh = ConsentPageVC(nibName: "ConsentPageVC", bundle: nil)
        mh.surveyRecord = surveyRecord
        present(mh, animated: true, completion: nil)
    }
    
    @IBAction func onCancelTap(_ sender: Any) {
        let mh = ChoiceHCPVC(nibName: "ChoiceHCPVC", bundle: nil)
        present(mh, animated: true, completion: nil)
    }
    
    var answered = [0,0,0,0,0,0,0,0]
    
    let presenter = PushDurationPresenter(durationService: PushDuration())
    private var durationRequest : Dictionary<String, Any>?
    let selectedHCP = HCPModel(JSONString: Utils.getFromCache(key: CacheKey.selectedHCP)!)
    
    func prepareSurveyRecord(){
        let user : UserData = UserData(JSONString: Utils.getFromCache(key: CacheKey.userdata)!)!
        surveyRecord = SurveyRecord()
        surveyRecord?.appid = durationRequest!["appid"] as? String
        surveyRecord?.usersysid = user.sysid
        surveyRecord?.clientid = durationRequest!["clientid"] as? String
        surveyRecord?.hcpid = selectedHCP?.id
        surveyRecord?.appver = durationRequest!["appver"] as? String
        surveyRecord?.createAt = durationRequest!["endtime"] as? String
        surveyRecord?.startTick = Int(Date().timeIntervalSince1970*1000)
        surveyRecord?.signatureUrl = ""
        surveyRecord?.feedback = []
        
        for feedback in feedbacks {
            let fb = FeedbackRecord()
            fb.question = feedback.question
            fb.answer = feedback.answer
            surveyRecord?.feedback?.append(fb)            
        }
        Utils.saveSurveyRecord(record: surveyRecord!)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(self)
        prepareDurationRequest()
        setupUI()
        
        viewHCPInfos[0].imgTitle.image = #imageLiteral(resourceName: "ic_hcp_name.png")
        viewHCPInfos[1].imgTitle.image = #imageLiteral(resourceName: "ic_hcp_spec.png")
        viewHCPInfos[2].imgTitle.image = #imageLiteral(resourceName: "ic_hcp_dept.png")
        viewHCPInfos[3].imgTitle.image = #imageLiteral(resourceName: "ic_hcp_type.png")
        viewHCPInfos[4].imgTitle.image = #imageLiteral(resourceName: "ic_hcp_phone.png")
        viewHCPInfos[5].imgTitle.image = #imageLiteral(resourceName: "ic_hcp_email.png")
        
        viewHCPInfos[0].lbTitle.text = "Tên HCP"
        viewHCPInfos[1].lbTitle.text = "Chuyên khoa"
        viewHCPInfos[2].lbTitle.text = "Khoa"
        viewHCPInfos[3].lbTitle.text = "Loại HCP"
        viewHCPInfos[4].lbTitle.text = "Số điện thoại"
        viewHCPInfos[5].lbTitle.text = "Email"
        
        setContentHcp(viewHCPInfos[0].lbTitle, (selectedHCP?.name)!)
        setContentHcp(viewHCPInfos[1].lbTitle, (selectedHCP?.specialty)!)
        setContentHcp(viewHCPInfos[2].lbTitle, (selectedHCP?.dept)!)
        setContentHcp(viewHCPInfos[3].lbTitle, (selectedHCP?.hcptype)!)
        setContentHcp(viewHCPInfos[4].lbTitle, (selectedHCP?.phone)!)
        setContentHcp(viewHCPInfos[5].lbTitle, (selectedHCP?.email)!)
        
        if let request = durationRequest {
            presenter.pushDuration(durationRequestModel: request)
        }
    }
    
    func prepareDurationRequest(){
        if let durationRecord = Utils.getLastDuration(){
            durationRequest = durationRecord.durations
            durationRequest!["clientid"] = durationRecord.clientid
            durationRequest!["appid"] = durationRecord.appid
            durationRequest!["appver"] = durationRecord.appver
            durationRequest!["usersysid"] = durationRecord.usersysid
            durationRequest!["starttime"] = durationRecord.starttime
            durationRequest!["longitude"] = durationRecord.longitude
            durationRequest!["hcpid"] = durationRecord.hcpid
            durationRequest!["latitude"] = durationRecord.latitude
            
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
            let endTime = formatter.string(from: Date())
            durationRequest!["endtime"] = endTime
            
        }
    }
    
    func setContentHcp(_ target : UILabel, _ content : String){
        if !content.isEmpty {
            target.textColor = UIColor.black
            target.text = content
        }
    }
    
    func setupUI(){
        let gradient:CAGradientLayer = CAGradientLayer()
        gradient.frame.size = self.lbTitle.frame.size
        gradient.colors = [UIColor(hexString: "#0075bc")?.cgColor,UIColor(hexString: "#1e3f94")?.cgColor]
        gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        lbTitle.layer.addSublayer(gradient)
        let reverseGradient:CAGradientLayer = CAGradientLayer()
        reverseGradient.frame.size = self.lbSurveyTitle.frame.size
        reverseGradient.colors = [UIColor(hexString: "#1e3f94")?.cgColor,UIColor(hexString: "#0075bc")?.cgColor]
        reverseGradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        reverseGradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        lbSurveyTitle.layer.addSublayer(reverseGradient)
        let ques = ["Câu hỏi 1: Quý vị đánh giá phần nội dung trình bày vừa rồi như thế nào ?",
                    "Câu hỏi 2: Quý vị đánh giá phần nội dung trình bày về Ensure Gold như thế nào ?",
                    "Câu hỏi 3: Quý vị đánh giá phần nội dung trình bày về Glucerna như thế nào ?",
                    "Câu hỏi 4: Quý vị đánh giá phần nội dung trình bày về Prosure như thế nào ?",
                    "Câu hỏi 5: Quý vị đánh giá phần nội dung trình bày về Ensure Plus như thế nào ?",
                    "Câu hỏi 6: Quý vị đánh giá phần nội dung trình bày về Vital như thế nào ?",
                    "Câu hỏi 7: Những thông tin nào Quý Vị mong muốn chúng tôi chia sẻ thêm ?",
                    "Câu hỏi 8: Quý Vị muốn chúng tôi chia sẻ thông tin bằng cách nào ?"]
        let ans4 = ["Dữ liệu lâm sàng", "Các hội thảo khoa học", "Thông tin dinh dưỡng", "Các chứng cứ mới"]
        
        let chk4 = contentAnswer[6] as! CheckboxSurvey
        chk4.setQuestions(ans4)
        let ans5 = ["Bản tin E-Mail", "Bản in ấn", "File mềm"]
        let chk5 = contentAnswer[7] as! CheckboxSurvey
        chk5.setQuestions(ans5)
        
        for i in 0...ques.count-1{
            let fb = Feedback(JSONString: "{}")
            fb?.question = ques[i]
            fb?.answer = []
            feedbacks.append(fb!)
        }
        
        for i in 0...5{
            let content = contentAnswer[i] as! RatingSurvey
            content.onTap = {
                self.feedbacks[i].question = ques[i]
                self.feedbacks[i].answer = []
                self.feedbacks[i].answer?.append(String(content.targetResult))
                self.answered[i] = 1
                self.clvQuestion.reloadData()
            }
        }
        
        for i in 6...7 {
            let content = contentAnswer[i] as! CheckboxSurvey
            content.callback = { index, bool in
                print(index, bool)
                if bool {
                    let fb = self.feedbacks[i]
                    
                    var ansTemp = [String]()
                    
                    fb.question = ques[i]
                    
                    if i == 6{
                        ansTemp.append(ans4[index])
                    }
                    if i == 7{
                        ansTemp.append(ans5[index])
                    }
                    if let ans = fb.answer{
                        ansTemp.append(contentsOf: ans)
                    }
                    ansTemp = ansTemp.removingDuplicates()
                    fb.answer = ansTemp
                    self.feedbacks[i] = fb
                    self.answered[i] = 1
                    self.clvQuestion.reloadData()
                    
                }
                else{
                    let fb = self.feedbacks[i]
                    var ansTemp = [String]()
                    fb.question = ques[i]
                    if let ans = fb.answer{
                        ansTemp.append(contentsOf: ans)
                        if i == 6{
                            ansTemp.remove(at: ansTemp.index(of: (ans4[index]))!)
                        }
                        if i == 7{
                            ansTemp.remove(at: ansTemp.index(of: (ans5[index]))!)
                        }
                    }
                    fb.answer = ansTemp
                    self.feedbacks[i] = fb
                    self.answered[i] = 0
                    self.clvQuestion.reloadData()
                }
            }
            
        }
        
        for view in viewAnswers {
            view.isHidden = true
        }
        clvQuestion.dataSource = self
        clvQuestion.register(UINib.init(nibName:"QuestionProcessHeaderCell",bundle:nil), forCellWithReuseIdentifier: "QuestionProcessHeaderCell")
        clvQuestion.register(UINib.init(nibName:"QuestionProcessFooterCell",bundle:nil), forCellWithReuseIdentifier: "QuestionProcessFooterCell")
        let layout: CollectionViewCenteredFlowLayout = clvQuestion.collectionViewLayout as! CollectionViewCenteredFlowLayout
        layout.estimatedItemSize = CGSize(width: 100, height: 62)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        clvQuestion!.collectionViewLayout = layout
        viewAnswers[0].isHidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    var lastSelectedIndexPath : IndexPath?
    
    @objc func onQuestionTap( _ sender : UITapGestureRecognizer){
        let tapLocation = sender.location(in: clvQuestion)
        let indexPath = clvQuestion.indexPathForItem(at : tapLocation)
        let index = indexPath?.row
        for view in viewAnswers {
            view.isHidden = true
        }
        viewAnswers[index!].isHidden = false
        
        if let lastIndex = lastSelectedIndexPath {
            if answered[lastIndex.row] != 1{
                if let lastCell = clvQuestion.cellForItem(at: lastIndex) as? QuestionProcessHeaderCell {
                    deselectCell(lastCell)
                }
                if let lastCell = clvQuestion.cellForItem(at: lastIndex) as? QuestionProcessFooterCell {
                    deselectCell(lastCell)
                }
            }
        }
        if (answered[(indexPath?.row)!] != 1){
            if let cell = clvQuestion.cellForItem(at: indexPath!) as? QuestionProcessHeaderCell {
                selectCell(cell)
            }
            if let cell = clvQuestion.cellForItem(at: indexPath!) as? QuestionProcessFooterCell {
                selectCell(cell)
            }
        }
        lastSelectedIndexPath = indexPath
        UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 4, options: [], animations: {
            self.constControlTop.constant = self.viewAnswers[index!].bounds.height + 30
            self.view.layoutIfNeeded()
            
        }, completion: nil)
        
    }
    
    func selectCell(_ cell : QuestionProcessHeaderCell){
        cell.lbQuestions.backgroundColor = UIColor(hexString: "#FC872A")
        cell.lbQuestions.textColor = UIColor.white
    }
    
    func deselectCell(_ cell : QuestionProcessHeaderCell){
        cell.lbQuestions.backgroundColor = UIColor.white
        cell.lbQuestions.textColor = UIColor(hexString: "#FC872A")
    }
    
    func selectCell(_ cell : QuestionProcessFooterCell){
        cell.lbQuestions.backgroundColor = UIColor(hexString: "#FC872A")
        cell.lbQuestions.textColor = UIColor.white
    }
    
    func deselectCell(_ cell : QuestionProcessFooterCell){
        cell.lbQuestions.backgroundColor = UIColor.white
        cell.lbQuestions.textColor = UIColor(hexString: "#FC872A")
    }
    
}

extension SurveyVC : PushDurationView {
    
    
    
    func pushDurationSuccess(_ data: ReturnDurationModel) {
        Utils.clearDuration(data.clientid!)
    }
    
    func pushDurationFailed(_ message: String) {
        
    }
    
    func showLoading(_ message: String) {
        SwiftSpinner.show(message)
    }
    
    func hideLoading() {
        SwiftSpinner.hide()
    }
    
}

extension SurveyVC : UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row == 7 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "QuestionProcessFooterCell", for: indexPath) as! QuestionProcessFooterCell
            cell.lbQuestions.text = "\(indexPath.row + 1)"
            cell.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onQuestionTap(_:))))
            if (answered[indexPath.row] == 1){
                cell.imgDone.isHidden = false
                cell.lbQuestions.textColor = UIColor(hexString: "#1176B9")
                cell.lbQuestions.backgroundColor = UIColor(hexString: "#1176B9")
            } else {
                cell.imgDone.isHidden = true
                cell.lbQuestions.textColor = UIColor(hexString: "#FC872A")
                cell.lbQuestions.backgroundColor = UIColor.white
            }
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "QuestionProcessHeaderCell", for: indexPath) as! QuestionProcessHeaderCell
            cell.lbQuestions.text = "\(indexPath.row + 1)"
            cell.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onQuestionTap(_:))))
            if (answered[indexPath.row] == 1){
                cell.imgDone.isHidden = false
                cell.viewConnection.backgroundColor = UIColor(hexString: "#1176B9")
                cell.lbQuestions.backgroundColor = UIColor(hexString: "#1176B9")
                cell.lbQuestions.textColor = UIColor(hexString: "#1176B9")
            } else {
                cell.imgDone.isHidden = true
                cell.viewConnection.backgroundColor = UIColor(hexString: "#ACACAC")
                cell.lbQuestions.backgroundColor = UIColor.white
                cell.lbQuestions.textColor = UIColor(hexString: "#FC872A")
            }
            return cell
        }
    }
}

extension Array where Element: Equatable {
    func removingDuplicates() -> Array {
        return reduce(into: []) { result, element in
            if !result.contains(element) {
                result.append(element)
            }
        }
    }
}
