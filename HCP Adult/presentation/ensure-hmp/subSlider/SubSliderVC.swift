//
//  SubSliderVC.swift
//  EnsureHMP
//
//  Created by Bình Phạm on 3/29/18.
//

import UIKit
import Pageboy
import PopupDialog

class SubSliderVC: PageboyViewController {
    
    public var orderedViewControllers: [BaseDetailVC] = []
    public var vcParent : PageView?
    private var timer = Timer()
    private var durationRecord : DurationRecord?
    private var currentPageIndex = 0

    @IBAction func onCloseTap(_ sender: Any) {
        dismiss(animated: true, completion: {
            self.vcParent?.startTimer()
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dataSource = self
        delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        for vc in orderedViewControllers {
            if vc is EnsurePage2aVC
            {
                (vc as! EnsurePage2aVC).delegate = self
            } else if vc is EnsurePage2bVC {
                (vc as! EnsurePage2bVC).delegate = self
            } else if vc is EnsurePage2cVC {
                (vc as! EnsurePage2cVC).delegate = self
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        durationRecord = Utils.getLastDuration()
        startTimer()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        pauseTimer()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func actionTimer(){
        print(orderedViewControllers[currentPageIndex].pageID)
        durationRecord?.durations[orderedViewControllers[currentPageIndex].pageID] = (durationRecord?.durations[orderedViewControllers[currentPageIndex].pageID])! + 1
        if let record = durationRecord {
            Utils.saveLastDuration(duration: record)
        }
    }
    
    func startTimer(){
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(actionTimer), userInfo: nil, repeats: true)
    }
    
    func pauseTimer(){
        timer.invalidate()
    }
    
}

extension SubSliderVC : PageboyViewControllerDataSource {
    
    func numberOfViewControllers(in pageboyViewController: PageboyViewController) -> Int {
        return orderedViewControllers.count
    }
    
    func viewController(for pageboyViewController: PageboyViewController, at index: PageboyViewController.PageIndex) -> UIViewController? {
        return orderedViewControllers[index]
    }
    
    func defaultPage(for pageboyViewController: PageboyViewController) -> PageboyViewController.Page? {
        return nil
    }
    
}

extension SubSliderVC : PageboyViewControllerDelegate {
    func pageboyViewController(_ pageboyViewController: PageboyViewController, didScrollTo position: CGPoint, direction: PageboyViewController.NavigationDirection, animated: Bool) {
        
    }
    
    func pageboyViewController(_ pageboyViewController: PageboyViewController, didReloadWith currentViewController: UIViewController, currentPageIndex: PageboyViewController.PageIndex) {
        
    }
    
    func pageboyViewController(_ pageboyViewController: PageboyViewController,
                               didScrollToPageAt index: Int,
                               direction: PageboyViewController.NavigationDirection,
                               animated: Bool){
        currentPageIndex = index
        startTimer()
        
//        if orderedViewControllers[index] is EnsurePage2aVC ||
//            orderedViewControllers[index] is EnsurePage2bVC ||
//            orderedViewControllers[index] is EnsurePage2cVC {
//            pageboyViewController.isScrollEnabled = false
//        } else {
//            pageboyViewController.isScrollEnabled = true
//        }
    }
    
    func pageboyViewController(_ pageboyViewController: PageboyViewController, willScrollToPageAt index: Int, direction: PageboyViewController.NavigationDirection, animated: Bool) {
        pauseTimer()
    }
}

extension SubSliderVC: PageView {
    func onChangeFlow(vc: UIViewController) {
    }
    
    func onRequestEndCall() {
        
    }
    
    func moveToPage(index: Int) {
        if index == -1 {
            scrollToPage(Page.at(index: currentPageIndex + 1), animated: true)
        } else {
            scrollToPage(Page.at(index: index), animated: true)
        }
    }
    
    func setTransition(_ trans: PageboyViewController.Transition) {
        transition = trans
    }
    
    
}
