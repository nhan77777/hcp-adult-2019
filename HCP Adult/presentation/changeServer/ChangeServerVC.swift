//
//  ChangeServerVC.swift
//  HCP Adult
//
//  Created by Bình Phạm on 5/31/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import UIKit

class ChangeServerVC: UIViewController {

    @IBOutlet weak var txtUrl: UITextField!
    @IBOutlet weak var viewUrl: UIView!
    
    @IBAction func onBackTap(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func onSaveTap(_ sender: Any) {
        Utils.saveToCache(key: CacheKey.baseUrl, value: txtUrl.text!)
        dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    
    }
    
    func setupUI(){
        viewUrl.layer.cornerRadius = 20
        txtUrl.text = Utils.getFromCache(key: CacheKey.baseUrl)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
