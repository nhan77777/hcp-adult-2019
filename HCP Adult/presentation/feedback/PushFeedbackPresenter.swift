//
//  FeedbackViewController.swift
//  HCP Adult
//
//  Created by Long Quách Phi on 7/3/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import UIKit

class PushFeedbackPresenter {
    fileprivate let feedbackService : PushFeedback
    weak fileprivate var pushFeedbackView : PushFeedbackView?
    
    init(feedbackService : PushFeedback){
        self.feedbackService = feedbackService
    }
    
    func attachView(_ view : PushFeedbackView){
        pushFeedbackView = view
    }
    
    func detachView() {
        pushFeedbackView = nil
    }
    
    func pushFeedback(hcpSurvey: HCPSurvey){
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        pushFeedbackView?.showLoading("Uploading ... ")
        feedbackService.pushFeedback(hcpSurvey: hcpSurvey) { success, response in
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            self.pushFeedbackView?.hideLoading()
            if success{
                if let status = response.status {
                    if (status.elementsEqual("true")){
                        self.pushFeedbackView?.pushFeedbackSuccess(response)
                    } else {
                        self.pushFeedbackView?.pushFeedbackFailed(response.msg!)
                    }
                }
                else{
                    self.pushFeedbackView?.pushFeedbackFailed("Kết nối thất bại")
                }
            }
            else{
                self.pushFeedbackView?.pushFeedbackFailed("Kết nối thất bại")
            }
        }
    }
}
