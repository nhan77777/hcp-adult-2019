//
//  Page4VC.swift
//  Glucerna
//
//  Created by Long Quách Phi on 3/22/18.
//  Copyright © 2018 Fractal. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class Page4VC: BaseDetailVC, DetailView{
    
    @IBOutlet var viewVideoPlayer: [UIView]!
    @IBOutlet var viewCoverVideoPlayer: [UIView]!
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet var viewFooter: [UIView]!
    private var player = [AVPlayer(),AVPlayer()]
    
    func setupAction(){
        for i in 0...viewVideoPlayer.count - 1{
            viewVideoPlayer[i].addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onViewTapVideo(_:))))
            viewVideoPlayer[i].isUserInteractionEnabled = true
            viewVideoPlayer[i].tag = i
        }
        viewFooter[0].addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onViewTapFooter(_:))))
        viewFooter[0].isUserInteractionEnabled = true
        prepareViewPlayer()
    }
    
    @objc func onViewTapFooter(_ sender : UITapGestureRecognizer){
        showPDFView(filename: "glu_3")
    }
    
    @objc func onViewTapVideo(_ sender : UITapGestureRecognizer){
        let index = sender.view?.tag
        view.bringSubviewToFront(viewVideoPlayer[index!])
        if player[index!].isPlaying {
            player[index!].pause()
            viewCoverVideoPlayer[index!].layer.opacity = 0
            view.sendSubviewToBack(viewVideoPlayer[index!])
        }
        else{
            viewCoverVideoPlayer[index!].layer.opacity = 1
            player[index!].play()
        }
        if player[index!].currentItem?.duration == player[index!].currentTime() {
            player[index!].seek(to: CMTime.zero)
            view.sendSubviewToBack(viewVideoPlayer[index!])
        }

    }
    
    func loadImages() {
        loadResourceToImage("glu_bg_page4", imgBackground)
    }
    
    func initialViewVideo(index: Int){
        guard let path = Bundle.main.path(forResource: "glu_video\(index)", ofType:"mp4") else {
            debugPrint("video\(index).mp4 not found")
            return
        }
        let videoUrl = URL(fileURLWithPath: path)
        player[index] = AVPlayer(url: videoUrl)
        let layer: AVPlayerLayer = AVPlayerLayer(player: player[index])
        layer.frame = viewVideoPlayer[index].bounds
        layer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        player[index].isMuted = true
        NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: player[index].currentItem, queue: .main) { _ in
            self.player[index].seek(to: CMTime.zero)
            self.player[index].play()
        }
        viewVideoPlayer[index].layer.addSublayer(layer)
        view.sendSubviewToBack(viewVideoPlayer[index])
    }
    
    func prepareViewPlayer(){
        initialViewVideo(index: 0)
        initialViewVideo(index: 1)
    }
    
    func resetAnimation() {
        for i in 0...1{
            player[i].pause()
            player[i].seek(to: CMTime.zero)
            view.sendSubviewToBack(viewVideoPlayer[i])
        }
        viewCoverVideoPlayer[0].layer.opacity = 0
        viewCoverVideoPlayer[1].layer.opacity = 0
    }
    
    func startAnimation() {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pageID = "page33"
        setupAction()
        loadImages()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        resetAnimation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        startAnimation()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        player[0].pause()
        player[1].pause()
    }
    
}

extension AVPlayer {
    
    var isPlaying: Bool {
        return rate != 0 && error == nil
    }
    
}
