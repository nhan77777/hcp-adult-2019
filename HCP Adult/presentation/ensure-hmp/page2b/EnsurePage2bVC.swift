//
//  EnsurePageThreeVC.swift
//  HCP Adult
//
//  Created by Long Quách Phi on 5/24/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import UIKit

class EnsurePage2bVC: BaseDetailVC, DetailView {
    
    @IBOutlet weak var imgBg: UIImageView!
    
    @IBOutlet weak var btnFirst: UIButton!
    @IBOutlet weak var btnSecond: UIButton!
    @IBOutlet weak var btnThird: UIButton!
    @IBOutlet weak var btnFourth: UIButton!
    
    var delegate: PageView?
    
    private var is1stItemSelected = false {
        didSet {
            !is1stItemSelected ? (btnFirst.setImage(UIImage(named: "ic_ChiSoDuongHuyet_unselected_page2b"), for: .normal)) : (btnFirst.setImage(UIImage(named: "ic_ChiSoDuongHuyet_selected_page2b"), for: .normal))
        }
    }
    
    private var is2ndtItemSelected = false {
        didSet {
            !is2ndtItemSelected ? (btnSecond.setImage(UIImage(named: "ic_KhoiCo_unselected_page2b"), for: .normal)) : (btnSecond.setImage(UIImage(named: "ic_KhoiCo_selected_page2b"), for: .normal))
        }
    }
    
    private var is3rdtItemSelected = false {
        didSet {
            !is3rdtItemSelected ? (btnThird.setImage(UIImage(named: "ic_BienChung_unselected_page2b"), for: .normal)) : (btnThird.setImage(UIImage(named: "ic_BienChung_selected_page2b"), for: .normal))
        }
    }
    
    private var is4thtItemSelected = false {
        didSet {
            !is4thtItemSelected ? (btnFourth.setImage(UIImage(named: "ic_MachHuyeAp_unselected_page2b"), for: .normal)) : (btnFourth.setImage(UIImage(named: "ic_MachHuyetAp_selected_page2b"), for: .normal))
        }
    }
    
    func loadImages() {
        loadResourceToImage("ensure_bg_page_2b", imgBg)
        
    }
    
    func resetAnimation() {
        
    }
    
    func startAnimation() {
       
    }
    
    @IBAction func abtnTapped(_ sender: UIButton) {
        select(buttonTag: sender.tag)
    }
    @IBAction func abtnResult(_ sender: UIButton) {
        
        let vc = CongratulationVC.init(nibName: "CongratulationVC", bundle: nil)
        vc.modalTransitionStyle = .coverVertical
        vc.modalPresentationStyle = .overCurrentContext
        vc.callBack = {
            self.delegate?.moveToPage(index: -1)
        }
        if is1stItemSelected ||
            is2ndtItemSelected ||
            is3rdtItemSelected ||
            is4thtItemSelected {
            
            vc.isCorrect = false
            
        } else {
            
            vc.isCorrect = true
        }
        present(vc, animated: true, completion: nil)
    }
    
    func select(buttonTag: Int) {
        
        switch buttonTag {
        case 1:
            is1stItemSelected ? (is1stItemSelected = false) : (is1stItemSelected = true)
            break
        case 2:
            is2ndtItemSelected ? (is2ndtItemSelected = false) : (is2ndtItemSelected = true)
            break
        case 3:
            is3rdtItemSelected ? (is3rdtItemSelected = false) : (is3rdtItemSelected = true)
            break
        case 4:
            is4thtItemSelected ? (is4thtItemSelected = false) : (is4thtItemSelected = true)
            break
        default:
            break
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadImages()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        resetAnimation()
        
        is1stItemSelected = false
        is2ndtItemSelected = false
        is3rdtItemSelected = false
        is4thtItemSelected = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        startAnimation()
    }
    
}
