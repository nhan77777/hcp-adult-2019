//
//  API.swift
//  ba
//
//  Created by Bình Phạm on 11/3/17.
//  Copyright © 2017 Fractal. All rights reserved.
//
import Foundation

class API {
    
    public static var linkInstallr = "http://installr.firstcom.vn/hcp-adult"
    
    public static var loginValidation = "/MBLoginValidationController"
    
    public static var downloadHCP = "/SyncDownHCPForHCPAdult"
    
    public static var callDuration = "/SyncUpDetailingCallDuration"
    
    public static var feedback = "/SyncUpHCPFeedback"
    
    public static var checkLatestAppVersion = "/CheckLatestAppVersion"
    
}


