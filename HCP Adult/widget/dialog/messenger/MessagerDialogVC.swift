//
//  MessagerDialogVC.swift
//  HCP Adult
//
//  Created by Bình Phạm on 6/6/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import UIKit

class MessagerDialogVC: UIViewController {
    
    @IBOutlet weak var lbMessenge: UILabel!
    @IBAction func onOkTap(_ sender: Any) {
        if !isAlert{
            dismiss(animated: true, completion: {
                self.callbackPositive?(self)
            })
        }
        else{
            callbackPositive?(self)
        }
    }
    public var isAlert: Bool = false
    public var messenge : String = ""
    public var callbackPositive:((UIViewController) -> ())?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.layer.borderColor = UIColor(hexString: "#2b6eb2")?.cgColor
        view.layer.borderWidth = 2
        view.layer.cornerRadius = 15
        // Do any additional setup after loading the view.
        lbMessenge.text = messenge
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
