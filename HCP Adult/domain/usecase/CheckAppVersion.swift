//
//  CheckAppVersion.swift
//  HCP Adult
//
//  Created by Long Quách Phi on 11/16/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import Foundation
import Alamofire

class CheckAppVersion {
    
    func params(_ appId : String, _ appVer : String) -> Parameters{
        let request:Parameters = ["appid":appId, "appver": appVer]
        return request
    }
    
    func checkAppVersion(_ appId : String, _ appVer : String, callBack:@escaping (Bool, ResponseCheckVersion) -> Void){
        NetworkService
            .post(endPoint: API.checkLatestAppVersion, requestBody: params(appId,appVer)){ success, response in
                if let data = ResponseCheckVersion(JSONString: response) {
                    callBack(success, data)
                } else {
                    callBack(false, ResponseCheckVersion(JSONString: "{}")!)
                }
        }
    }
}
