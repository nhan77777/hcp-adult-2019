//
//  UserData.swift
//
//  Created by Bình Phạm on 6/6/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public class UserData: Mappable {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let name = "name"
        static let sysid = "sysid"
        static let userid = "userid"
    }
    
    // MARK: Properties
    public var name: String?
    public var sysid: String?
    public var userid: String?
    
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public required init?(map: Map){
        
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        name <- map[SerializationKeys.name]
        sysid <- map[SerializationKeys.sysid]
        userid <- map[SerializationKeys.userid]

    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = name { dictionary[SerializationKeys.name] = value }
        if let value = sysid { dictionary[SerializationKeys.sysid] = value }
        if let value = userid { dictionary[SerializationKeys.userid] = value }
        return dictionary
    }
    
}
