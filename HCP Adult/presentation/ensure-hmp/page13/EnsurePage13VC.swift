//
//  EnsurePage13VC.swift
//  HCP Adult
//
//  Created by Le Nhan on 1/7/19.
//  Copyright © 2019 Bình Phạm. All rights reserved.
//

import UIKit

class EnsurePage13VC: BaseDetailVC, DetailView {
    
    @IBOutlet weak var imgBg: UIImageView!
    
    func loadImages() {
        loadResourceToImage("ensure_bg_page_13", imgBg)
    }
    
    func resetAnimation() {
        
    }
    
    func startAnimation() {
        
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        loadImages()
    }
}
