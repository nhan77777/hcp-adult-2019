//
//  PushFeedback.swift
//  HCP Adult
//
//  Created by Long Quách Phi on 7/11/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import UIKit

import Foundation

class PushFeedback {
    func params(_ hcpSurvey : HCPSurvey) -> HCPSurvey{
        var getHCPSurvey = HCPSurvey(JSONString: "{}")!
        getHCPSurvey = hcpSurvey
        return getHCPSurvey
    }
    
    func pushFeedback(hcpSurvey : HCPSurvey, callBack:@escaping (Bool, ReturnDurationModel) -> Void){
        NetworkService.post(endPoint: API.feedback, requestBody: params(hcpSurvey).toJSON()) { success,response in
            if let data = ReturnDurationModel(JSONString: response) {
                callBack(success, data)
            } else {
                callBack(false, ReturnDurationModel(JSONString: "{}")!)
            }
        }
    }
}
