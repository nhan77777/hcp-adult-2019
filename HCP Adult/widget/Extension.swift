//
//  Effect.swift
//  HCP Adult
//
//  Created by Long Quách Phi on 5/24/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import UIKit
import Nuke
import PopupDialog

extension UIViewController {
    
    func showAlertWith(message: String, callBackPositive: @escaping (()->()) = {}){
        let vc = MessagerDialogVC(nibName: "MessagerDialogVC", bundle: nil)
        vc.messenge = message
        vc.isAlert = true
        vc.callbackPositive = { (_) in
            callBackPositive()
        }
        let popup = PopupDialog(viewController: vc, buttonAlignment: .vertical, transitionStyle: .bounceUp, preferredWidth: 450, tapGestureDismissal: false, panGestureDismissal: false, hideStatusBar: false, completion: nil)
        present(popup, animated: true, completion: nil)
    }
    
    func getVersionString() -> String{
        return "\(Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String)"
    }
    
    func getAppidString() -> String{
        return "210001"
    }
    
    func showPDFView(filename : String){
        let vc = PDFViewController(nibName: "PDFViewController", bundle: nil)
        vc.fileName = filename
        present(vc, animated: true, completion: nil)
    }
    
    func showSubSlide(_ controllers : [BaseDetailVC], _ parent : PageView){
        let vc = SubSliderVC(nibName: "SubSliderVC", bundle: nil)
        vc.orderedViewControllers = controllers
        vc.vcParent = parent
        parent.pauseTimer()
        present(vc, animated: true, completion: nil)
    }
    
    func loadResourceToImage(_ resourceName : String, _ image : UIImageView){
        let path = Bundle.main.url(forResource: resourceName, withExtension: "png")
        Nuke.loadImage(with: path!, into: image)
    }
    
    func showHightlight(content : UIImageView, color : UIColor, radius : CGFloat){
        content.layer.shadowColor = color.cgColor
        content.layer.shadowOpacity = 1
        content.layer.shadowOffset = CGSize.zero
        content.layer.shadowRadius = radius
        content.layer.shouldRasterize = true
    }
    
    func twinkleImageView(_ content : UIView){
        let viewTwinkle = UIView()
        viewTwinkle.frame = CGRect(x: content.frame.origin.x, y: content.frame.origin.y, width: content.bounds.size.width, height: content.bounds.size.height)
        view.addSubview(viewTwinkle)
    }
    
    func rotateView(_ content : UIView, _ time : Double){
        var rotationAnimation = CABasicAnimation()
        rotationAnimation = CABasicAnimation.init(keyPath: "transform.rotation.z")
        rotationAnimation.toValue = NSNumber(value: (Double.pi * 2.0))
        rotationAnimation.duration = time
        rotationAnimation.isCumulative = true
        rotationAnimation.repeatCount = .infinity
        content.layer.add(rotationAnimation, forKey: "rotationAnimation")
    }
    
    func boucingView(_ content : UIView, _ scale : CGFloat, _ time : Double){
        content.transform = CGAffineTransform(scaleX: scale, y: scale)
        UIView.animate(withDuration: time, delay: 0.0, usingSpringWithDamping: 0.2, initialSpringVelocity: 6.0, options: [], animations: {
            content.transform = CGAffineTransform.identity
        }, completion: nil)
    }
    
    func boucingViewForever(_ content : UIView, _ scale : CGFloat){
        content.transform = CGAffineTransform(scaleX: scale, y: scale)
        UIView.animate(withDuration: 1.5, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1.5, options: [.repeat, .autoreverse, .allowUserInteraction], animations: {
            content.transform = CGAffineTransform.identity
        }, completion: nil)
    }
    
}

extension UIImage {
    
    func mask(with color: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        defer {
            UIGraphicsEndImageContext()
        }
        guard let context = UIGraphicsGetCurrentContext() else {
            return self
        }
        context.translateBy(x: 0, y: self.size.height)
        context.scaleBy(x: 1.0, y: -1.0)
        context.setBlendMode(.normal)
        let rect = CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height)
        guard let mask = self.cgImage else {
            return self
        }
        context.clip(to: rect, mask: mask)
        color.setFill()
        context.fill(rect)
        guard let newImage = UIGraphicsGetImageFromCurrentImageContext() else {
            return self
        }
        return newImage
    }
    
}

extension CALayer {
    
    func addBorder(edge: UIRectEdge, color: UIColor, thickness: CGFloat, opacity: Float = 1) {
        let border = CALayer()
        border.opacity = opacity
        switch edge {
        case .top:
            border.frame = CGRect(x: 0, y: 0, width: frame.width, height: thickness)
        case .bottom:
            border.frame = CGRect(x: 0, y: frame.height - thickness, width: frame.width, height: thickness)
        case .left:
            border.frame = CGRect(x: 0, y: 0, width: thickness, height: frame.height)
        case .right:
            border.frame = CGRect(x: frame.width - thickness, y: 0, width: thickness, height: frame.height)
        default:
            break
        }
        border.backgroundColor = color.cgColor;
        addSublayer(border)
    }
    
}

