//
//  EnsureMenuVC.swift
//  HCP Adult
//
//  Created by Long Quách Phi on 5/25/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import UIKit
import Pageboy

class EnsureMenuVC: UIViewController {
    
    private let periodTime = 0.35
    
    public var homeController : PageView?
    public var targetFlowVC : UIViewController?
    public var targetFlowName : String?
    public var targetFlow : Int?
    @IBOutlet var changeFlowButton: [UIButton]!
    
    var page1: EnsurePage1VC = {
        let page = EnsurePage1VC(nibName: "EnsurePage1VC", bundle: nil)
        (page as BaseDetailVC).pageID = "page67"
        return page
    }()
    
    var page2a: EnsurePage2aVC = {
        let page = EnsurePage2aVC(nibName: "EnsurePage2aVC", bundle: nil)
        (page as BaseDetailVC).pageID = "page68"
        return page
    }()
    
    var page2b: EnsurePage2bVC = {
        let page = EnsurePage2bVC(nibName: "EnsurePage2bVC", bundle: nil)
        (page as BaseDetailVC).pageID = "page70"
        return page
    }()
    
    var page2c: EnsurePage2cVC = {
        let page = EnsurePage2cVC(nibName: "EnsurePage2cVC", bundle: nil)
        (page as BaseDetailVC).pageID = "page72"
        return page
    }()
    
    var page3a: EnsurePage3aVC = {
        let page = EnsurePage3aVC(nibName: "EnsurePage3aVC", bundle: nil)
        (page as BaseDetailVC).pageID = "page69"
        return page
    }()
    
    var page3b: EnsurePage3bVC = {
        let page = EnsurePage3bVC(nibName: "EnsurePage3bVC", bundle: nil)
        (page as BaseDetailVC).pageID = "page71"
        return page
    }()
    
    var page3c: EnsurePage3cVC = {
        let page = EnsurePage3cVC(nibName: "EnsurePage3cVC", bundle: nil)
        (page as BaseDetailVC).pageID = "page73"
        return page
    }()
    
    var page4a: EnsurePage4aVC = {
        let page = EnsurePage4aVC(nibName: "EnsurePage4aVC", bundle: nil)
        (page as BaseDetailVC).pageID = "page74"
        return page
    }()
    
    var page4b: EnsurePage4bVC = {
        let page = EnsurePage4bVC(nibName: "EnsurePage4bVC", bundle: nil)
        (page as BaseDetailVC).pageID = "page75"
        return page
    }()
    
    var page4c: EnsurePage4cVC = {
        let page = EnsurePage4cVC(nibName: "EnsurePage4cVC", bundle: nil)
        (page as BaseDetailVC).pageID = "page76"
        return page
    }()
    
    var page5: EnsurePage5VC = {
        let page = EnsurePage5VC(nibName: "EnsurePage5VC", bundle: nil)
        (page as BaseDetailVC).pageID = "page77"
        return page
    }()
    
    var page6: EnsurePage6VC = {
        let page = EnsurePage6VC(nibName: "EnsurePage6VC", bundle: nil)
        (page as BaseDetailVC).pageID = "page78"
        return page
    }()
    
    var page7: EnsurePage7VC = {
        let page = EnsurePage7VC(nibName: "EnsurePage7VC", bundle: nil)
        (page as BaseDetailVC).pageID = "page79"
        return page
    }()
    
    var page8: EnsurePage8VC = {
        let page = EnsurePage8VC(nibName: "EnsurePage8VC", bundle: nil)
        (page as BaseDetailVC).pageID = "page80"
        return page
    }()
    
    var page9: EnsurePage9VC = {
        let page = EnsurePage9VC(nibName: "EnsurePage9VC", bundle: nil)
        (page as BaseDetailVC).pageID = "page81"
        return page
    }()
    
    var page10: EnsurePage10VC = {
        let page = EnsurePage10VC(nibName: "EnsurePage10VC", bundle: nil)
        (page as BaseDetailVC).pageID = "page81"
        return page
    }()
    
    var page11a: EnsurePage11aVC = {
        let page = EnsurePage11aVC(nibName: "EnsurePage11aVC", bundle: nil)
        (page as BaseDetailVC).pageID = "page82"
        return page
    }()
    
    var page11b: EnsurePage11bVC = {
        let page = EnsurePage11bVC(nibName: "EnsurePage11bVC", bundle: nil)
        (page as BaseDetailVC).pageID = "page83"
        return page
    }()
    
    var page11c: EnsurePage11cVC = {
        let page = EnsurePage11cVC(nibName: "EnsurePage11cVC", bundle: nil)
        (page as BaseDetailVC).pageID = "page84"
        return page
    }()
    
    var page12: EnsurePage12VC = {
        let page = EnsurePage12VC(nibName: "EnsurePage12VC", bundle: nil)
        (page as BaseDetailVC).pageID = "page85"
        return page
    }()
    
    var page13: EnsurePage13VC = {
        let page = EnsurePage13VC(nibName: "EnsurePage13VC", bundle: nil)
        (page as BaseDetailVC).pageID = "page86"
        return page
    }()
    
    var page14: EnsurePage14VC = {
        let page = EnsurePage14VC(nibName: "EnsurePage14VC", bundle: nil)
        (page as BaseDetailVC).pageID = "page87"
        return page
    }()
    
    var page15: EnsurePage15VC = {
        let page = EnsurePage15VC(nibName: "EnsurePage15VC", bundle: nil)
        (page as BaseDetailVC).pageID = "page88"
        return page
    }()
    
    var page16a: EnsurePage16aVC = {
        let page = EnsurePage16aVC(nibName: "EnsurePage16aVC", bundle: nil)
        (page as BaseDetailVC).pageID = "page89"
        return page
    }()
    
    var page16b: EnsurePage16bVC = {
        let page = EnsurePage16bVC(nibName: "EnsurePage16bVC", bundle: nil)
        (page as BaseDetailVC).pageID = "page90"
        return page
    }()
    
    var page16c: EnsurePage16cVC = {
        let page = EnsurePage16cVC(nibName: "EnsurePage16cVC", bundle: nil)
        (page as BaseDetailVC).pageID = "page91"
        return page
    }()
    
    var page17: EnsurePage17VC = {
        let page = EnsurePage17VC(nibName: "EnsurePage17VC", bundle: nil)
        (page as BaseDetailVC).pageID = "page92"
        return page
    }()
    
    var page18: EnsurePage18VC = {
        let page = EnsurePage18VC(nibName: "EnsurePage18VC", bundle: nil)
        (page as BaseDetailVC).pageID = "page93"
        return page
    }()
    
    var pageTaiLieu: EnsurePageTaiLieu = {
        let page = EnsurePageTaiLieu(nibName: "EnsurePageTaiLieu", bundle: nil)
        (page as BaseDetailVC).pageID = "page94"
        return page
    }()
    
    
    //    @IBAction func onButton1Section1Tap(_ sender: Any) {
    //        homeController?.setTransition(.init(style: .fade, duration: periodTime))
    //        dismiss(animated: true, completion: {
    //            self.homeController?.moveToPage(index: 1)
    //        })
    //    }
    
    @IBAction func onSection2_1Tap(_ sender: Any) {
        
        showSubSlide([page1, page2a, page3a, page4a, page5, page6, page10, page8, page7, page11c, page12],homeController!)
    }
    
    @IBAction func onSection2_2Tap(_ sender: Any) {
        
        showSubSlide([page1, page2a, page3a, page4a, page5, page6, page8, page7, page11b, page12],homeController!)
    }
    ///
    @IBAction func onSection2_3Tap(_ sender: Any) {
        
        showSubSlide([page1, page2a, page3a, page4a, page5, page6, page7, page8, page11a, page12],homeController!)
    }
    @IBAction func onSection3_1Tap(_ sender: Any) {
        
        
        showSubSlide([page1, page2b, page3b, page4b, page5, page6, page10, page8, page7, page11c, page12],homeController!)
    }
    
    @IBAction func onSection3_2Tap(_ sender: Any) {
        
        showSubSlide([page1, page2b, page3b, page4b, page5, page6, page9, page7, page11b, page12],homeController!)
    }
    
    @IBAction func onSection3_3Tap(_ sender: Any) {
        
        showSubSlide([page1, page2b, page3b, page4b, page5, page6, page7, page11a],homeController!)
    }
    
    @IBAction func onSection4Tap(_ sender: Any) {
        
        showSubSlide([page1, page2c, page3c, page4c, page5, page6, page10, page8, page7, page11c, page12],homeController!)
        
    }
    
    @IBAction func onSection5_1Tap(_ sender: Any) {
        
        showSubSlide([page2a, page3a],homeController!)
    }
    
    @IBAction func onSection5_2Tap(_ sender: Any) {
        
        showSubSlide([page2b, page3b], homeController!)
    }
    
    @IBAction func onSection5_3Tap(_ sender: Any) {
        
        showSubSlide([page2c, page3c], homeController!)
        
    }
    @IBAction func onSection6_1Tap(_ sender: Any) {
        
        showSubSlide([page4a], homeController!)
        
    }
    
    @IBAction func onSection6_2Tap(_ sender: Any) {
        
        showSubSlide([page4b], homeController!)
    }
    
    @IBAction func onSection6_3Tap(_ sender: Any) {
        
        showSubSlide([page4c], homeController!)
        
    }
    
    @IBAction func onSection7Tap(_ sender: Any) {
        
        showSubSlide([page5], homeController!)
    }
    
    @IBAction func onSection8Tap(_ sender: Any) {
        
        showSubSlide([page6], homeController!)
    }
    
    @IBAction func onSection9Tap(_ sender: Any) {
        
        showSubSlide([page7], homeController!)
    }
    @IBAction func onSection10Tap(_ sender: Any) {
        
        showSubSlide([page8], homeController!)
    }
    @IBAction func onSection11Tap(_ sender: Any) {
        
        showSubSlide([page9], homeController!)
    }
    
    @IBAction func onSection12Tap(_ sender: Any) {
        
        showSubSlide([page10], homeController!)
    }
    
    @IBAction func onSection13_1Tap(_ sender: Any) {
        
        showSubSlide([page11a], homeController!)
    }
    
    @IBAction func onSection13_2Tap(_ sender: Any) {
        
        showSubSlide([page11b], homeController!)
    }
    
    @IBAction func onSection13_3Tap(_ sender: Any) {
        
        showSubSlide([page11c], homeController!)
    }
    
    
    @IBAction func onSection14Tap(_ sender: Any) {
        
        showSubSlide([page12], homeController!)
    }
    
    @IBAction func onSection15_1Tap(_ sender: Any) {
        
        showSubSlide([page13], homeController!)
    }
    @IBAction func onSection15_2Tap(_ sender: Any) {
        
        showSubSlide([page14], homeController!)
    }
    
    @IBAction func onSection15_3Tap(_ sender: Any) {
        
        showSubSlide([page15], homeController!)
    }
    
    @IBAction func onSection16_1Tap(_ sender: Any) {
        
        homeController?.pauseTimer()
        let vc = EnsurePDFViewController(nibName: "EnsurePDFViewController", bundle: nil)
        vc.homeController = homeController
        vc.pdfFiles.append("thanhphan_ensure_gold")
        vc.navTitle = "BẢNG THÀNH PHẦN"
        present(vc, animated: true, completion: nil)
    }
    
    
    
    @IBAction func onSection16_2Tap(_ sender: Any) {
        
        homeController?.pauseTimer()
        let vc = EnsurePDFViewController(nibName: "EnsurePDFViewController", bundle: nil)
        vc.homeController = homeController
        vc.pdfFiles.append("thanhphan_ensure_plus")
        vc.navTitle = "BẢNG THÀNH PHẦN"
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func onSection16_3Tap(_ sender: Any) {
        
        homeController?.pauseTimer()
        let vc = EnsurePDFViewController(nibName: "EnsurePDFViewController", bundle: nil)
        vc.homeController = homeController
        vc.pdfFiles.append("thanhphan_ensure_vital")
        vc.navTitle = "BẢNG THÀNH PHẦN"
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func onSection17_1Tap(_ sender: Any) {
        
        homeController?.pauseTimer()
        let vc = EnsurePDFViewController(nibName: "EnsurePDFViewController", bundle: nil)
        vc.homeController = homeController
        vc.pdfFiles.append("ensure_pdf_page_17")
        vc.navTitle = "NGHIÊN CỨU LÂM SÀNG"
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func onSection17_2Tap(_ sender: Any) {
        homeController?.pauseTimer()
        let vc = EnsurePDFViewController(nibName: "EnsurePDFViewController", bundle: nil)
        vc.homeController = homeController
        vc.pdfFiles.append("ensure_pdf_page_18")
        vc.navTitle = "NGHIÊN CỨU LÂM SÀNG"
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func onSection17_4Tap(_ sender: Any) {
        showSubSlide([pageTaiLieu], homeController!)
    }
    
    @IBAction func onGotoOtherFlow(_ sender: UIButton) {
        homeController?.pauseTimer()
        switch sender.tag {
            
        case 0 :
            let vc = Page0VC.instantiate(appStoryboard: .Main)
            vc.isHidenTyp2 = false
            targetFlowVC = vc
            break
        case 1 :
            targetFlowVC =  ProsureHomeVC.instantiate(appStoryboard: .Main)
            break
        default:
            break
        }
        present(targetFlowVC!, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupFlowChange()
        
    }
    
    func setupFlowChange() {
        for i in 0...changeFlowButton.count - 1{
            changeFlowButton[i].tag = i
            
            switch i {
                
            case 0 :
                targetFlowName = "CHUYỂN ĐẾN GLUCERNA"
                changeFlowButton[i].backgroundColor = UIColor(hexString: "#e76baa")
                break
            case 1 :
                targetFlowName = "CHUYỂN ĐẾN PROSURE"
                changeFlowButton[i].backgroundColor = UIColor(hexString: "#8d65a5")
                break
                
            default:
                targetFlowName = "Untarget"
                break
            }
            
            changeFlowButton[i].setTitle(targetFlowName!, for: .normal)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
}
