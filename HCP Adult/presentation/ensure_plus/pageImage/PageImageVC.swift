//
//  PageImageVC.swift
//  Pediatric
//
//  Created by Bình Phạm on 9/4/18.
//  Copyright © 2018 Fractal. All rights reserved.
//

import UIKit
import Async

class PageImageVC: BaseDetailVC, DetailView {
    
    @IBOutlet weak var imgBg: UIImageView!
    func loadImages() {
        loadResourceToImage(imgName!, imgBg)
    }
    
    func resetAnimation() {
        if pageID.elementsEqual("page59") {
            imgEnsureGold?.layer.removeAllAnimations()
            imgEnsurePlus?.layer.removeAllAnimations()
        }
    }
    
    func startAnimation() {
        if pageID.elementsEqual("page59") {
            imgEnsureGold?.twinkle()
            boucingViewForever(imgEnsureGold!, 0.9)
            
            imgEnsurePlus?.twinkle()
            boucingViewForever(imgEnsurePlus!, 0.9)
        }
        
    }
    
    public var imgName : String?
    var imgEnsureGold: UIImageView?
    var imgEnsurePlus: UIImageView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let _ = imgName {
            loadImages()
        }
        
        if pageID.elementsEqual("page59") {
            
            imgEnsureGold = UIImageView(frame: CGRect(x: 520, y: 253, width: 120, height: 225))
            imgEnsurePlus = UIImageView(frame: CGRect(x: 40, y: 255, width: 80, height: 200))
            
            loadResourceToImage("ic_ensure_gold_page59", imgEnsureGold!)
            loadResourceToImage("ic_ensure_plus_page59", imgEnsurePlus!)
            
            imgEnsureGold!.contentMode = .scaleAspectFit
            
            view.addSubview(imgEnsureGold!)
            view.bringSubviewToFront(imgEnsureGold!)
            
            
            imgEnsurePlus!.contentMode = .scaleAspectFit
            view.addSubview(imgEnsurePlus!)
            view.bringSubviewToFront(imgEnsurePlus!)
            
            let height = 100
            let width = 500
            
            let imgContentBelow1 = UIImageView(frame: CGRect(x: 47, y: 632, width: width, height: height))
            loadResourceToImage("ic_ensure_plus_below_1_page6", imgContentBelow1)
            
            imgContentBelow1.contentMode = .scaleAspectFit
            view.addSubview(imgContentBelow1)
            view.bringSubviewToFront(imgContentBelow1)
            
            let imgContentBelow2 = UIImageView(frame: CGRect(x: 463, y: 616, width: width + 50, height: height + 11))
            loadResourceToImage("ic_ensure_plus_below_2_page6", imgContentBelow2)
            
            imgContentBelow2.contentMode = .scaleAspectFit
            view.addSubview(imgContentBelow2)
            view.bringSubviewToFront(imgContentBelow2)
            
            imgContentBelow1.isUserInteractionEnabled = true
            imgContentBelow2.isUserInteractionEnabled = true
            
            let tapGesture1 = UITapGestureRecognizer(target: self, action: #selector(onImageTap(_:)))
            let tapGesture2 = UITapGestureRecognizer(target: self, action: #selector(onImageTap(_:)))
            tapGesture1.numberOfTapsRequired = 1
            tapGesture2.numberOfTapsRequired = 1
            imgContentBelow1.addGestureRecognizer(tapGesture1)
            imgContentBelow2.addGestureRecognizer(tapGesture2)
            
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        
        if pageID.elementsEqual("page59") {
            resetAnimation()
        }
        
    }
    override func viewDidAppear(_ animated: Bool) {
        
        if pageID.elementsEqual("page59") {
            Async.main(after: 0.1){
                self.startAnimation()
            }
        }
    }
    
    
    @objc func onImageTap(_ sender: UITapGestureRecognizer) {
    
        guard let tappedImage = sender.view as? UIImageView else {return}
        
       
        UIView.animateKeyframes(withDuration: 0.5, delay: 0.0, options: .allowUserInteraction, animations: {
            UIView.addKeyframe(withRelativeStartTime: 0.0, relativeDuration: 0.25, animations: {
                tappedImage.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
            })
            UIView.addKeyframe(withRelativeStartTime: 0.25, relativeDuration: 0.25, animations: {
                tappedImage.transform = CGAffineTransform.identity
            })
        }, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
