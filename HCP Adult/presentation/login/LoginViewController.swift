//
//  LoginViewController.swift
//  HCP Adult
//
//  Created by Long Quách Phi on 5/31/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import UIKit
import PopupDialog
import Async
import SwiftSpinner

class LoginViewController: UIViewController {
    
    @IBOutlet var imgIC: [UIImageView]!
    @IBOutlet weak var imgbg: UIImageView!
    @IBOutlet weak var txtFieldID: UITextField!
    @IBOutlet weak var txtFieldPass: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    var userid = ""
    
    fileprivate let presenter = LoginPresenter(loginService: Login())
    
    @IBAction func onMenuTap(_ sender: Any) {
        present(ChangeServerVC(nibName: "ChangeServerVC", bundle: nil), animated: true, completion: nil)
    }
    
    @IBAction func abtnLogin() {
        userid = txtFieldID.text!
        presenter.doLogin(id: txtFieldID.text!, password: txtFieldPass.text!)
        
    }
    
    func loadImage(){
        loadResourceToImage("bg_page_login", imgbg)
        loadResourceToImage("ic_page_login_0", imgIC[0])
        loadResourceToImage("ic_page_login_1", imgIC[1])
    }
    
    func repair(){
        txtFieldPass.placeholder = "Mật Khẩu"
        txtFieldPass.attributedPlaceholder = NSAttributedString(string: txtFieldPass.placeholder!, attributes: [NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)])
        txtFieldID.placeholder = "ID Nhân Viên"
        txtFieldID.attributedPlaceholder = NSAttributedString(string: txtFieldID.placeholder!, attributes: [NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)])
        navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(self)
        repair()
        loadImage()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}

extension LoginViewController : LoginView {
    
    func showLoading(_ message: String) {
        SwiftSpinner.show(message)
    }
    
    func hideLoading() {
        SwiftSpinner.hide()
    }
    
    func loginSuccess(_ data: LoginModel) {
        let userdata = UserData(JSONString: "{}")
        userdata?.userid = userid
        userdata?.sysid = data.sysid!
        userdata?.name = data.name!
        Utils.saveToCache(key: CacheKey.userdata, value: (userdata?.toJSONString()!)!)
        let mh = ChoiceHCPVC(nibName: "ChoiceHCPVC", bundle: nil)
        present(mh, animated: true, completion: nil)
    }
    
    func loginFailed(_ meassage : String) {
        let vc = MessagerDialogVC(nibName: "MessagerDialogVC", bundle: nil)
        vc.messenge = meassage
        let containerAppearance = PopupDialogContainerView.appearance()
        containerAppearance.cornerRadius = 15
        let popup = PopupDialog(viewController: vc, buttonAlignment: .vertical, transitionStyle: .bounceUp, preferredWidth: 450, tapGestureDismissal: true, hideStatusBar: false, completion: nil)
        present(popup, animated: true, completion: nil)
        
    }
    
    
}
