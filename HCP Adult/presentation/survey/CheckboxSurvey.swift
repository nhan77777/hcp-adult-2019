//
//  CheckboxSurvey.swift
//  HCP Adult
//
//  Created by Bình Phạm on 6/11/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import UIKit
import M13Checkbox

class CheckboxSurvey: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var mainStackView: UIStackView!
    public var questions : [String] = []
    public var checkboxs : [CheckboxView] = []
    public var callback : ((Int, Bool)->())?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit(){
        Bundle.main.loadNibNamed("CheckboxSurvey", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
    public func setQuestions(_ questions : [String]){
        self.questions = questions
        checkboxs = []
        for i in 0...questions.count-1 {
            let check = CheckboxView(frame: CGRect(x: 0, y: 0, width: mainStackView.bounds.width, height: 0))
            check.lbTitle.text = questions[i]
            check.checkBox.tag = i
            check.checkBox.addTarget(self, action: #selector(checkboxValueChanged(_:)), for: .valueChanged)
            checkboxs.append(check)
            mainStackView.addArrangedSubview(check)
        }
    }
    
    @objc func checkboxValueChanged(_ sender: M13Checkbox) {
        if let call = callback {
            call(sender.tag, sender.checkState == M13Checkbox.CheckState.checked)
        }
    }
    
}
