//
//  Page2VC.swift
//  Glucerna
//
//  Created by Long Quách Phi on 3/22/18.
//  Copyright © 2018 Fractal. All rights reserved.
//

import UIKit

class Page11VC: BaseDetailVC, DetailView {
    
    @IBOutlet weak var imgTopBar: UIImageView!
    @IBOutlet var viewActionICContent: [UIView]!
    @IBOutlet weak var imgBackground: UIImageView!
    
    func loadImages() {
        loadResourceToImage("glu_bg_page11", imgBackground)
        loadResourceToImage("glu_ic_page11", imgTopBar)
    }
    
    func resetAnimation() {
    }
    
    func startAnimation() {
        for i in 0...viewActionICContent.count-1 {
            viewActionICContent[i].addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onViewTap(_:))))
            viewActionICContent[i].isUserInteractionEnabled = true
            viewActionICContent[i].tag = i
        }
    }
    
    @objc func onViewTap(_ sender : UITapGestureRecognizer){
        let index = (sender.view?.tag)!
        let filename = "glu_\(index)"
        showPDFView(filename: filename)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pageID = "page40"
        loadImages()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        resetAnimation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        startAnimation()
    }
    
}

