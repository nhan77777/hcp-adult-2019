//
//  Section1Page4VC.swift
//  HCP Adult
//
//  Created by Long Quách Phi on 6/8/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import UIKit

class Section1Page4VC: BaseDetailVC, DetailView {
    
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet var viewFooter: [UIView]!
    @IBOutlet weak var viewTwinkle:  UIImageView!
    
    func loadImages() {
        loadResourceToImage("glu_bg_section1_page4", imgBackground)
        loadResourceToImage("glu_ic_milkbottle_mini", viewTwinkle)
    }
    
    func resetAnimation() {
    }
    
    func startAnimation() {
        viewTwinkle.twinkle()
    }
    
    func setupActionFooter(){
        for i in 0...viewFooter.count - 1{
            viewFooter[i].addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onViewTapFooter(_:))))
            viewFooter[i].isUserInteractionEnabled = true
        }
    }
    
    @objc func onViewTapFooter(_ sender : UITapGestureRecognizer){
        showPDFView(filename: "glu_2")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pageID = "page29"
        loadImages()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        resetAnimation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        startAnimation()
    }
    
}

