//
//  d.swift
//  HCP Adult
//
//  Created by Long Quách Phi on 7/3/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

class PushDuration{
    public static let ALL_PAGE_COUNT = 94
    public static let ENSURE_PAGE_COUNT = 24
    public static let GLUCERNA_PAGE_COUNT = 17
    public static let PROSURE_PAGE_COUNT = 12
    
    func params(durationRequestModel: Dictionary<String, Any>) -> Parameters{
        let param:Parameters?
        param = durationRequestModel
        return param!
    }
 
    func pushDuration(durationRequestModel: Dictionary<String, Any>, callBack:@escaping (Bool, ReturnDurationModel) -> Void){
        NetworkService.post(endPoint: API.callDuration, requestBody: params(durationRequestModel: durationRequestModel)) { success,response in
            if let data = ReturnDurationModel(JSONString: response) {
                callBack(success, data)
            } else {
                callBack(false, ReturnDurationModel(JSONString: "{}")!)
            }
        }
    }
    
}
