//
//  page0VC.swift
//  Glucerna
//
//  Created by Long Quách Phi on 3/22/18.
//  Copyright © 2018 Fractal. All rights reserved.
//

import UIKit
import Pageboy
import Nuke
import SideMenu
import PopupDialog
import Async

class Page0VC: PageboyViewController, PageView {
    func onChangeFlow(vc: UIViewController) {
        present(vc, animated: true, completion: nil)
    }
    
    func onRequestEndCall() {
        let popup = PopupDialog(viewController: vc!, buttonAlignment: .vertical, transitionStyle: .bounceUp, preferredWidth: 450, tapGestureDismissal: true, hideStatusBar: false, completion: nil)
        self.present(popup, animated: true, completion: nil)
    }
    
    
    @IBOutlet weak var imgMenuBack: UIImageView!
    @IBOutlet weak var imgMenuDetail: UIImageView!
    @IBOutlet weak var imgClose: UIImageView!

    var isHidenTyp2 = false
    var pageWhileColor = 11

    private var currentPageIndex = 0
    var timer = Timer()
    private var vc : ConfirmDialogVC?
    private var durationRecord : DurationRecord?

    
    func initialPopupDialog(){
        vc = ConfirmDialogVC(nibName: "ConfirmDialogVC", bundle: nil)
        vc?.negativeText = "Quay lại"
        vc?.positiveText = "Kết thúc"
        vc?.messenge = "Bạn có chắc chắn muốn kết thúc trò chuyện ?"
        vc?.callbackPositive = {vc in
            self.pauseTimer()
            self.present(SurveyVC(nibName: "SurveyVC", bundle: nil), animated: true, completion: nil)
        }
        vc?.callbackNegative = {vc in
        }
    }
    
    func updateEndTime(){
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
        let endTime = formatter.string(from: Date())
        durationRecord?.endtime = endTime
    }
    
    @objc func actionTimer(){
        print(orderedViewControllers[currentPageIndex].pageID)
        durationRecord?.durations[orderedViewControllers[currentPageIndex].pageID] = (durationRecord?.durations[orderedViewControllers[currentPageIndex].pageID])! + 1
        if let record = durationRecord {
            Utils.saveLastDuration(duration: record)
        }
    }
    
    func startTimer(){
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(actionTimer), userInfo: nil, repeats: true)
    }
    
    func pauseTimer(){
        timer.invalidate()
    }
    
    @objc func onMenuTap(_ sender : UITapGestureRecognizer) {
        if currentIndex == 0{
            present(customSideMenuManager.menuLeftNavigationController! , animated: true, completion: nil)
        }
        else{
            moveToPage(index: 0)
        }
    }
    
    @objc func onMenuDetailTap(_ sender : UITapGestureRecognizer) {
        present(customSideMenuManager.menuLeftNavigationController! , animated: true, completion: nil)
    }
    
    @objc func onCloseTap(){
        let containerAppearance = PopupDialogContainerView.appearance()
        containerAppearance.cornerRadius = 15
        let popup = PopupDialog(viewController: vc!, buttonAlignment: .vertical, transitionStyle: .bounceUp, preferredWidth: 450, tapGestureDismissal: true, hideStatusBar: false, completion: nil)
        self.present(popup, animated: true, completion: nil)
    }
    
    func setActionImgMenuBack(){
        imgMenuBack.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onMenuTap(_:))))
        imgMenuDetail.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onMenuDetailTap(_:))))
        imgClose.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onCloseTap)))
    }
    
    let customSideMenuManager = SideMenuManager()
    
    func setSideMenu(){
        let sideMenuVC = MenuVC(nibName: "MenuVC", bundle: nil) as MenuVC
        sideMenuVC.detailViewDelagete = self
        sideMenuVC.targetFlow = Utils.getFlowableIndex(1)[0]
        sideMenuVC.isHidenTyp2 = isHidenTyp2
        let menuLeftNav = UISideMenuNavigationController(rootViewController: sideMenuVC)
        customSideMenuManager.menuLeftNavigationController = menuLeftNav
        customSideMenuManager.menuShadowColor = #colorLiteral(red: 0.6264399886, green: 0.06424147636, blue: 0.4137963057, alpha: 1)
        customSideMenuManager.menuAllowPushOfSameClassTwice = false
        customSideMenuManager.menuPresentMode = .menuSlideIn
        customSideMenuManager.menuAnimationPresentDuration = 0.5
        customSideMenuManager.menuAnimationDismissDuration = 0.5
        customSideMenuManager.menuAnimationBackgroundColor = UIColor.clear
        customSideMenuManager.menuWidth = 567
        customSideMenuManager.menuEnableSwipeGestures = true
    }
    
    var orderedViewControllers: [BaseDetailVC] = {
        [
            Page1VC(nibName: "Page1VC", bundle: nil),
            Section1Page1VC(nibName: "Section1Page1VC", bundle: nil),
            Section1Page2VC(nibName: "Section1Page2VC", bundle: nil),
            Section1Page3VC(nibName: "Section1Page3VC", bundle: nil),
            Section1Page4VC(nibName: "Section1Page4VC", bundle: nil),
            Section1Page5VC(nibName: "Section1Page5VC", bundle: nil),
            Page2VC(nibName: "Page2VC", bundle: nil),
            Page3VC(nibName: "Page3VC", bundle: nil),
            Page4VC(nibName: "Page4VC", bundle: nil),
            Page5VC(nibName: "Page5VC", bundle: nil),
            Page6VC(nibName: "Page6VC", bundle: nil),
            Page7VC(nibName: "Page7VC", bundle: nil),
            Page8VC(nibName: "Page8VC", bundle: nil),
            Page9VC(nibName: "Page9VC", bundle: nil),
            Page10VC(nibName: "Page10VC", bundle: nil),
            Page11VC(nibName: "Page11VC", bundle: nil),
            Page12VC(nibName: "Page12VC", bundle: nil)
        ]
    }()
    
    func moveToPage(index: Int) {
        dataSource = self
        isInfiniteScrollEnabled = false
        scrollToPage(Page.at(index: index), animated: true)
    }
    
    func setTransition(_ trans: PageboyViewController.Transition) {
        transition = trans
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialPopupDialog()
        dataSource = self
        delegate = self
        transition = Transition(style: .fade, duration: 0.0)
        setActionImgMenuBack()
        setImgMask(imgMenuDetail, UIImage(named: "glu_ic_hamberger.png")!, UIColor(hexString: "#AF1D7E")!)
        setImgMask(imgClose, UIImage(named: "ic_x.png")!, UIColor(hexString: "#AF1D7E")!)
        imgMenuBack.isHidden = true
        setSideMenu()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setImgMask(_ imgv : UIImageView,_ image : UIImage,_ color : UIColor){
        imgv.image=image.mask(with: color)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setColor()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        durationRecord = Utils.getLastDuration()
        pauseTimer()
        startTimer()
        view.gestureRecognizers?.removeAll()
    }

    func setColor(){
        if isHidenTyp2{
            setImgMask(imgMenuBack, UIImage(named: "glu_ic_hamberger.png")!, UIColor.white)
        }
        else{
            setImgMask(imgMenuBack, UIImage(named: "glu_ic_hamberger.png")!, UIColor(hexString: "#AF1D7E")!)
        }
    }
    
}

extension Page0VC : UISideMenuNavigationControllerDelegate {
    
    func sideMenuWillAppear(menu: UISideMenuNavigationController, animated: Bool) {
    }
    
    func sideMenuDidAppear(menu: UISideMenuNavigationController, animated: Bool) {
    
    }
    
    func sideMenuWillDisappear(menu: UISideMenuNavigationController, animated: Bool) {
    }
    
    func sideMenuDidDisappear(menu: UISideMenuNavigationController, animated: Bool) {
    }
    
}

extension Page0VC : PageboyViewControllerDataSource,PageboyViewControllerDelegate {
    
    func pageboyViewController(_ pageboyViewController: PageboyViewController, didScrollTo position: CGPoint, direction: PageboyViewController.NavigationDirection, animated: Bool) {
        
    }
    
    func pageboyViewController(_ pageboyViewController: PageboyViewController, didReloadWith currentViewController: UIViewController, currentPageIndex: PageboyViewController.PageIndex) {
        
    }
    
    
    func numberOfViewControllers(in pageboyViewController: PageboyViewController) -> Int {
        return orderedViewControllers.count
    }
    
    func viewController(for pageboyViewController: PageboyViewController, at index: PageboyViewController.PageIndex) -> UIViewController? {
        return orderedViewControllers[index]
    }
    
    func defaultPage(for pageboyViewController: PageboyViewController) -> PageboyViewController.Page? {
        return nil
    }
    
    func pageboyViewController(_ pageboyViewController: PageboyViewController, didScrollToPageAt index: Int, direction: PageboyViewController.NavigationDirection, animated: Bool) {
        if  index == pageWhileColor{
            setImgMask(imgMenuBack, UIImage(named: "glu_ic_backhome.png")!, UIColor(hexString: "#AF1D7E")!)
            setImgMask(imgMenuDetail, UIImage(named: "glu_ic_hamberger.png")!, UIColor(hexString: "#AF1D7E")!)
            setImgMask(imgClose, UIImage(named: "ic_x.png")!, UIColor(hexString: "#AF1D7E")!)
        }
        else{
            setImgMask(imgMenuBack, UIImage(named: "glu_ic_backhome.png")!, UIColor.white)
            setImgMask(imgMenuDetail, UIImage(named: "glu_ic_hamberger.png")!, UIColor.white)
            setImgMask(imgClose, UIImage(named: "ic_x.png")!, UIColor.white)
        }
        if index == 0{
            setColor()
            setImgMask(imgMenuDetail, UIImage(named: "glu_ic_hamberger.png")!, UIColor(hexString: "#AF1D7E")!)
            setImgMask(imgClose, UIImage(named: "ic_x.png")!, UIColor(hexString: "#AF1D7E")!)
            imgMenuBack.isHidden = true
        }
        else if index == 5 && orderedViewControllers.count == 17 {
            setImgMask(imgMenuBack, UIImage(named: "glu_ic_backhome.png")!, UIColor.white)
            setImgMask(imgMenuDetail, UIImage(named: "glu_ic_hamberger.png")!, UIColor(hexString: "#AF1D7E")!)
            setImgMask(imgClose, UIImage(named: "ic_x.png")!, UIColor(hexString: "#AF1D7E")!)
        }
        else{
            imgMenuBack.isHidden = true
            imgClose.isHidden = false
            imgMenuDetail.isHidden = false
        }
        currentPageIndex = index
    }
    
    func pageboyViewController(_ pageboyViewController: PageboyViewController, willScrollToPageAt index: Int, direction: PageboyViewController.NavigationDirection, animated: Bool) {
    }
    
}
