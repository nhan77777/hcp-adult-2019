//
//  Login.swift
//  HCP Adult
//
//  Created by Bình Phạm on 6/6/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import Foundation

class Login {
    
    func params(_ id : String, _ password : String) -> LoginRequestModel{
        let loginRequest = LoginRequestModel(JSONString: "{}")!
        loginRequest.userid = id
        loginRequest.pwd = password
        return loginRequest
    }
    
    func login (id : String, password : String, callBack:@escaping (Bool, LoginModel) -> Void){
        NetworkService
            .post(endPoint: API.loginValidation, requestBody: params(id,password).toJSON()){ success, response in
                if let data = LoginModel(JSONString: response) {
                    callBack(success, data)
                } else {
                    callBack(false, LoginModel(JSONString: "{}")!)
                }
        }
    }
}
