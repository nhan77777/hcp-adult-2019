//
//  FeedbackRecord.swift
//  HCP Adult
//
//  Created by Bình Phạm on 7/16/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import Foundation
import EVReflection

class FeedbackRecord : EVObject {
    public var answer: [String]?
    public var question: String?
}
