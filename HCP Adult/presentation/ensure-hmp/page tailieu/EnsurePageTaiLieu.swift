//
//  EnsurePageTaiLieu.swift
//  HCP Adult
//
//  Created by Le Nhan on 1/11/19.
//  Copyright © 2019 Bình Phạm. All rights reserved.
//

import UIKit

class EnsurePageTaiLieu: BaseDetailVC, DetailView {

    @IBOutlet weak var imgBg: UIImageView!
    
    func loadImages() {
        loadResourceToImage("ensure_bg_page_tailieu", imgBg)
        
    }
    
    func resetAnimation() {
        
    }
    
    func startAnimation() {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadImages()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        resetAnimation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        startAnimation()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
