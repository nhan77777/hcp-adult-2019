//
//  TwinkleView.swift
//  PediasureHCP
//
//  Created by Bình Phạm on 12/18/17.
//  Copyright © 2017 Bình Phạm. All rights reserved.
//

import UIKit
import Foundation
import CoreGraphics

private let TwinkleLayerEmitterShapeKey = "circle"
private let TwinkleLayerEmitterModeKey = "surface"
private let TwinkleLayerRenderModeKey = "unordered"
private let TwinkleLayerMagnificationFilter = "linear"
private let TwinkleLayerMinificationFilter = "trilinear"

public class Twinkle {
    
    public class func twinkle(_ view: UIView, image: UIImage? = nil) {
        var twinkleLayers: [TwinkleLayer] = []
//        let upperBound: UInt32 = 100
//        let lowerBound: UInt32 = 5
//        let count: UInt = UInt(arc4random_uniform(upperBound) + lowerBound)
        view.layer.sublayers?.removeAll()
        for i in 0..<100 {
            let twinkleLayer: TwinkleLayer = image == nil ? TwinkleLayer() : TwinkleLayer(image: image!)
            let x: Int = Int(arc4random_uniform(UInt32(view.layer.bounds.size.width)))
            let y: Int = Int(arc4random_uniform(UInt32(view.layer.bounds.size.height)))
            twinkleLayer.position = CGPoint(x: CGFloat(x), y: CGFloat(y))
            twinkleLayer.opacity = 0
            twinkleLayers.append(twinkleLayer)
            view.layer.addSublayer(twinkleLayer)
            twinkleLayer.addPositionAnimation()
            twinkleLayer.addRotationAnimation()
            twinkleLayer.addFadeInOutAnimation( CACurrentMediaTime() + CFTimeInterval(0.5 * Float(i)))
        }
        twinkleLayers.removeAll(keepingCapacity: false)
    }
    
}

internal class TwinkleLayer: CAEmitterLayer {
    
    internal convenience init(image: UIImage) {
        self.init()
        self.commonInit(image)
    }
    
    internal override init() {
        super.init()
        self.commonInit()
    }
    
    internal required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    internal func commonInit(_ image: UIImage? = nil) {
        var twinkleImage: UIImage? = nil
        if let customImage = image {
            twinkleImage = customImage
        } else {
            let frameworkBundle = Bundle(for: self.classForCoder)
            if let imagePath = frameworkBundle.path(forResource: "twin_white", ofType: "png") {
                twinkleImage = UIImage(contentsOfFile: imagePath)
            }
        }
        self.emitterCells?.removeAll()
        let emitterCells: [CAEmitterCell] = [CAEmitterCell(), CAEmitterCell()]
        for cell in emitterCells {
            cell.birthRate = 8
            cell.lifetime = 1.25
            cell.lifetimeRange = 0
            cell.emissionRange = (.pi / 4)
            cell.velocity = 2
            cell.velocityRange = 18
            cell.scale = 0.65
            cell.scaleRange = 0.7
            cell.scaleSpeed = 0.6
            cell.spin = 0.9
            cell.spinRange = .pi
            cell.color = UIColor(white: 1.0, alpha: 0.3).cgColor
            cell.alphaSpeed = -0.8
            cell.contents = twinkleImage?.cgImage
            cell.magnificationFilter = TwinkleLayerMagnificationFilter
            cell.minificationFilter = TwinkleLayerMinificationFilter
            cell.isEnabled = true
        }
        self.emitterCells = emitterCells
        self.emitterPosition = CGPoint(x: (bounds.size.width * 0.5), y: (bounds.size.height * 0.5))
        self.emitterSize = bounds.size
        self.emitterShape = CAEmitterLayerEmitterShape(rawValue: TwinkleLayerEmitterShapeKey)
        self.emitterMode = CAEmitterLayerEmitterMode(rawValue: TwinkleLayerEmitterModeKey)
        self.renderMode = CAEmitterLayerRenderMode(rawValue: TwinkleLayerRenderModeKey)
    }
    
}

fileprivate let TwinkleLayerPositionAnimationKey = "positionAnimation"
fileprivate let TwinkleLayerTransformAnimationKey = "transformAnimation"
fileprivate let TwinkleLayerOpacityAnimationKey = "opacityAnimation"

extension TwinkleLayer {
    
    internal func addPositionAnimation() {
        CATransaction.begin()
        let keyFrameAnim = CAKeyframeAnimation(keyPath: "position")
        keyFrameAnim.duration = 0.3
        keyFrameAnim.isAdditive = true
        keyFrameAnim.repeatCount = MAXFLOAT
        keyFrameAnim.isRemovedOnCompletion = false
        keyFrameAnim.beginTime = CFTimeInterval(arc4random_uniform(1000) + 1) * 0.2 * 0.25
        let points: [NSValue] = [NSValue(cgPoint: CGPoint.random(0.25)),
                                 NSValue(cgPoint: CGPoint.random(0.25)),
                                 NSValue(cgPoint: CGPoint.random(0.25)),
                                 NSValue(cgPoint: CGPoint.random(0.25)),
                                 NSValue(cgPoint: CGPoint.random(0.25))]
        keyFrameAnim.values = points
        self.add(keyFrameAnim, forKey: TwinkleLayerPositionAnimationKey)
        CATransaction.commit()
    }
    
    internal func addRotationAnimation() {
        CATransaction.begin()
        let keyFrameAnim = CAKeyframeAnimation(keyPath: "transform")
        keyFrameAnim.duration = 0.3
        keyFrameAnim.valueFunction = CAValueFunction(name: CAValueFunctionName.rotateZ)
        keyFrameAnim.isAdditive = true
        keyFrameAnim.repeatCount = MAXFLOAT
        keyFrameAnim.isRemovedOnCompletion = false
        keyFrameAnim.beginTime = CFTimeInterval(arc4random_uniform(1000) + 1) * 0.2 * 0.25
        let radians: Float = 0.104 // ~6 degrees
        keyFrameAnim.values = [-radians, radians, -radians]
        self.add(keyFrameAnim, forKey: TwinkleLayerTransformAnimationKey)
        CATransaction.commit()
    }
    
    internal func addFadeInOutAnimation(_ beginTime: CFTimeInterval) {
        CATransaction.begin()
        let fadeAnimation: CABasicAnimation = CABasicAnimation(keyPath: "opacity")
        fadeAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        fadeAnimation.fromValue = 0
        fadeAnimation.toValue = 1
        fadeAnimation.repeatCount = 2
        fadeAnimation.autoreverses = true // fade in then out
        fadeAnimation.duration = 0.4
        fadeAnimation.fillMode = CAMediaTimingFillMode.forwards
        fadeAnimation.beginTime = beginTime
        CATransaction.setCompletionBlock({
            self.removeFromSuperlayer()
        })
        self.add(fadeAnimation, forKey: TwinkleLayerOpacityAnimationKey)
        CATransaction.commit()
    }
    
}

extension CGPoint {
    
    internal static func random(_ range: Float) -> CGPoint {
        let x = Int(-range + (Float(arc4random_uniform(1000)) / 1000.0) * 2.0 * range)
        let y = Int(-range + (Float(arc4random_uniform(1000)) / 1000.0) * 2.0 * range)
        return CGPoint(x: x, y: y)
    }
    
}

extension UIView {
    
    public func twinkle() {
        Twinkle.twinkle(self)
    }
    
    public func twinkle(_ image : UIImage){
        Twinkle.twinkle(self, image: image)
    }
    
}
