//
//  EnsureVitalPage5VC.swift
//  HCP Adult
//
//  Created by Le Nhan on 11/29/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import UIKit
import Async
import Nuke

class EnsureVitalPage5VC: BaseDetailVC, DetailView{
    
    @IBOutlet weak var imgBG: UIImageView!
    
    @IBOutlet var imgTitleCollection: [UIImageView]!
    @IBOutlet weak var imgContentBelow: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadImages()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        resetAnimation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Async.main(after: 0.3){
            self.startAnimation()
        }
    }
    
    func loadImages() {
        loadResourceToImage("ensure_vital_page_5", imgBG)
        loadResourceToImage("ic_vital_contentbelow_page5", imgContentBelow)
        for icon in imgTitleCollection {
            switch icon.tag {
            case 1: loadResourceToImage("ic_vital_title1_page5", icon)
            case 2: loadResourceToImage("ic_vital_title2_page5", icon)
            case 3: loadResourceToImage("ic_vital_title3_page5", icon)
            case 4: loadResourceToImage("ic_vital_title4_page5", icon)
            default:
                loadResourceToImage("ic_vital_title1_page5", icon)
            }
        }
    }
    
    func resetAnimation() {
        imgContentBelow.layer.removeAllAnimations()
        for image in imgTitleCollection {
            image.layer.removeAllAnimations()
        }
    }
    
    func startAnimation() {
        imgContentBelow.twinkle()
        
        for image in imgTitleCollection {
            image.startGlowingWithColor(color: UIColor(hexString: "#f38dff")!, intensity: 0.5)
        }
    }
}
