//
//  Page2VC.swift
//  Glucerna
//
//  Created by Long Quách Phi on 3/22/18.
//  Copyright © 2018 Fractal. All rights reserved.
//

import UIKit

class Page9VC: BaseDetailVC, DetailView {
    
    @IBOutlet var viewCover: [UIView]!
    @IBOutlet weak var imgTopBar: UIImageView!
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet var imgGlassMilkAction: [UIImageView]!
    private var arrCheckTap = [false,false,false]
    
    func loadImages() {
        loadResourceToImage("glu_bg_page9", imgBackground)
        loadResourceToImage("glu_ic_page9", imgTopBar)
    }
    
    func resetAnimation() {
        viewCover[0].layer.opacity = 0
        viewCover[1].layer.opacity = 0
        arrCheckTap = [false,false,false]
        imgGlassMilkAction[0].frame.origin = CGPoint(x: 204, y: 341)
        imgGlassMilkAction[1].frame.origin = CGPoint(x: 541, y: 341)
        imgGlassMilkAction[2].frame.origin = CGPoint(x: 891, y: 341)
    }
    
    func startAnimation() {
        for i in 0...imgGlassMilkAction.count-1 {
            imgGlassMilkAction[i].addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onViewTap(_:))))
            imgGlassMilkAction[i].isUserInteractionEnabled = true
            imgGlassMilkAction[i].tag = i
        }
    }
    
    @objc func onViewTap(_ sender : UITapGestureRecognizer){
        let index = sender.view?.tag
        switch index! {
        case 0:
            if !arrCheckTap[0] && imgGlassMilkAction[0].frame.origin.y == 341 {
                UIView.animate(withDuration: 0.9, animations: {
                    self.imgGlassMilkAction[0].frame.origin = CGPoint(x: self.imgGlassMilkAction[0].frame.origin.x - 161, y: self.imgGlassMilkAction[0].frame.origin.y - 120)
                    self.viewCover[0].layer.opacity = 1
                })
                arrCheckTap[0] = true
            }
            else{
                UIView.animate(withDuration: 0.9, animations: {
                    self.imgGlassMilkAction[0].frame.origin = CGPoint(x: self.imgGlassMilkAction[0].frame.origin.x + 161, y: self.imgGlassMilkAction[0].frame.origin.y + 120)
                    self.viewCover[0].layer.opacity = 0
                })
                arrCheckTap[0] = false
            }
            
            break
        case 1:
            if !arrCheckTap[1] && imgGlassMilkAction[1].frame.origin.y == 341 {
                UIView.animate(withDuration: 0.7, animations: {
                    self.imgGlassMilkAction[1].frame.origin = CGPoint(x: self.imgGlassMilkAction[1].frame.origin.x, y: self.imgGlassMilkAction[1].frame.origin.y - 120)
                    self.viewCover[1].layer.opacity = 1
                })
                arrCheckTap[1] = true
            }
            else{
                UIView.animate(withDuration: 0.7, animations: {
                    self.imgGlassMilkAction[1].frame.origin = CGPoint(x: self.imgGlassMilkAction[1].frame.origin.x, y: self.imgGlassMilkAction[1].frame.origin.y + 120)
                    self.viewCover[1].layer.opacity = 0
                })
                arrCheckTap[1] = false
            }
            break
        case 2:
            if !arrCheckTap[2] && imgGlassMilkAction[2].frame.origin.y == 341 {
                UIView.animate(withDuration: 0.7, animations: {
                    self.imgGlassMilkAction[2].frame.origin = CGPoint(x: self.imgGlassMilkAction[2].frame.origin.x, y: self.imgGlassMilkAction[2].frame.origin.y - 120)
                })
                arrCheckTap[2] = true
            }
            else{
                UIView.animate(withDuration: 0.7, animations: {
                    self.imgGlassMilkAction[2].frame.origin = CGPoint(x: self.imgGlassMilkAction[2].frame.origin.x, y: self.imgGlassMilkAction[2].frame.origin.y + 120)
                })
                arrCheckTap[2] = false
            }
            break
        default:
            print("ERROR")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pageID = "page38"
        loadImages()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        resetAnimation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        startAnimation()
    }
    
}

