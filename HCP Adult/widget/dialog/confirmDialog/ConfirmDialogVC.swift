//
//  ConfirmDialogVC.swift
//  HCP Adult
//
//  Created by Bình Phạm on 6/7/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import UIKit

class ConfirmDialogVC: UIViewController {

    @IBOutlet weak var btnPositive: UIButton!
    @IBOutlet weak var btnNegative: UIButton!
    @IBOutlet weak var lbMessage: UILabel!
    
    @IBAction func onPositiveTap(_ sender: Any) {
        dismiss(animated: true, completion: {
            self.callbackPositive?(self)
        })
    }
    
    @IBAction func onNegativeTap(_ sender: Any) {
        dismiss(animated: true, completion: {
            self.callbackNegative?(self)
        })
    }
    
    public var callbackPositive:((UIViewController) -> ())?
    public var callbackNegative:((UIViewController) -> ())?
    public var negativeText : String = ""
    public var positiveText : String = ""
    public var messenge : String = ""

    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.layer.borderColor = UIColor(hexString: "#2b6eb2")?.cgColor
        view.layer.borderWidth = 2
        view.layer.cornerRadius = 15
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        btnNegative.setTitle(negativeText.uppercased(), for: .normal)
        btnPositive.setTitle(positiveText.uppercased(), for: .normal)
        lbMessage.text = messenge
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
