//
//  EnsurePlusPage1VC.swift
//  HCP Adult
//
//  Created by Long Quách Phi on 11/23/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import UIKit
import Async

class EnsurePlusPage4VC: BaseDetailVC, DetailView {
    
    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var icMilk: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadImages()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        resetAnimation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Async.main(after: 0.1){
            self.startAnimation()
        }
    }
    
    func loadImages() {
        loadResourceToImage("bg_ensure_plus_page_4", imgBG)
        loadResourceToImage("ic_ensure_plus_new", icMilk)
    }
    
    func resetAnimation() {
        icMilk.layer.removeAllAnimations()
    }
    
    func startAnimation() {
        icMilk.twinkle()
        boucingViewForever(icMilk, 0.9)
    }
    
}
