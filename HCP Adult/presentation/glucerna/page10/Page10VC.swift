//
//  Page2VC.swift
//  Glucerna
//
//  Created by Long Quách Phi on 3/22/18.
//  Copyright © 2018 Fractal. All rights reserved.
//

import UIKit

class Page10VC: BaseDetailVC, DetailView {
    
    @IBOutlet var imgGlassMilkAction: [UIImageView]!
    @IBOutlet var imgGlassMilkCover: [UIImageView]!
    @IBOutlet weak var imgTopBar: UIImageView!
    @IBOutlet weak var imgBackground: UIImageView!
    private var arrCheckTap = [false,false,false]
    
    func loadImages() {
        loadResourceToImage("glu_bg_page10", imgBackground)
        loadResourceToImage("glu_ic_page10", imgTopBar)
    }
    
    func resetAnimation() {
        for i in 0...imgGlassMilkCover.count-1 {
            imgGlassMilkCover[i].layer.opacity = 0
        }
        imgGlassMilkAction[0].frame.origin = CGPoint(x: 227, y: 341)
        imgGlassMilkAction[1].frame.origin = CGPoint(x: 566, y: 341)
        imgGlassMilkAction[2].frame.origin = CGPoint(x: 908, y: 341)
        arrCheckTap = [false,false,false]
    }
    
    func startAnimation() {
        for i in 0...imgGlassMilkAction.count-1 {
            imgGlassMilkAction[i].addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onViewTap(_:))))
            imgGlassMilkAction[i].isUserInteractionEnabled = true
            imgGlassMilkAction[i].tag = i
        }
    }
    
    @objc func onViewTap(_ sender : UITapGestureRecognizer){
        if let index = sender.view?.tag {
            if arrCheckTap[index] && imgGlassMilkAction[index].frame.origin.y == 221{
                UIView.animate(withDuration: 0.7, animations: {
                    self.imgGlassMilkAction[index].frame.origin = CGPoint(x: self.imgGlassMilkAction[index].frame.origin.x, y: self.imgGlassMilkAction[index].frame.origin.y + 120)
                    self.imgGlassMilkCover[index].layer.opacity = 0
                })
                arrCheckTap[index] = false
            }
            else if imgGlassMilkAction[index].frame.origin.y == 341  {
                UIView.animate(withDuration: 0.7, animations: {
                    self.imgGlassMilkAction[index].frame.origin = CGPoint(x: self.imgGlassMilkAction[index].frame.origin.x, y: self.imgGlassMilkAction[index].frame.origin.y - 120)
                    self.imgGlassMilkCover[index].layer.opacity = 1
                })
                arrCheckTap[index] = true
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pageID = "page39"
        loadImages()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        resetAnimation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        startAnimation()
    }
    
}

