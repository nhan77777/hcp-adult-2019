//
//  Page5VC.swift
//  Glucerna
//
//  Created by Long Quách Phi on 3/22/18.
//  Copyright © 2018 Fractal. All rights reserved.
//

import UIKit

class Page5VC: BaseDetailVC, DetailView {
    
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet var imgICContent: [UIImageView]!
    @IBOutlet weak var viewRotate: UIImageView!
    @IBOutlet weak var viewAction: UIView!
    @IBOutlet var viewFooter: [UIView]!
    
    func loadImages() {
        loadResourceToImage("glu_bg_page5", imgBackground)
        loadResourceToImage("glu_ic_onlyone", imgICContent[0])
        loadResourceToImage("glu_ic_page5", imgICContent[1])
        loadResourceToImage("glu_ic_circle", viewRotate)
    }
    
    func resetAnimation() {
        imgICContent[0].layer.removeAllAnimations()
    }
    
    func startAnimation() {
        rotateView(viewRotate, 4.0)
    }
    
    @objc func onViewTapFooter(_ sender : UITapGestureRecognizer){
        let index = (sender.view?.tag)!
        switch index {
        case 0:
            showPDFView(filename: "glu_0")
        case 1:
            showPDFView(filename: "glu_1")
        case 2:
            showPDFView(filename: "glu_4")
        default:
            print("error")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pageID = "page34"
        loadImages()
        for i in 0...viewFooter.count-1 {
            viewFooter[i].addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onViewTapFooter(_:))))
            viewFooter[i].tag = i
            viewFooter[i].isUserInteractionEnabled = true
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        resetAnimation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        startAnimation()
    }
    
}


