//
//  ChoiceHCPPresenter.swift
//  HCP Adult
//
//  Created by Bình Phạm on 6/6/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import Foundation
import UIKit

class ChoiceHCPPresenter {
    fileprivate let getHcp : GetHCP
    fileprivate let durationService : PushDuration
    fileprivate let checkVersionService : CheckAppVersion
    fileprivate let hcpSurveyService : PushFeedback
    weak fileprivate var view : ChoiceHCPView?
    
    init(getHcp : GetHCP, durationService : PushDuration, hcpSurveyService: PushFeedback,  checkVersionService: CheckAppVersion){
        self.getHcp = getHcp
        self.durationService = durationService
        self.hcpSurveyService = hcpSurveyService
        self.checkVersionService = checkVersionService
    }
    
    func attachView(_ view : ChoiceHCPView){
        self.view = view
    }
    
    func detachView() {
        view = nil
    }
    
    func checkVersion(_ appId : String, _ appVer : String) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        checkVersionService.checkAppVersion(appId, appVer) { success, response in
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            if success {
                self.view?.checkVersionSuccess(response)
            } else {
                self.view?.checkVersionFailed("Kết nối thất bại")
            }
        }
    }
    
    func doGetHCP(id : String) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        view?.showLoading("Processing ...")
        getHcp.postHCP(id: id){ success, response in
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
            self.view?.hideLoading()
            if success {
                self.view?.onGetHCP(response.data!)
            } else {
                self.view?.onGetHCPFailed("Kết nối thất bại")
            }
        }
    }
    
    func pushDuration(durationRequestModel: Dictionary<String, Any>){
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        durationService.pushDuration(durationRequestModel: durationRequestModel) { success, response in
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            if success{
                if let status = response.status {
                    if (status.elementsEqual("true")){
                        self.view?.pushDurationSuccess(response)
                    } else {
                        self.view?.pushDurationFailed(response.msg!)
                    }
                }
                else{
                    self.view?.pushDurationFailed("Kết nối thất bại")
                }
            }
            else{
                self.view?.pushDurationFailed("Kết nối thất bại")
            }
        }
    }
    
    func pushFeedback(hcpSurvey: HCPSurvey){
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        hcpSurveyService.pushFeedback(hcpSurvey: hcpSurvey) { success, response in
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            if success{
                if let status = response.status {
                    if (status.elementsEqual("true")){
                        self.view?.pushHCPSurveySuccess(response)
                    } else {
                        self.view?.pushHCPSurveyFailed(response.msg!)
                    }
                }
                else{
                    self.view?.pushHCPSurveyFailed("Kết nối thất bại")
                }
            }
            else{
                self.view?.pushHCPSurveyFailed("Kết nối thất bại")
            }
        }
    }
}
