//
//  BaseDetailVC.swift
//  HCP Adult
//
//  Created by Long Quách Phi on 7/5/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import UIKit

class BaseDetailVC: UIViewController {
    
    public var pageID = ""

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
