//
//  Hcp.swift
//
//  Created by Long Quách Phi on 5/29/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class HCPModel: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let name = "name"
    static let email = "email"
    static let id = "id"
    static let specialty = "specialty"
    static let phone = "phone"
    static let hcptype = "hcptype"
    static let anforceid = "anforceid"
    static let dept = "dept"
    static let hospname = "hospname"
    static let hospanforceid = "hospanforceid"
  }

  // MARK: Properties
  public var name: String?
  public var email: String?
  public var id: String?
  public var specialty: String?
  public var phone: String?
  public var hcptype: String?
  public var anforceid: String?
  public var dept: String?
  public var hospname: String?
  public var hospanforceid: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    name <- map[SerializationKeys.name]
    email <- map[SerializationKeys.email]
    id <- map[SerializationKeys.id]
    specialty <- map[SerializationKeys.specialty]
    phone <- map[SerializationKeys.phone]
    hcptype <- map[SerializationKeys.hcptype]
    anforceid <- map[SerializationKeys.anforceid]
    dept <- map[SerializationKeys.dept]
    hospname <- map[SerializationKeys.hospname]
    hospanforceid <- map[SerializationKeys.hospanforceid]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = name { dictionary[SerializationKeys.name] = value }
    if let value = email { dictionary[SerializationKeys.email] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = specialty { dictionary[SerializationKeys.specialty] = value }
    if let value = phone { dictionary[SerializationKeys.phone] = value }
    if let value = hcptype { dictionary[SerializationKeys.hcptype] = value }
    if let value = anforceid { dictionary[SerializationKeys.anforceid] = value }
    if let value = dept { dictionary[SerializationKeys.dept] = value }
    if let value = hospname {dictionary[SerializationKeys.hospname] = value}
    if let value = hospanforceid { dictionary[SerializationKeys.hospanforceid] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.name = aDecoder.decodeObject(forKey: SerializationKeys.name) as? String
    self.email = aDecoder.decodeObject(forKey: SerializationKeys.email) as? String
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? String
    self.specialty = aDecoder.decodeObject(forKey: SerializationKeys.specialty) as? String
    self.phone = aDecoder.decodeObject(forKey: SerializationKeys.phone) as? String
    self.hcptype = aDecoder.decodeObject(forKey: SerializationKeys.hcptype) as? String
    self.anforceid = aDecoder.decodeObject(forKey: SerializationKeys.anforceid) as? String
    self.dept = aDecoder.decodeObject(forKey: SerializationKeys.dept) as? String
    self.hospname = aDecoder.decodeObject(forKey: SerializationKeys.hospname) as? String
    self.hospanforceid = aDecoder.decodeObject(forKey: SerializationKeys.hospanforceid) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(name, forKey: SerializationKeys.name)
    aCoder.encode(email, forKey: SerializationKeys.email)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(specialty, forKey: SerializationKeys.specialty)
    aCoder.encode(phone, forKey: SerializationKeys.phone)
    aCoder.encode(hcptype, forKey: SerializationKeys.hcptype)
    aCoder.encode(anforceid, forKey: SerializationKeys.anforceid)
    aCoder.encode(hospanforceid, forKey: SerializationKeys.hospanforceid)
    aCoder.encode(dept, forKey: SerializationKeys.dept)
    aCoder.encode(hospname, forKey: SerializationKeys.hospname)
  }

}
