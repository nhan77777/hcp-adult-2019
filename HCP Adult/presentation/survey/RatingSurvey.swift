//
//  RatingSurvey.swift
//  HCP Adult
//
//  Created by Bình Phạm on 6/8/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import UIKit

class RatingSurvey: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var tapView: UIStackView!
    @IBOutlet weak var consProcess: NSLayoutConstraint!
    public var onTap : (()->())?
    public var targetResult = 0
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit(){
        Bundle.main.loadNibNamed("RatingSurvey", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        contentView.layer.cornerRadius = 10
        tapView.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(onSwipe(_:))))
        tapView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onTap(_:))))
    }
    
    @objc func onTap(_ sender : UITapGestureRecognizer){
        let point: CGPoint = sender.location(in: sender.view)
        let target : Int = Int(point.x/(contentView.bounds.width/10))
        targetResult = target + 1
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 3, options: [], animations: {
            self.consProcess.constant = CGFloat(target+1) * (self.contentView.bounds.width/10)
            self.contentView.layoutIfNeeded()
            self.onTap!()
        }, completion: nil)
    }
    
    @objc func onSwipe(_ sender : UIPanGestureRecognizer){
        let point: CGPoint = sender.location(in: sender.view)
        if sender.state == .ended {
            let target : Int = Int(point.x/(contentView.bounds.width/10))
            targetResult = target + 1
            UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 3, options: [], animations: {
                self.consProcess.constant = CGFloat(target+1) * (self.contentView.bounds.width/10)
                self.contentView.layoutIfNeeded()
                self.onTap!()
            }, completion: nil)
        } else {
            UIView.animate(withDuration: 0.2, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1.5, options: [], animations: {
                self.consProcess.constant = point.x
                self.contentView.layoutIfNeeded()
                self.onTap!()
            }, completion: nil)
        }
    }
    
}
