//
//  ProsureMenuVC.swift
//  HCP Adult
//
//  Created by Bình Phạm on 7/27/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import UIKit

class ProsureMenuVC: UIViewController{
    
    public var homeView : PageView?
    public var targetFlowVC : UIViewController?
    public var targetFlowName : String?
    public var targetFlow : Int?
    @IBOutlet var changeFlowButton: [UIButton]!
    
    var isHidenTyp2 = false
    
    @IBAction func onGotoOtherFlow(_ sender: UIButton) {
        homeView?.pauseTimer()
        switch sender.tag {
        case 0:
            targetFlowVC =  EnsureHomeVC.instantiate(appStoryboard: .Main)
            break
        case 1 :
            let vc = Page0VC.instantiate(appStoryboard: .Main)
            vc.isHidenTyp2 = false
            targetFlowVC = vc
            break
    
        default:
            break
        }
        present(targetFlowVC!, animated: true, completion: nil)
    }
    
    func setupFlowChange() {
        
        for i in 0...changeFlowButton.count - 1{
            changeFlowButton[i].tag = i
            
            switch i{
            case 0:
                targetFlowName = "Chuyển đến Ensure Gold"
                changeFlowButton[i].backgroundColor = UIColor.orange
                break
            case 1 :
                targetFlowName = "Chuyển đến Glucerna"
                changeFlowButton[i].backgroundColor = UIColor(hexString: "#4f0090")
                break
            default:
                targetFlowName = "Untarget"
                break
            }
            
            changeFlowButton[i].setTitle(targetFlowName!, for: .normal)
        }
    }

    
    @IBAction func onButton1Tap(_ sender: Any) {
        homeView?.pauseTimer()
        let page2 = ProsurePage2VC(nibName: "ProsurePage2VC", bundle: nil)
        page2.pageID = "page43"
        showSubSlide([page2],homeView!)
    }
    
    @IBAction func onButton2Tap(_ sender: Any) {
        homeView?.pauseTimer()
        let page4 = ProsurePage4VC(nibName: "ProsurePage4VC", bundle: nil)
        let page6 = ProsurePage6VC(nibName: "ProsurePage6VC", bundle: nil)
        page4.pageID = "page45"
        page6.pageID = "page47"
        showSubSlide([page4,page6],homeView!)
    }
    
    @IBAction func onButton3Tap(_ sender: Any) {
        homeView?.pauseTimer()
        let page5 = ProsurePage5VC(nibName: "ProsurePage5VC", bundle: nil)
        let page7 = ProsurePage7VC(nibName: "ProsurePage7VC", bundle: nil)
        let page8 = ProsurePage8VC(nibName: "ProsurePage8VC", bundle: nil)
        page5.pageID = "page46"
        page7.pageID = "page48"
        page8.pageID = "page49"
        showSubSlide([page5,page7,page8],homeView!)
    }
    
    @IBAction func onButton4Tap(_ sender: Any) {
        homeView?.pauseTimer()
        let page9 = ProsurePage9VC(nibName: "ProsurePage9VC", bundle: nil)
        page9.pageID = "page50"
        showSubSlide([page9],homeView!)
    }
    
    @IBAction func onButton5Tap(_ sender: Any) {
        homeView?.pauseTimer()
        let page10 = ProsurePage10VC(nibName: "ProsurePage10VC", bundle: nil)
        let page11 = ProsurePage12VC(nibName: "ProsurePage12VC", bundle: nil)
        page10.pageID = "page51"
        page11.pageID = "page53"
        showSubSlide([page10,page11],homeView!)
    }
    
    @IBAction func onButton6Tap(_ sender: Any) {
        homeView?.pauseTimer()
        let viewImage = ViewImageVC(nibName: "ViewImageVC", bundle: nil)
        viewImage.image = #imageLiteral(resourceName: "pro_bg_HDSD.png")
        viewImage.homeView = homeView
        present(viewImage, animated: true, completion: nil)
    }
    
    @IBAction func onButton7Tap(_ sender: Any) {
        homeView?.pauseTimer()
        showPDFView(filename: "Thanh phan prosure")
    }
    
    @IBAction func onButton8Tap(_ sender: Any) {
        homeView?.pauseTimer()
        showPDFView(filename: "Cau hoi thuong gap")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupFlowChange()
        navigationController?.navigationBar.isHidden = true
        
    }
    
}

