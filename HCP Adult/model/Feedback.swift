//
//  Feedback.swift
//
//  Created by Long Quách Phi on 7/11/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class Feedback: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let answer = "answer"
    static let question = "question"
  }

  // MARK: Properties
  public var answer: [String]?
  public var question: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    answer <- map[SerializationKeys.answer]
    question <- map[SerializationKeys.question]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = answer { dictionary[SerializationKeys.answer] = value }
    if let value = question { dictionary[SerializationKeys.question] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.answer = aDecoder.decodeObject(forKey: SerializationKeys.answer) as? [String]
    self.question = aDecoder.decodeObject(forKey: SerializationKeys.question) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(answer, forKey: SerializationKeys.answer)
    aCoder.encode(question, forKey: SerializationKeys.question)
  }

}
