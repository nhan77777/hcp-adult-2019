//
//  Page6VC.swift
//  Glucerna
//
//  Created by Long Quách Phi on 3/22/18.
//  Copyright © 2018 Fractal. All rights reserved.
//

import UIKit
import EasyAnimation

class Page6VC: BaseDetailVC, DetailView {
    
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet var imgICContent: [UIImageView]!
    @IBOutlet weak var viewRotate: UIImageView!
    @IBOutlet var viewActionICContent: [UIView]!
    @IBOutlet weak var imgICOnlyOne: UIImageView!
    @IBOutlet var viewFooter: [UIView]!
    
    func loadImages() {
        loadResourceToImage("glu_bg_page6", imgBackground)
        loadResourceToImage("glu_ic_page6_0", imgICContent[0])
        loadResourceToImage("glu_ic_page6_1", imgICContent[1])
        loadResourceToImage("glu_ic_page6_2", imgICContent[2])
        loadResourceToImage("glu_ic_onlyone", imgICOnlyOne)
        loadResourceToImage("glu_ic_circle", viewRotate)
    }
    
    @objc func onViewTapFooter(_ sender : UITapGestureRecognizer){
        showPDFView(filename: "glu_1")
    }
    
    func resetAnimation() {
    }
    
    func startAnimation() {
        rotateView(viewRotate, 4.0)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pageID = "page35"
        loadImages()
        for i in 0...viewFooter.count-1 {
            viewFooter[i].addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onViewTapFooter(_:))))
            viewFooter[i].tag = i
            viewFooter[i].isUserInteractionEnabled = true
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        resetAnimation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        startAnimation()
    }
    
}

