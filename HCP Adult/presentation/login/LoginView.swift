//
//  LoginView.swift
//  HCP Adult
//
//  Created by Bình Phạm on 6/6/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import Foundation

protocol LoginView: BaseView {
    func loginSuccess(_ data : LoginModel)
    func loginFailed(_ message : String)
}
