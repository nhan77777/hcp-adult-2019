//
//  EnsurePage10VC.swift
//  HCP Adult
//
//  Created by Le Nhan on 1/7/19.
//  Copyright © 2019 Bình Phạm. All rights reserved.
//

import UIKit
import Async

class EnsurePage10VC: BaseDetailVC, DetailView {
    
    @IBOutlet weak var imgIcon: UIImageView!
    
    @IBOutlet weak var imgBg: UIImageView!
    
    func loadImages() {
        loadResourceToImage("ensure_bg_page_10", imgBg)
    }
    
    func resetAnimation() {
        imgIcon.layer.removeAllAnimations()
    }
    
    func startAnimation() {
        imgIcon.twinkle()
        boucingViewForever(imgIcon, 1.15)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadImages()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        resetAnimation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Async.main(after: 0.1, {
            self.startAnimation()
        })
    }
}
