//
//  ProsurePage11VC.swift
//  HCP Adult
//
//  Created by Bình Phạm on 7/26/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import UIKit
import Async

class ProsurePage12VC: BaseDetailVC, DetailView {
    
    @IBOutlet weak var imgBg: UIImageView!
    @IBOutlet var imgIcons: [UIImageView]!
    
    func loadImages() {
        loadResourceToImage("pro_bg_page_12", imgBg)
        for i in 0...imgIcons.count-1 {
            loadResourceToImage("pro_ic_page_1_\(i)", imgIcons[i])
        }
    }
    
    func resetAnimation() {
        for ic in imgIcons {
            ic.layer.removeAllAnimations()
        }
    }
    
    func startAnimation() {
        imgIcons[0].twinkle()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadImages()
        for i in 1...3 {
            imgIcons[i].isUserInteractionEnabled = true
            imgIcons[i].addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onImageTap(_:))))
        }
    }
    
    @objc func onImageTap(_ sender : UITapGestureRecognizer){
        let image = sender.view
        image?.startGlowingWithColor(color: UIColor.yellow, fromIntensity: 0.2, toIntensity: 0.5, repeat: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Async.main(after: 0.1, {
            self.startAnimation()
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
