//
//  ProsurePage10VC.swift
//  HCP Adult
//
//  Created by Bình Phạm on 7/26/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import UIKit

class ProsurePage11VC: BaseDetailVC, DetailView {
    
    @IBOutlet weak var imgBg: UIImageView!
    
    func loadImages() {
        loadResourceToImage("pro_bg_page_11", imgBg)
    }
    
    func resetAnimation() {
        
    }
    
    func startAnimation() {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadImages()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
