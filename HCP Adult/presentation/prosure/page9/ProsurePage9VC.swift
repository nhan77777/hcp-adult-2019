//
//  ProsurePage8VC.swift
//  HCP Adult
//
//  Created by Bình Phạm on 7/26/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import UIKit
import Async

class ProsurePage9VC: BaseDetailVC, DetailView {
    
    @IBOutlet weak var imgBg: UIImageView!
    @IBOutlet var imgIcons: [UIImageView]!
    
    func loadImages() {
        loadResourceToImage("pro_bg_page_9", imgBg)
        loadResourceToImage("pro_ic_page_9_0", imgIcons[0])
        loadResourceToImage("pro_ic_page_9_1", imgIcons[1])
        loadResourceToImage("pro_ic_page_9_2", imgIcons[2])
        loadResourceToImage("pro_ic_page_9_3", imgIcons[3])
        loadResourceToImage("pro_ic_page_9_4", imgIcons[4])
        loadResourceToImage("pro_ic_page_3_0", imgIcons[imgIcons.count-1])
    }
    
    func resetAnimation() {
        for ic in imgIcons {
            ic.layer.removeAllAnimations()
            ic.stopGlowing()
        }
    }
    
    func startAnimation() {
        for i in 0...3 {
            imgIcons[i].startGlowingWithColor(color: UIColor.yellow, fromIntensity: 0.2, toIntensity: 0.6, repeat: true)
        }
        imgIcons[5].startGlowingWithColor(color: UIColor.yellow, fromIntensity: 0.2, toIntensity: 0.6, repeat: true)
        imgIcons[4].twinkle()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadImages()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Async.main(after: 0.1, {
            self.startAnimation()
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
