//
//  EnsurePage5VC.swift
//  HCP Adult
//
//  Created by Le Nhan on 1/7/19.
//  Copyright © 2019 Bình Phạm. All rights reserved.
//

import UIKit

class EnsurePage5VC: BaseDetailVC, DetailView {
    
    @IBOutlet weak var imgBg: UIImageView!
    
    func loadImages() {
        loadResourceToImage("ensure_bg_page_5", imgBg)
    }
    
    func resetAnimation() {
        
    }
    
    func startAnimation() {
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadImages()
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
