//
//  ProsurePage9VC.swift
//  HCP Adult
//
//  Created by Bình Phạm on 7/26/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import UIKit
import Async

class ProsurePage10VC: BaseDetailVC, DetailView {
    
    @IBOutlet weak var imgBg: UIImageView!
    @IBOutlet var imgIcons: [UIImageView]!
    
    func loadImages() {
        loadResourceToImage("pro_bg_page_10", imgBg)
        loadResourceToImage("pro_ic_page_10_0", imgIcons[0])
        loadResourceToImage("pro_ic_page_10_1", imgIcons[1])
        loadResourceToImage("pro_ic_page_10_2", imgIcons[2])
        loadResourceToImage("pro_ic_page_10_3", imgIcons[3])
        loadResourceToImage("pro_ic_page_10_4", imgIcons[4])
        loadResourceToImage("pro_ic_page_1_0", imgIcons[5])
        loadResourceToImage("pro_ic_page_1_1", imgIcons[6])
        loadResourceToImage("pro_ic_page_1_2", imgIcons[7])
        loadResourceToImage("pro_ic_page_1_3", imgIcons[8])
    }
    
    func resetAnimation() {
        for ic in imgIcons {
            ic.layer.removeAllAnimations()
            ic.stopGlowing()
        }
    }
    
    func startAnimation() {
        for i in 0...4 {
            imgIcons[i].startGlowing()
        }
        imgIcons[5].twinkle()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadImages()
        
        for i in 6...8 {
            imgIcons[i].isUserInteractionEnabled = true
            imgIcons[i].addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onImageTap(_:))))
        }
    }
    
    @objc func onImageTap(_ sender : UITapGestureRecognizer){
        let image = sender.view
        image?.startGlowingWithColor(color: UIColor.yellow, fromIntensity: 0.2, toIntensity: 0.5, repeat: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Async.main(after: 0.1, {
            self.startAnimation()
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
