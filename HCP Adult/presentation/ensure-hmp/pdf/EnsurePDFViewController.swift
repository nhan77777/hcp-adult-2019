//
//  PDFViewController.swift
//  EnsureHMP
//
//  Created by Bình Phạm on 5/3/18.
//

import UIKit

class EnsurePDFViewController: UIViewController {
    
    @IBOutlet weak var navItem: UINavigationItem!
    @IBOutlet weak var clvMenu: UICollectionView!
    @IBOutlet weak var webView: UIWebView!
    public var fileName : String?
    public var homeController : PageView?
    var selectedIndex : Int = 0
    var navTitle = ""
    
    var pdfFiles: [String] = []
    
    @IBAction func onClose(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onShareTap(_ sender: UIBarButtonItem) {
        if let pdfURL = Bundle.main.url(forResource: fileName!, withExtension: "pdf", subdirectory: nil, localization: nil)  {
            do {
                let shareVC = UIActivityViewController(activityItems: [pdfURL], applicationActivities: nil)
                shareVC.popoverPresentationController?.barButtonItem = sender
                shareVC.popoverPresentationController?.permittedArrowDirections = .up
                self.present(shareVC, animated: true, completion: nil)
            }
            catch {
                print("Data file pdf not invalid")
            }
        }
    }
    
    func loadPdf(){
        if let pdfURL = Bundle.main.url(forResource: fileName!, withExtension: "pdf", subdirectory: nil, localization: nil)  {
            do {
                let data = try Data(contentsOf: pdfURL)
                webView.load(data, mimeType: "application/pdf", textEncodingName:"", baseURL: pdfURL.deletingLastPathComponent())
            }
            catch {
                print("Data file pdf not invalid")
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
        navItem.title = navTitle
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        fileName = pdfFiles[0]
        loadPdf()
    }
    
    func setupCollectionView(){
        extendedLayoutIncludesOpaqueBars = false
        automaticallyAdjustsScrollViewInsets = false
        clvMenu.dataSource = self
        clvMenu.delegate = self
        clvMenu.register(UINib.init(nibName:"PDFMenuCell",bundle:nil), forCellWithReuseIdentifier: "PDFMenuCell")
        if let flowLayout = clvMenu.collectionViewLayout as? UICollectionViewFlowLayout{
            flowLayout.minimumLineSpacing = 3
            flowLayout.minimumInteritemSpacing = 1
            flowLayout.estimatedItemSize = CGSize(width: 1, height: 1)
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        homeController?.startTimer()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension EnsurePDFViewController : UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        clvMenu.scrollToItem(at: indexPath, at: UICollectionViewScrollPosition(rawValue: UICollectionViewScrollPosition.RawValue(indexPath.row)), animated: true)
        fileName = pdfFiles[indexPath.row]
        selectedIndex = indexPath.row
        collectionView.reloadData()
        loadPdf()
    }
    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true;
    }
}

extension EnsurePDFViewController : UICollectionViewDataSource {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pdfFiles.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PDFMenuCell", for: indexPath) as! PDFMenuCell
        cell.lbTitle.text = pdfFiles[indexPath.row]
        if selectedIndex == indexPath.row {
            cell.lbTitle.font = UIFont.boldSystemFont(ofSize: 17)
        } else {
            cell.lbTitle.font = UIFont.systemFont(ofSize: 15)
        }
        return cell
    }
    
}


