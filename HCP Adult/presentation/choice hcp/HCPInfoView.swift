//
//  HCPInfoView.swift
//  HCP Adult
//
//  Created by Bình Phạm on 6/8/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import UIKit

class HCPInfoView: UIView {

    @IBOutlet weak var imgTitle: UIImageView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var contentView: UIView!
   
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit(){
        Bundle.main.loadNibNamed("HCPInfoView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        contentView.layer.borderColor = UIColor(hexString: "#175DA6")?.cgColor
        contentView.layer.borderWidth = 2
        contentView.layer.cornerRadius = 5
        
    }

}
