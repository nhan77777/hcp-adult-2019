//
//  HCPSurvey.swift
//
//  Created by Long Quách Phi on 7/11/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class HCPSurvey: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let clientid = "clientid"
    static let hcpid = "hcpid"
    static let appid = "appid"
    static let usersysid = "usersysid"
    static let consent = "consent"
    static let feedback = "feedback"
    static let createAt = "create_at"
    static let appver = "appver"
    static let signature = "signature"
    static let phone = "phone"
    static let email = "email"
  }

  // MARK: Properties
  public var clientid: String?
  public var hcpid: String?
  public var appid: String?
  public var usersysid: String?
  public var consent: [String]?
  public var feedback: [Feedback]?
  public var createAt: String?
  public var appver: String?
  public var signature: String?
  public var email : String?
  public var phone : String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    clientid <- map[SerializationKeys.clientid]
    hcpid <- map[SerializationKeys.hcpid]
    appid <- map[SerializationKeys.appid]
    usersysid <- map[SerializationKeys.usersysid]
    consent <- map[SerializationKeys.consent]
    feedback <- map[SerializationKeys.feedback]
    createAt <- map[SerializationKeys.createAt]
    appver <- map[SerializationKeys.appver]
    signature <- map[SerializationKeys.signature]
    phone <- map[SerializationKeys.phone]
    email <- map[SerializationKeys.email]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = clientid { dictionary[SerializationKeys.clientid] = value }
    if let value = hcpid { dictionary[SerializationKeys.hcpid] = value }
    if let value = appid { dictionary[SerializationKeys.appid] = value }
    if let value = usersysid { dictionary[SerializationKeys.usersysid] = value }
    if let value = consent { dictionary[SerializationKeys.consent] = value }
    if let value = feedback { dictionary[SerializationKeys.feedback] = value.map { $0.dictionaryRepresentation() } }
    if let value = createAt { dictionary[SerializationKeys.createAt] = value }
    if let value = appver { dictionary[SerializationKeys.appver] = value }
    if let value = signature { dictionary[SerializationKeys.signature] = value }
    if let value = phone { dictionary[SerializationKeys.phone] = value }
    if let value = email { dictionary[SerializationKeys.email] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.clientid = aDecoder.decodeObject(forKey: SerializationKeys.clientid) as? String
    self.hcpid = aDecoder.decodeObject(forKey: SerializationKeys.hcpid) as? String
    self.appid = aDecoder.decodeObject(forKey: SerializationKeys.appid) as? String
    self.usersysid = aDecoder.decodeObject(forKey: SerializationKeys.usersysid) as? String
    self.consent = aDecoder.decodeObject(forKey: SerializationKeys.consent) as? [String]
    self.feedback = aDecoder.decodeObject(forKey: SerializationKeys.feedback) as? [Feedback]
    self.createAt = aDecoder.decodeObject(forKey: SerializationKeys.createAt) as? String
    self.appver = aDecoder.decodeObject(forKey: SerializationKeys.appver) as? String
    self.signature = aDecoder.decodeObject(forKey: SerializationKeys.signature) as? String
    self.email = aDecoder.decodeObject(forKey: SerializationKeys.email) as? String
    self.phone = aDecoder.decodeObject(forKey: SerializationKeys.phone) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(clientid, forKey: SerializationKeys.clientid)
    aCoder.encode(hcpid, forKey: SerializationKeys.hcpid)
    aCoder.encode(appid, forKey: SerializationKeys.appid)
    aCoder.encode(usersysid, forKey: SerializationKeys.usersysid)
    aCoder.encode(consent, forKey: SerializationKeys.consent)
    aCoder.encode(feedback, forKey: SerializationKeys.feedback)
    aCoder.encode(createAt, forKey: SerializationKeys.createAt)
    aCoder.encode(appver, forKey: SerializationKeys.appver)
    aCoder.encode(signature, forKey: SerializationKeys.signature)
    aCoder.encode(email, forKey: SerializationKeys.email)
    aCoder.encode(phone, forKey: SerializationKeys.phone)
  }

}
