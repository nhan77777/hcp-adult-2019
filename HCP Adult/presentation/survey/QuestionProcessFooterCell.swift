//
//  QuestionProcessFooterCell.swift
//  HCP Adult
//
//  Created by Bình Phạm on 6/8/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import UIKit

class QuestionProcessFooterCell: UICollectionViewCell {

    @IBOutlet weak var viewNote: UIView!
    @IBOutlet weak var lbQuestions: UILabel!
    @IBOutlet weak var imgDone: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewNote.layer.cornerRadius = viewNote.bounds.width/2
        contentView.translatesAutoresizingMaskIntoConstraints = false
        viewNote.layer.cornerRadius = viewNote.bounds.width/2
        viewNote.layer.borderColor = UIColor(hexString: "#e1e1e1")?.cgColor
        viewNote.layer.borderWidth = 8
    }

}
