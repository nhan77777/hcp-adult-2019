//
//  EnsurePage11cVC.swift
//  HCP Adult
//
//  Created by Le Nhan on 1/7/19.
//  Copyright © 2019 Bình Phạm. All rights reserved.
//

import UIKit
import Async

class EnsurePage11cVC: BaseDetailVC, DetailView {
    @IBOutlet weak var imgIcon: UIImageView!
    
    @IBOutlet weak var imgBg: UIImageView!
    
    func loadImages() {
        loadResourceToImage("ensure_bg_page_11c", imgBg)
    }
    
    func resetAnimation() {
        imgIcon.layer.removeAllAnimations()
    }
    
    func startAnimation() {
        imgIcon.twinkle()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadImages()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        resetAnimation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Async.main(after: 0.1, {
            self.startAnimation()
        })
    }
}
