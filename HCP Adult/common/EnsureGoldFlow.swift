//
//  EnsureGoldFlow.swift
//  HCP Adult
//
//  Created by Le Nhan on 1/9/19.
//  Copyright © 2019 Bình Phạm. All rights reserved.
//

import Foundation

enum EnsureGoldFlow {
    case I1
    case I2
    case I3
    case S1
    case S2
    case S3
    case U
    case MAIN
}

enum FlowCase {
    case ICU
    case NOI_TIEU_HOA
    case NGOAI_TIEU_HOA
    case TIM_MACH_HO_HAP
    case NGOAI_CHAN_THUONG_CHINH_HINH
    case CHUYEN_KHOA_SAN
    case NOI_TIET
    case UNG_THU
    case NOI_KHOA_KHAC
    case NGOAI_KHOA_KHAC
    
    public static func getEnsureFlow(dept: String) -> FlowCase {
        
        if (dept.lowercased() == "icu"
            || dept.lowercased() == "hoi suc"
            || dept.lowercased() == "tieu hoa"
            || dept.lowercased() == "noi tieu tieu hoa"
            || dept.lowercased() == "ngoai tieu tieu hoa") {
            
            return .ICU
            
        } else if (dept.lowercased() == "tieu hoa"
            || dept.lowercased() == "noi tieu hoa") {
            return .NOI_TIEU_HOA
        } else if ( dept.lowercased() == "ngoai tieu tieu hoa") {
            
            return .NGOAI_TIEU_HOA
            
        } else if (dept.lowercased() == "tim mach"
            || dept.lowercased() == "ho hap"
            || dept.lowercased() == "phoi"
            || dept.lowercased() == "noi tim mach"
            || dept.lowercased() == "noi ho hap") {
            
            return .TIM_MACH_HO_HAP
            
        } else if (dept.lowercased() == "ngoai chan thuong"
            || dept.lowercased() == "ngoai chan thuong - bong"
            || dept.lowercased() == "ngoai chan thuong chinh hinh"
            || dept.lowercased() == "ngoai chan thuong chinh hinh - bong"
            || dept.lowercased() == "ngoai chinh hinh"
            || dept.lowercased() == "ngoai than kinh, cot song, ctch"
            || dept.lowercased() == "ngoai cot song"
            || dept.lowercased() == "ngoai ctch") {
            
            return .NGOAI_CHAN_THUONG_CHINH_HINH
            
        } else if (dept.lowercased() == "san"
            || dept.lowercased() == "khoa san"
            || dept.lowercased() == "phu san") {
            
            return .CHUYEN_KHOA_SAN
            
        } else if (dept.lowercased() == "noi tiet") {
            
            return .NOI_TIET
            
        } else if (dept.lowercased() == "ung thu") {
            
            return .UNG_THU
            
        } else if (dept.lowercased() == "khoa ngoai"
            || dept.lowercased() == "khoa ngoai tong hop"
            || dept.lowercased() == "khoa ngoai ung buu"
            || dept.lowercased() == "khoa ngoai 1"
            || dept.lowercased() == "khoa ngoai 2"
            || dept.lowercased() == "khoa ngoai 3"
            || dept.lowercased() == "khoa ngoai 4"
            || dept.lowercased() == "khoa ngoai 5"
            || dept.lowercased() == "khoa ngoai a3"
            || dept.lowercased() == "khoa ngoai a5"
            || dept.lowercased() == "khoa ngoai a6"
            || dept.lowercased() == "khoa ngoai b"
            || dept.lowercased() == "khoa ngoai b1"
            || dept.lowercased() == "khoa ngoai b2"
            || dept.lowercased() == "khoa ngoai bong"
            || dept.lowercased() == "khoa ngoai c"
            || dept.lowercased() == "khoa ngoai chan thuong"
            || dept.lowercased() == "khoa ngoai chan thuong chinh hinh"
            || dept.lowercased() == "khoa ngoai chan thuong than kinh"
            || dept.lowercased() == "khoa ngoai chan thuong va bong"
            || dept.lowercased() == "khoa ngoai chinh hinh"
            || dept.lowercased() == "khoa ngoai chung"
            || dept.lowercased() == "khoa ngoai d"
            || dept.lowercased() == "khoa ngoai e"
            || dept.lowercased() == "khoa ngoai gan mat"
            || dept.lowercased() == "khoa ngoai hau mon"
            || dept.lowercased() == "khoa ngoai long nguc"
            || dept.lowercased() == "khoa ngoai long nguc mach mau"
            || dept.lowercased() == "khoa ngoai long nguc mach mau than kinh"
            || dept.lowercased() == "khoa ngoai nhan dan"
            || dept.lowercased() == "khoa ngoai nieu"
            || dept.lowercased() == "khoa ngoai nieu a"
            || dept.lowercased() == "khoa ngoai nieu b"
            || dept.lowercased() == "khoa ngoai phu"
            || dept.lowercased() == "khoa ngoai sau mo"
            || dept.lowercased() == "khoa ngoai so nao va chan thuong chinh hinh"
            || dept.lowercased() == "khoa ngoai than kinh"
            || dept.lowercased() == "khoa ngoai than kinh 3b1"
            || dept.lowercased() == "khoa ngoai than kinh 3b3"
            || dept.lowercased() == "khoa ngoai tiet nieu"
            || dept.lowercased() == "khoa ngoai tong hop"
            || dept.lowercased() == "khoa ngoai tong hop b2"
            || dept.lowercased() == "khoa phau thuat"
            || dept.lowercased() == "phau thuat"
            || dept.lowercased() == "khoa phau thuat bung"
            || dept.lowercased() == "khoa phau thuat cot song"
            || dept.lowercased() == "khoa phau thuat gan mat"
            || dept.lowercased() == "khoa phau thuat gay me"
            || dept.lowercased() == "khoa phau thuat gay me hoi suc"
            || dept.lowercased() == "khoa phau thuat long nguc"
            || dept.lowercased() == "khoa phau thuat tao hinh"
            || dept.lowercased() == "khoa phau thuat tao hinh ham mat"
            || dept.lowercased() == "khoa phau thuat tham my"
            || dept.lowercased() == "khoa phau thuat than kinh"
            || dept.lowercased() == "khoa phau thuat tiet nieu"
            || dept.lowercased() == "khoa phau thuat tim mach"
            || dept.lowercased() == "ngoai"
            || dept.lowercased() == "ngoai 1 (ngoai tong hop)"
            || dept.lowercased() == "ngoai 2"
            || dept.lowercased() == "ngoai 3"
            || dept.lowercased() == "ngoai 4"
            || dept.lowercased() == "ngoai 5"
            || dept.lowercased() == "ngoai bong"
            || dept.lowercased() == "ngoai bung"
            || dept.lowercased() == "ngoai chuyen khoa"
            || dept.lowercased() == "ngoai gan mat"
            || dept.lowercased() == "ngoai long nguc"
            || dept.lowercased() == "ngoai long nguc mach mau"
            || dept.lowercased() == "ngoai long nguc mach mau than kinh"
            || dept.lowercased() == "ngoai long nguc- mach mau- tong quat-tiet nieu"
            || dept.lowercased() == "ngoai nhan dan"
            || dept.lowercased() == "ngoai nieu"
            || dept.lowercased() == "ngoai nieu a"
            || dept.lowercased() == "ngoai than kinh"
            || dept.lowercased() == "ngoai than kinh cot song"
            || dept.lowercased() == "ngoai than tiet nieu"
            || dept.lowercased() == "ngoai theo yeu cau"
            || dept.lowercased() == "ngoai tiet nieu"
            || dept.lowercased() == "ngoai tiet nieu- long nguc"
            || dept.lowercased() == "ngoai tim long nguc"
            || dept.lowercased() == "ngoai tkln"
            || dept.lowercased() == "ngoai tong quat"
            || dept.lowercased() == "ngoai tong hop"
            || dept.lowercased() == "ngoai tong quat"
            || dept.lowercased() == "ngoai tong quat 4"
            || dept.lowercased() == "ngoai tong quat b"
            || dept.lowercased() == "ngoai tq"
            || dept.lowercased() == "ngoai ung buu"
            || dept.lowercased() == "ngoai ub"
            || dept.lowercased() == "ngoai ung buou"
            || dept.lowercased() == "ngoai nieu c") {
            
            return .NGOAI_KHOA_KHAC
            
        } else {
            
            return .NOI_KHOA_KHAC
            
        }
    }
}
