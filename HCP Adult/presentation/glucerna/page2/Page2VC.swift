//
//  Page2VC.swift
//  Glucerna
//
//  Created by Long Quách Phi on 3/22/18.
//  Copyright © 2018 Fractal. All rights reserved.
//

import UIKit

class Page2VC: BaseDetailVC, DetailView {
    
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet var viewFooter: [UIView]!
    
    func loadImages() {
        loadResourceToImage("glu_bg_page2", imgBackground)
    }
    
    func resetAnimation() {
    }
    
    func startAnimation() {
    }
    
    func setupActionFooter(){
        for i in 0...viewFooter.count - 1{
            viewFooter[i].addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onViewTapFooter(_:))))
            viewFooter[i].isUserInteractionEnabled = true
        }
    }
    
    @objc func onViewTapFooter(_ sender : UITapGestureRecognizer){
        showPDFView(filename: "glu_2")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pageID = "page31"
        loadImages()
        setupActionFooter()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        resetAnimation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        startAnimation()
    }
    
}

