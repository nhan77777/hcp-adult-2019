//
//  EnsurePageSevenVC.swift
//  HCP Adult
//
//  Created by Long Quách Phi on 5/24/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import UIKit

class EnsurePage3cVC: BaseDetailVC, DetailView {
    
    @IBOutlet weak var imgBg: UIImageView!
    
    func loadImages() {
        loadResourceToImage("ensure_bg_page_3c", imgBg)
        
    }
    
    func resetAnimation() {
        
    }
    
    func startAnimation() {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadImages()
        
    }
    
    @objc func onIconTap(_ sender : UITapGestureRecognizer){
        boucingView(sender.view!, 1.5, 1.0)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        resetAnimation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        startAnimation()
    }
    
}
