//
//  EnsureHomeVC.swift
//  HCP Adult
//
//  Created by Long Quách Phi on 5/24/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import UIKit
import Pageboy
import Nuke
import SideMenu
import Instructions
import PopupDialog
import Async
import Foundation
import ObjectMapper

class Store {
    public static var sectionChoose = 0
}

class EnsureHomeVC: PageboyViewController, PageView {
    
    var flow: EnsureGoldFlow = .MAIN
    
    func onChangeFlow(vc: UIViewController) {
        present(vc, animated: true, completion: nil)
    }
    
    func onRequestEndCall() {
        let popup = PopupDialog(viewController: vc!, buttonAlignment: .vertical, transitionStyle: .bounceUp, preferredWidth: 450, tapGestureDismissal: true, hideStatusBar: false, completion: nil)
        present(popup, animated: true, completion: nil)
    }
    
    
    @IBOutlet weak var imgMenu: UIImageView!
    @IBOutlet weak var btnClose: UIImageView!
    private var vc : ConfirmDialogVC?
    
    private var timer = Timer()
    private var durationRecord : DurationRecord?
    private var currentPageIndex = 0
    
    func initialPopupDialog(){
        vc = ConfirmDialogVC(nibName: "ConfirmDialogVC", bundle: nil)
        vc?.negativeText = "Quay lại"
        vc?.positiveText = "Kết thúc"
        vc?.messenge = "Bạn có chắc chắn muốn kết thúc trò chuyện ?"
        vc?.callbackPositive = {vc in
            self.pauseTimer()
            self.updateEndTime()
            self.present(SurveyVC(nibName: "SurveyVC", bundle: nil), animated: true, completion: nil)
        }
        vc?.callbackNegative = {vc in
        }
    }
    
    func moveToPage(index: Int) {
        if index == -1 {
            scrollToPage(Page.at(index: currentPageIndex + 1), animated: true)
        } else {
            scrollToPage(Page.at(index: index), animated: true)
        }
    }
    
    func setTransition(_ trans: PageboyViewController.Transition) {
        transition = trans
    }
    
    //RULE :- Save last slide time when slide to new slide
    @objc func actionTimer(){
        print(orderedViewControllers[currentPageIndex].pageID)
        durationRecord?.durations[orderedViewControllers[currentPageIndex].pageID] = (durationRecord?.durations[orderedViewControllers[currentPageIndex].pageID])! + 1
        if let record = durationRecord {
            Utils.saveLastDuration(duration: record)
        }
    }
    
    func startTimer(){
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(actionTimer), userInfo: nil, repeats: true)
    }
    
    func pauseTimer(){
        timer.invalidate()
    }
    
    private var orderedViewControllers: [BaseDetailVC] = []
        
    
    func contentViewController(_ name: String, _ type : Int = 0) -> UIViewController {
        if type == 0 {
            let vc = UIStoryboard(name: "Main", bundle: nil) .
                instantiateViewController(withIdentifier: "\(name)VC")
            return vc
        }
        else {
            let vc = UIStoryboard(name: "Main", bundle: nil) .
                instantiateViewController(withIdentifier: "\(name)VC")
            return vc
        }
    }
    
    func updateEndTime(){
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
        let endTime = formatter.string(from: Date())
        durationRecord?.endtime = endTime
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dataSource = self
        delegate = self
        initialPopupDialog()
        loadResourceToImage("ensure_ic_menu", imgMenu)
        setupMenu()
        setActionImgMenu()
    }
    
    func setPageOnFlow(type: EnsureGoldFlow){
        
        let page1: EnsurePage1VC = {
            let page = EnsurePage1VC(nibName: "EnsurePage1VC", bundle: nil)
            (page as BaseDetailVC).pageID = "page67"
            return page
        }()
        
        let page2a: EnsurePage2aVC = {
            let page = EnsurePage2aVC(nibName: "EnsurePage2aVC", bundle: nil)
            (page as BaseDetailVC).pageID = "page68"
            return page
        }()
        
        let page2b: EnsurePage2bVC = {
            let page = EnsurePage2bVC(nibName: "EnsurePage2bVC", bundle: nil)
            (page as BaseDetailVC).pageID = "page70"
            return page
        }()
        
        let page2c: EnsurePage2cVC = {
            let page = EnsurePage2cVC(nibName: "EnsurePage2cVC", bundle: nil)
            (page as BaseDetailVC).pageID = "page72"
            return page
        }()
        
        let page3a: EnsurePage3aVC = {
            let page = EnsurePage3aVC(nibName: "EnsurePage3aVC", bundle: nil)
            (page as BaseDetailVC).pageID = "page69"
            return page
        }()
        
        let page3b: EnsurePage3bVC = {
            let page = EnsurePage3bVC(nibName: "EnsurePage3bVC", bundle: nil)
            (page as BaseDetailVC).pageID = "page71"
            return page
        }()
        
        let page3c: EnsurePage3cVC = {
            let page = EnsurePage3cVC(nibName: "EnsurePage3cVC", bundle: nil)
            (page as BaseDetailVC).pageID = "page73"
            return page
        }()
        
        let page4a: EnsurePage4aVC = {
            let page = EnsurePage4aVC(nibName: "EnsurePage4aVC", bundle: nil)
            (page as BaseDetailVC).pageID = "page74"
            return page
        }()
        
        let page4b: EnsurePage4bVC = {
            let page = EnsurePage4bVC(nibName: "EnsurePage4bVC", bundle: nil)
            (page as BaseDetailVC).pageID = "page75"
            return page
        }()
        
        let page4c: EnsurePage4cVC = {
            let page = EnsurePage4cVC(nibName: "EnsurePage4cVC", bundle: nil)
            (page as BaseDetailVC).pageID = "page76"
            return page
        }()
        
        let page5: EnsurePage5VC = {
            let page = EnsurePage5VC(nibName: "EnsurePage5VC", bundle: nil)
            (page as BaseDetailVC).pageID = "page77"
            return page
        }()
        
        let page6: EnsurePage6VC = {
            let page = EnsurePage6VC(nibName: "EnsurePage6VC", bundle: nil)
            (page as BaseDetailVC).pageID = "page78"
            return page
        }()
        
        let page7: EnsurePage7VC = {
            let page = EnsurePage7VC(nibName: "EnsurePage7VC", bundle: nil)
            (page as BaseDetailVC).pageID = "page79"
            return page
        }()
        
        let page8: EnsurePage8VC = {
            let page = EnsurePage8VC(nibName: "EnsurePage8VC", bundle: nil)
            (page as BaseDetailVC).pageID = "page80"
            return page
        }()
        
        let page9: EnsurePage9VC = {
            let page = EnsurePage9VC(nibName: "EnsurePage9VC", bundle: nil)
            (page as BaseDetailVC).pageID = "page81"
            return page
        }()
        
        let page10: EnsurePage10VC = {
            let page = EnsurePage10VC(nibName: "EnsurePage10VC", bundle: nil)
            (page as BaseDetailVC).pageID = "page81"
            return page
        }()
        
        let page11a: EnsurePage11aVC = {
            let page = EnsurePage11aVC(nibName: "EnsurePage11aVC", bundle: nil)
            (page as BaseDetailVC).pageID = "page82"
            return page
        }()
        
        let page11b: EnsurePage11bVC = {
            let page = EnsurePage11bVC(nibName: "EnsurePage11bVC", bundle: nil)
            (page as BaseDetailVC).pageID = "page83"
            return page
        }()
        
        let page11c: EnsurePage11cVC = {
            let page = EnsurePage11cVC(nibName: "EnsurePage11cVC", bundle: nil)
            (page as BaseDetailVC).pageID = "page84"
            return page
        }()
        
        let page12: EnsurePage12VC = {
            let page = EnsurePage12VC(nibName: "EnsurePage12VC", bundle: nil)
            (page as BaseDetailVC).pageID = "page85"
            return page
        }()
        
        var page13: EnsurePage13VC = {
            let page = EnsurePage13VC(nibName: "EnsurePage13VC", bundle: nil)
            (page as BaseDetailVC).pageID = "page86"
            return page
        }()
        
        var _: EnsurePage14VC = {
            let page = EnsurePage14VC(nibName: "EnsurePage14VC", bundle: nil)
            (page as BaseDetailVC).pageID = "page87"
            return page
        }()
        
        var page15: EnsurePage15VC = {
            let page = EnsurePage15VC(nibName: "EnsurePage15VC", bundle: nil)
            (page as BaseDetailVC).pageID = "page88"
            return page
        }()
        
        var page16a: EnsurePage16aVC = {
            let page = EnsurePage16aVC(nibName: "EnsurePage16aVC", bundle: nil)
            (page as BaseDetailVC).pageID = "page89"
            return page
        }()
        
        var page16b: EnsurePage16bVC = {
            let page = EnsurePage16bVC(nibName: "EnsurePage16bVC", bundle: nil)
            (page as BaseDetailVC).pageID = "page90"
            return page
        }()
        
        var page16c: EnsurePage16cVC = {
            let page = EnsurePage16cVC(nibName: "EnsurePage16cVC", bundle: nil)
            (page as BaseDetailVC).pageID = "page91"
            return page
        }()
        
        var page17: EnsurePage17VC = {
            let page = EnsurePage17VC(nibName: "EnsurePage17VC", bundle: nil)
            (page as BaseDetailVC).pageID = "page92"
            return page
        }()
        
        var page18: EnsurePage18VC = {
            let page = EnsurePage18VC(nibName: "EnsurePage18VC", bundle: nil)
            (page as BaseDetailVC).pageID = "page93"
            return page
        }()
        
        switch type {
        
        case .I1:
            orderedViewControllers = [page1, page2a, page3a, page4a, page5, page6, page10, page8, page7, page11c, page12]
        case .I2:
            orderedViewControllers = [page1, page2a, page3a, page4a, page5, page6, page8, page7, page11b, page12]
        case .I3:
            orderedViewControllers = [page1, page2a, page3a, page4a, page5, page6, page7, page8, page11a, page12]
        case .S1:
            orderedViewControllers = [page1, page2b, page3b, page4b, page5, page6, page10, page8, page7, page11c, page12]
        case .S2:
            orderedViewControllers = [page1, page2b, page3b, page4b, page5, page6, page10, page9, page7, page11c, page12]
        case .S3:
            orderedViewControllers = [page1, page2b, page3b, page4b, page5, page6, page7, page8, page10, page11a]
        case .U:
            orderedViewControllers = [page1, page2c, page3c, page4c, page5, page6, page10, page8, page7, page11c, page12]
        case .MAIN:
            orderedViewControllers = [page1, page2a, page3a, page2b, page3b, page2c, page3c, page4a, page4b, page4c, page5, page6, page7, page8, page9, page10, page11a, page11b, page11c, page12]
        }
        
        page2a.delegate = self
        page2b.delegate = self
        page2c.delegate = self
        
        reloadData()
        
        debugPrint("Flow = \(flow)")
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        durationRecord = Utils.getLastDuration()
        print(durationRecord?.toJsonString())
        pauseTimer()
        startTimer()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setPageOnFlow(type: flow)
    }
    
    @objc func onMenuTap(_ sender : UITapGestureRecognizer) {
        present(customSideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
    }
    
    @objc func onCloseTap(_ sender : UITapGestureRecognizer) {
        let containerAppearance = PopupDialogContainerView.appearance()
        containerAppearance.cornerRadius = 15
        let popup = PopupDialog(viewController: vc!, buttonAlignment: .vertical, transitionStyle: .bounceUp, preferredWidth: 450, tapGestureDismissal: true, hideStatusBar: false, completion: nil)
        self.present(popup, animated: true, completion: nil)
    }
    
    func setActionImgMenu(){
        imgMenu.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onMenuTap(_:))))
        btnClose.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onCloseTap(_:))))
    }
    
    let customSideMenuManager = SideMenuManager()
    
    func setupMenu(){
        let menuController = EnsureMenuVC(nibName: "EnsureMenuVC", bundle: nil)
        menuController.homeController = self
        menuController.targetFlow = Utils.getFlowableIndex(0)[0]
        let menuLeftNavigationController = UISideMenuNavigationController(rootViewController: menuController)
        customSideMenuManager.menuLeftNavigationController = menuLeftNavigationController
        customSideMenuManager.menuPresentMode = .menuSlideIn
        customSideMenuManager.menuEnableSwipeGestures = true
        customSideMenuManager.menuFadeStatusBar = false
        customSideMenuManager.menuAnimationOptions = .curveLinear
        customSideMenuManager.menuWidth = 620
        customSideMenuManager.menuShadowColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
    }
    
}

extension EnsureHomeVC : UISideMenuNavigationControllerDelegate {
    
    func sideMenuWillAppear(menu: UISideMenuNavigationController, animated: Bool) {
    }
    
    func sideMenuDidAppear(menu: UISideMenuNavigationController, animated: Bool) {
    }
    
    func sideMenuWillDisappear(menu: UISideMenuNavigationController, animated: Bool) {
    }
    
    func sideMenuDidDisappear(menu: UISideMenuNavigationController, animated: Bool) {
    }
    
}

extension EnsureHomeVC : PageboyViewControllerDelegate {
    
    func pageboyViewController(_ pageboyViewController: PageboyViewController, didScrollTo position: CGPoint, direction: PageboyViewController.NavigationDirection, animated: Bool) {
        
    }
    
    func pageboyViewController(_ pageboyViewController: PageboyViewController, didReloadWith currentViewController: UIViewController, currentPageIndex: PageboyViewController.PageIndex) {
        
    }
    
    func pageboyViewController(_ pageboyViewController: PageboyViewController,
                               didScrollToPageAt index: Int,
                               direction: PageboyViewController.NavigationDirection,
                               animated: Bool){
        currentPageIndex = index
    }
    
    func pageboyViewController(_ pageboyViewController: PageboyViewController, willScrollToPageAt index: Int, direction: PageboyViewController.NavigationDirection, animated: Bool) {
    }
}

extension EnsureHomeVC : PageboyViewControllerDataSource {
    
    func numberOfViewControllers(in pageboyViewController: PageboyViewController) -> Int {
        return orderedViewControllers.count
    }
    
    func viewController(for pageboyViewController: PageboyViewController, at index: PageboyViewController.PageIndex) -> UIViewController? {
        return orderedViewControllers[index]
    }
    
    func defaultPage(for pageboyViewController: PageboyViewController) -> PageboyViewController.Page? {
        return nil
    }
}

extension Date {
    var ticks: UInt64 {
        return UInt64((self.timeIntervalSince1970 + 62_135_596_800) * 10_000_000)
    }
}

extension String {
    mutating func replace(_ originalString:String, with newString:String) {
        self = self.replacingOccurrences(of: originalString, with: newString)
    }
}


