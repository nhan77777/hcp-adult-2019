//
//  ProsurePage8VC.swift
//  HCP Adult
//
//  Created by Long Quách Phi on 8/6/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import UIKit

class ProsurePage8VC: BaseDetailVC, DetailView {
    
    @IBOutlet weak var imgBg: UIImageView!
    
    func loadImages() {
        loadResourceToImage("pro_bg_page_8", imgBg)
    }
    
    func resetAnimation() {
        
    }
    
    func startAnimation() {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadImages()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
