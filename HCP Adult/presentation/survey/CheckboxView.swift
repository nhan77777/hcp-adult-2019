//
//  CheckBoxView.swift
//  HCP Adult
//
//  Created by Bình Phạm on 6/11/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import UIKit
import M13Checkbox

class CheckboxView: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var checkBox: M13Checkbox!
    @IBOutlet weak var lbTitle: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit(){
        Bundle.main.loadNibNamed("CheckboxView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }

}
