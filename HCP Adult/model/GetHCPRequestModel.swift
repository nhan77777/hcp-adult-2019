//
//  GetHCPRequestModel.swift
//  HCP Adult
//
//  Created by Long Quách Phi on 6/1/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import Foundation
import ObjectMapper

public final class GetHCPRequestModel: Mappable, NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let userid = "userid"
    }
    
    // MARK: Properties
    public var userid: String?
    
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public required init?(map: Map){
        
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        userid <- map[SerializationKeys.userid]
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = userid { dictionary[SerializationKeys.userid] = value }
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.userid = aDecoder.decodeObject(forKey: SerializationKeys.userid) as? String
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(userid, forKey: SerializationKeys.userid)
    }
    
}

