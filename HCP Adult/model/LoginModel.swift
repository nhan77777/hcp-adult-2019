//
//  Login.swift
//
//  Created by Long Quách Phi on 5/29/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class LoginModel: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let status = "status"
    static let sysid = "sysid"
    static let name = "name"
    static let msg = "msg"
  }

  // MARK: Properties
  public var status: String?
  public var sysid: String?
  public var name: String?
  public var msg: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }
    
    init() {
        
    }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    status <- map[SerializationKeys.status]
    sysid <- map[SerializationKeys.sysid]
    name <- map[SerializationKeys.name]
    msg <- map[SerializationKeys.msg]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = status { dictionary[SerializationKeys.status] = value }
    if let value = sysid { dictionary[SerializationKeys.sysid] = value }
    if let value = name { dictionary[SerializationKeys.name] = value }
    if let value = msg { dictionary[SerializationKeys.msg] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.status = aDecoder.decodeObject(forKey: SerializationKeys.status) as? String
    self.sysid = aDecoder.decodeObject(forKey: SerializationKeys.sysid) as? String
    self.name = aDecoder.decodeObject(forKey: SerializationKeys.name) as? String
    self.msg = aDecoder.decodeObject(forKey: SerializationKeys.msg) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(status, forKey: SerializationKeys.status)
    aCoder.encode(sysid, forKey: SerializationKeys.sysid)
    aCoder.encode(name, forKey: SerializationKeys.name)
    aCoder.encode(msg, forKey: SerializationKeys.msg)
  }

}
