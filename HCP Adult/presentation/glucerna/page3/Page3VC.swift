//
//  Page3VC.swift
//  Glucerna
//
//  Created by Long Quách Phi on 3/22/18.
//  Copyright © 2018 Fractal. All rights reserved.
//

import UIKit
import Async

class Page3VC: BaseDetailVC, DetailView {
    
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet var imgICContent: [UIImageView]!
    @IBOutlet weak var viewRotate: UIImageView!
    @IBOutlet weak var viewTwinkle: UIView!
    @IBOutlet var viewFooter: [UIView]!
    
    func loadImages() {
        loadResourceToImage("glu_bg_page3", imgBackground)
        loadResourceToImage("glu_ic_onlyone", imgICContent[0])
        loadResourceToImage("glu_ic_milkbottle_purple", imgICContent[1])
        loadResourceToImage("glu_ic_circle", viewRotate)
    }
    
    func setupActionFooter(){
        for i in 0...viewFooter.count - 1{
            viewFooter[i].addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onViewTapFooter(_:))))
            viewFooter[i].isUserInteractionEnabled = true
            viewFooter[i].tag = i
        }
    }
    
    @objc func onViewTapFooter(_ sender : UITapGestureRecognizer){
        let index = (sender.view?.tag)!
        switch index {
        case 0:
            showPDFView(filename: "glu_0")
        case 1:
            showPDFView(filename: "glu_1")
        default:
            print("error")
        }
    }
    
    func resetAnimation() {
        viewRotate.layer.removeAllAnimations()
        viewTwinkle.layer.removeAllAnimations()
    }
    
    func startAnimation() {
        rotateView(viewRotate, 4)
        viewTwinkle.twinkle()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pageID = "page32"
        loadImages()
        setupActionFooter()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        resetAnimation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Async.main(after: 0.01) {
            self.startAnimation()
        }
        
    }
    
}
