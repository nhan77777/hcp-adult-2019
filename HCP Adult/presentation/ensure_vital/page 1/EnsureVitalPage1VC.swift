//
//  EnsureVitalPage1VC.swift
//  HCP Adult
//
//  Created by Long Quách Phi on 11/23/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import UIKit
import Async

class EnsureVitalPage1VC: BaseDetailVC, DetailView {

    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var icMilk: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadImages()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        resetAnimation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Async.main(after: 0.3){
            self.startAnimation()
        }
    }
    
    func loadImages() {
        loadResourceToImage("bg_vital_page_1", imgBG)
        loadResourceToImage("ic_vital_page_4", icMilk)
    }
    
    func resetAnimation() {
        icMilk.layer.removeAllAnimations()
    }
    
    func startAnimation() {
        icMilk.twinkle()
    }

}
