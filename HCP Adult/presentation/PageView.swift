//
//  HomeView.swift
//  SimilacNeosure
//
//  Created by Bình Phạm on 3/12/18.
//  Copyright © 2018 Fractal. All rights reserved.
//

import Foundation
import Pageboy

protocol PageView {
    
    func onChangeFlow(vc : UIViewController)
    
    func onRequestEndCall()
    
    func moveToPage(index : Int)
    
    func setTransition(_ trans : PageboyViewController.Transition)
    
    func startTimer()
    
    func pauseTimer()
    
}
