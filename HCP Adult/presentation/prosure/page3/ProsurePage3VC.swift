//
//  ProsurePage3VC.swift
//  HCP Adult
//
//  Created by Bình Phạm on 7/25/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import UIKit
import Async

class ProsurePage3VC: BaseDetailVC, DetailView {
    
    @IBOutlet weak var imgBg: UIImageView!
    @IBOutlet var imgIcons: [UIImageView]!
    
    func loadImages() {
        loadResourceToImage("pro_bg_page_3", imgBg)
        for i in 0...imgIcons.count-2 {
            loadResourceToImage("pro_ic_page_3_\(i)", imgIcons[i])
        }
        loadResourceToImage("pro_ic_page_1_0", imgIcons[imgIcons.count-1])
    }
    
    func resetAnimation() {
        for ic in imgIcons {
            ic.layer.removeAllAnimations()
            ic.stopGlowing()
        }
    }
    
    func startAnimation() {
        for i in 0...imgIcons.count-2 {
            imgIcons[i].startGlowing()
        }
        imgIcons[imgIcons.count - 1].twinkle()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadImages()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Async.main(after: 0.1, {
            self.startAnimation()
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
