//
//  ImagePopupViewerVC.swift
//  HCP Adult
//
//  Created by Le Nhan on 1/12/19.
//  Copyright © 2019 Bình Phạm. All rights reserved.
//

import UIKit

class ImagePopupViewerVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        imgViewBG.image = image
    }
    
    public var homeView : PageView?
    @IBOutlet weak var imgViewBG: UIImageView!
    public var image: UIImage?
    
    @IBAction func abtnOnClose(_ sender: Any) {
        dismiss(animated: true) {
            self.homeView?.startTimer()
        }
    }
}
