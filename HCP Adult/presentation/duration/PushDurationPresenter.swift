//
//  DurationViewController.swift
//  HCP Adult
//
//  Created by Long Quách Phi on 7/3/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import UIKit

class PushDurationPresenter {
    fileprivate let durationService : PushDuration
    weak fileprivate var pushDurationView : PushDurationView?
    
    init(durationService : PushDuration){
        self.durationService = durationService
    }
    
    func attachView(_ view : PushDurationView){
        pushDurationView = view
    }
    
    func detachView() {
        pushDurationView = nil
    }
    
    func pushDuration(durationRequestModel: Dictionary<String, Any>){
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        pushDurationView?.showLoading("Uploading ... ")
        durationService.pushDuration(durationRequestModel: durationRequestModel) { success, response  in
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            self.pushDurationView?.hideLoading()
            if success{
                if let status = response.status {
                    if (status.elementsEqual("true")){
                        self.pushDurationView?.pushDurationSuccess(response)
                    } else {
                        self.pushDurationView?.pushDurationFailed(response.msg!)
                    }
                }
                else{
                    self.pushDurationView?.pushDurationFailed("Kết nối thất bại")
                }
            }
            else{
                self.pushDurationView?.pushDurationFailed("Kết nối thất bại")
            }
        }
    }
    
}
