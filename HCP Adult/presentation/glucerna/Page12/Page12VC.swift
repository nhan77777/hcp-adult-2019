//
//  Page2VC.swift
//  Glucerna
//
//  Created by Long Quách Phi on 3/22/18.
//  Copyright © 2018 Fractal. All rights reserved.
//

import UIKit

class Page12VC: BaseDetailVC, DetailView {
    
    @IBOutlet weak var imgTopBar: UIImageView!
    @IBOutlet weak var scr: UIScrollView!
    @IBOutlet weak var imgBackground: UIImageView!
    
    func loadImages() {
        loadResourceToImage("glu_bg_page12", imgBackground)
        loadResourceToImage("glu_ic_page12", imgTopBar)
    }
    
    func resetAnimation() {
    }
    
    func startAnimation() {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pageID = "page41"
        loadImages()
        scr.isScrollEnabled = true
        scr.contentSize = CGSize(width: 1024, height: 2346)
        scr.addSubview(imgBackground)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        resetAnimation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        startAnimation()
    }
    
}

