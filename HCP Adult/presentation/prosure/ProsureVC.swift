//
//  ProsureVC.swift
//  HCP Adult
//
//  Created by Bình Phạm on 7/25/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import UIKit
import Pageboy
import Nuke
import SideMenu
import Instructions
import PopupDialog
import Async
import Foundation
import ObjectMapper

class ProsureHomeVC: PageboyViewController, PageView {
    
    func onChangeFlow(vc: UIViewController) {
        present(vc, animated: true, completion: nil)
    }
    
    func onRequestEndCall() {
        let popup = PopupDialog(viewController: vc!, buttonAlignment: .vertical, transitionStyle: .bounceUp, preferredWidth: 450, tapGestureDismissal: true, hideStatusBar: false, completion: nil)
        self.present(popup, animated: true, completion: nil)
    }
    

    @IBOutlet weak var btnMenu: UIImageView!
    @IBAction func onCloseTap(_ sender: Any) {
        let popup = PopupDialog(viewController: vc!, buttonAlignment: .vertical, transitionStyle: .bounceUp, preferredWidth: 450, tapGestureDismissal: true, hideStatusBar: false, completion: nil)
        self.present(popup, animated: true, completion: nil)
    }
    
    private var vc : ConfirmDialogVC?
    
    private var timer = Timer()
    private var durationRecord : DurationRecord?
    private var currentPageIndex = 0
    
    func initialPopupDialog(){
        let containerAppearance = PopupDialogContainerView.appearance()
        containerAppearance.cornerRadius = 15
        vc = ConfirmDialogVC(nibName: "ConfirmDialogVC", bundle: nil)
        vc?.negativeText = "Quay lại"
        vc?.positiveText = "Kết thúc"
        vc?.messenge = "Bạn có chắc chắn muốn kết thúc trò chuyện ?"
        vc?.callbackPositive = {vc in
            self.pauseTimer()
            self.updateEndTime()
            self.present(SurveyVC(nibName: "SurveyVC", bundle: nil), animated: true, completion: nil)
        }
        vc?.callbackNegative = {vc in
        }
    }
    
    func moveToPage(index: Int) {
        scrollToPage(Page.at(index: index), animated: true)
    }
    
    func setTransition(_ trans: PageboyViewController.Transition) {
        transition = trans
    }
    
    @objc func actionTimer(){
        print(orderedViewControllers[currentPageIndex].pageID)
        durationRecord?.durations[orderedViewControllers[currentPageIndex].pageID] = (durationRecord?.durations[orderedViewControllers[currentPageIndex].pageID])! + 1
        if let record = durationRecord {
            Utils.saveLastDuration(duration: record)
        }
    }
    
    func startTimer(){
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(actionTimer), userInfo: nil, repeats: true)
    }
    
    func pauseTimer(){
        timer.invalidate()
    }
    
    func updateCurrentpage(currentPage: Int) {
        currentPageIndex = currentPage
    }
    
    private(set) lazy var orderedViewControllers: [BaseDetailVC] = {
        [
            ProsurePage1VC(nibName: "ProsurePage1VC", bundle: nil),//42
            ProsurePage2VC(nibName: "ProsurePage2VC", bundle: nil),//43
            ProsurePage3VC(nibName: "ProsurePage3VC", bundle: nil),//44
            ProsurePage4VC(nibName: "ProsurePage4VC", bundle: nil),//45
            ProsurePage5VC(nibName: "ProsurePage5VC", bundle: nil),//46
            ProsurePage6VC(nibName: "ProsurePage6VC", bundle: nil),//47
            ProsurePage7VC(nibName: "ProsurePage7VC", bundle: nil),//48
            ProsurePage8VC(nibName: "ProsurePage8VC", bundle: nil),//49
            ProsurePage9VC(nibName: "ProsurePage9VC", bundle: nil),//50
            ProsurePage10VC(nibName: "ProsurePage10VC", bundle: nil),//51
            ProsurePage11VC(nibName: "ProsurePage11VC", bundle: nil),//52
            ProsurePage12VC(nibName: "ProsurePage12VC", bundle: nil),//53

        ]
    }()
    
    func contentViewController(_ name: String, _ type : Int = 0) -> UIViewController {
        if type == 0 {
            let vc = UIStoryboard(name: "Main", bundle: nil) .
                instantiateViewController(withIdentifier: "\(name)VC")
            return vc
        }
        else {
            let vc = UIStoryboard(name: "Main", bundle: nil) .
                instantiateViewController(withIdentifier: "\(name)VC")
            return vc
        }
    }
    
    func updateEndTime(){
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
        let endTime = formatter.string(from: Date())
        durationRecord?.endtime = endTime
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        for i in 0...orderedViewControllers.count - 1 {
            orderedViewControllers[i].pageID = "page\(42+i)"
        }
        dataSource = self
        delegate = self
        initialPopupDialog()
        setupMenu()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        durationRecord = Utils.getLastDuration()
        pauseTimer()
        startTimer()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    @objc func onMenuTap(_ sender : UITapGestureRecognizer) {
        present(customSideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
    }
    
    let customSideMenuManager = SideMenuManager()
    
    func setupMenu(){
        let flowable = Utils.getFlowableIndex(2)[0]
        let menuController = ProsureMenuVC(nibName: "ProsureMenuVC", bundle: nil)
        menuController.homeView = self
        menuController.targetFlow = flowable
        let menuLeftNavigationController = UISideMenuNavigationController(rootViewController: menuController)
        customSideMenuManager.menuLeftNavigationController = menuLeftNavigationController
        customSideMenuManager.menuShadowColor = #colorLiteral(red: 0.9098039269, green: 0.4784313738, blue: 0.6431372762, alpha: 1)
        customSideMenuManager.menuAllowPushOfSameClassTwice = false
        customSideMenuManager.menuPresentMode = .menuSlideIn
        customSideMenuManager.menuAnimationPresentDuration = 0.5
        customSideMenuManager.menuAnimationDismissDuration = 0.5
        customSideMenuManager.menuAnimationBackgroundColor = UIColor.clear
        customSideMenuManager.menuWidth = 620
        customSideMenuManager.menuEnableSwipeGestures = true
        btnMenu.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onMenuTap(_:))))
    }
    
}

extension ProsureHomeVC : UISideMenuNavigationControllerDelegate {
    
    func sideMenuWillAppear(menu: UISideMenuNavigationController, animated: Bool) {
    }
    
    func sideMenuDidAppear(menu: UISideMenuNavigationController, animated: Bool) {
    }
    
    func sideMenuWillDisappear(menu: UISideMenuNavigationController, animated: Bool) {
    }
    
    func sideMenuDidDisappear(menu: UISideMenuNavigationController, animated: Bool) {
    }
    
}

extension ProsureHomeVC : PageboyViewControllerDelegate {
    func pageboyViewController(_ pageboyViewController: PageboyViewController, didScrollTo position: CGPoint, direction: PageboyViewController.NavigationDirection, animated: Bool) {
        
    }
    
    func pageboyViewController(_ pageboyViewController: PageboyViewController, didReloadWith currentViewController: UIViewController, currentPageIndex: PageboyViewController.PageIndex) {
        
    }
    
    func pageboyViewController(_ pageboyViewController: PageboyViewController,
                               didScrollToPageAt index: Int,
                               direction: PageboyViewController.NavigationDirection,
                               animated: Bool){
        currentPageIndex = index
    }
    
    func pageboyViewController(_ pageboyViewController: PageboyViewController, willScrollToPageAt index: Int, direction: PageboyViewController.NavigationDirection, animated: Bool) {
    }
}

extension ProsureHomeVC : PageboyViewControllerDataSource {
    
    func numberOfViewControllers(in pageboyViewController: PageboyViewController) -> Int {
        return orderedViewControllers.count
    }
    
    func viewController(for pageboyViewController: PageboyViewController, at index: PageboyViewController.PageIndex) -> UIViewController? {
        return orderedViewControllers[index]
    }
    
    func defaultPage(for pageboyViewController: PageboyViewController) -> PageboyViewController.Page? {
        return nil
    }
    
}
