//
//  HomeView.swift
//  EnsureHMP
//
//  Created by Bình Phạm on 2/22/18.
//

import Foundation

import Foundation
import Pageboy

protocol HomeView {
    func moveToPage(index : Int)
    func setTransition(_ trans : PageboyViewController.Transition)
}

