//
//  EnsurePage15VC.swift
//  HCP Adult
//
//  Created by Le Nhan on 1/7/19.
//  Copyright © 2019 Bình Phạm. All rights reserved.
//

import UIKit
import AVKit

class EnsurePage15VC: BaseDetailVC, DetailView {
    
    @IBOutlet weak var videoPlayerView: UIView!
    var player: AVPlayer?
    let playerController = AVPlayerViewController()
    
    @IBOutlet weak var imgBg: UIImageView!
    
    func loadImages() {
        loadResourceToImage("ensure_bg_page_15", imgBg)
    }
    
    func resetAnimation() {
        
    }
    
    func startAnimation() {
        
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadImages()

        guard let path = Bundle.main.path(forResource: "HMB_VN", ofType:"m4v") else {
            debugPrint("HMB_VN.m4v not found")
            return
        }
        player = AVPlayer(url: URL(fileURLWithPath: path))
        let playerController = AVPlayerViewController()
        playerController.view.frame = videoPlayerView.bounds
        playerController.showsPlaybackControls = true
        playerController.player = player
        
        addChild(playerController)
        playerController.didMove(toParent: self)
        videoPlayerView.addSubview(playerController.view)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        player?.seek(to: .zero)
        player?.play()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        player?.rate = 0
    }
}

