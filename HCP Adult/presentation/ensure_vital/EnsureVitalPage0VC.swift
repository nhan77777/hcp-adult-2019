//
//  EnsureVitalPage0VC.swift
//  HCP Adult
//
//  Created by Bình Phạm on 11/1/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import UIKit
import Pageboy
import SideMenu
import PopupDialog

class EnsureVitalPage0VC: PageboyViewController, PageView {
    
    func onChangeFlow(vc: UIViewController) {
        present(vc, animated: true, completion: nil)
    }
    
    func onRequestEndCall() {
        let popup = PopupDialog(viewController: vc!, buttonAlignment: .vertical, transitionStyle: PopupDialogTransitionStyle.fadeIn, preferredWidth: 450, tapGestureDismissal: true, hideStatusBar: false, completion: nil)
        present(popup, animated: true, completion: nil)
    }
    
    @IBAction func onMenuTap(_ sender: Any) {
        present(customSideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
    }
    
    let customSideMenuManager = SideMenuManager()
    
    func setupMenu(){
        var items : [MenuItem] = []
        items.append(MenuItem(title: "SUY DINH DƯỠNG VÀ HẬU QUẢ Ở BỆNH NHÂN NẶNG", type: -1, color : UIColor(hexString: "#EB3A94")!))
        items.append(MenuItem(title: "TÌNH TRẠNG KÉM DUNG NẠP: TRIỆU CHỨNG, HẬU QUẢ & KHUYẾN NGHỊ", type: -1, color : UIColor(hexString: "#EB3A94")!))
        items.append(MenuItem(title: "VITAL - GIẢI PHÁP DINH DƯỠNG ƯU VIỆT CHO BỆNH NHÂN NẶNG" , type: -1, color : UIColor(hexString: "#EB3A94")!))
        items.append(MenuItem(title: "ĐỐI TƯỢNG SỬ DỤNG", type: -1, color : UIColor(hexString: "#EB3A94")!))
        items.append(contentsOf: Utils.getFlowableData(flow: 4))
        
        
        let viewController = MenuChangeVC(nibName: "MenuChangeVC", bundle: nil)
        viewController.base = self
        viewController.items = items
        viewController.tapAction = { index in
            switch index {
            case 0:
                self.moveToPage(index: 1)
            case 1:
                self.moveToPage(index: 2)
            case 2:
                self.moveToPage(index: 3)
            case 3:
                self.moveToPage(index: 4)
            default:
                break
            }
        }
        let menuLeftNavigationController = UISideMenuNavigationController(rootViewController: viewController)
        customSideMenuManager.menuRightNavigationController = menuLeftNavigationController
        customSideMenuManager.menuPresentMode = .menuSlideIn
        customSideMenuManager.menuEnableSwipeGestures = true
        customSideMenuManager.menuFadeStatusBar = false
        customSideMenuManager.menuAnimationOptions = .curveLinear
        customSideMenuManager.menuWidth = 650
        customSideMenuManager.menuShadowColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
    }
    
    private lazy var orderedViewControllers: [BaseDetailVC] = {
        [
            EnsureVitalPage1VC(nibName: "EnsureVitalPage1VC", bundle: nil),
            getImageVC(name: "ensure_vital_page_2"),
            getImageVC(name: "ensure_vital_page_3"),
            EnsureVitalPage4VC(nibName: "EnsureVitalPage4VC", bundle: nil),
            EnsureVitalPage5VC(nibName: "EnsureVitalPage5VC", bundle: nil),
            getImageVC(name: "ensure_vital_page_6")
        ]
    }()
    
    func getImageVC(name : String) -> BaseDetailVC {
        let vc = PageImageVC(nibName: "PageImageVC", bundle: nil)
        vc.imgName = name
        return vc
    }
    
    func moveToPage(index: Int) {
        scrollToPage(.at(index: index), animated: true)
    }
    
    func setTransition(_ trans: PageboyViewController.Transition) {
        transition = trans
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupMenu()
        initEndcallDialog()
        dataSource = self
        setupPageID()
    }
    
    private var vc : ConfirmDialogVC?
    func initEndcallDialog(){
        let containerAppearance = PopupDialogContainerView.appearance()
        containerAppearance.cornerRadius = 15
        vc = ConfirmDialogVC(nibName: "ConfirmDialogVC", bundle: nil)
        vc?.negativeText = "Quay lại"
        vc?.positiveText = "Kết thúc"
        vc?.messenge = "Bạn có chắc chắn muốn kết thúc trò chuyện ?"
        vc?.callbackPositive = {vc in
            self.pauseTimer()
            self.updateEndTime()
            self.present(SurveyVC(nibName: "SurveyVC", bundle: nil), animated: true, completion: nil)
        }
        vc?.callbackNegative = {vc in
        }
    }
    
    private var timer = Timer()
    @objc func actionTimer(){
        print(orderedViewControllers[currentIndex!].pageID)
        durationRecord?.durations[orderedViewControllers[currentIndex!].pageID] = (durationRecord?.durations[orderedViewControllers[currentIndex!].pageID])! + 1
        if let record = durationRecord {
            Utils.saveLastDuration(duration: record)
        }
    }
    
    func startTimer(){
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(actionTimer), userInfo: nil, repeats: true)
    }
    
    func pauseTimer(){
        timer.invalidate()
    }
    
    private var durationRecord : DurationRecord?
    private var currentPageIndex = 0
    
    func setupPageID(){
        orderedViewControllers[0].pageID = "page61"
        orderedViewControllers[1].pageID = "page62"
        orderedViewControllers[2].pageID = "page63"
        orderedViewControllers[3].pageID = "page64"
        orderedViewControllers[4].pageID = "page65"
        orderedViewControllers[5].pageID = "page66"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        view.gestureRecognizers?.removeAll()
        durationRecord = Utils.getLastDuration()
        print(durationRecord?.toJsonString())
        pauseTimer()
        startTimer()
    }
    
    func updateEndTime(){
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
        let endTime = formatter.string(from: Date())
        durationRecord?.endtime = endTime
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

    
}

extension EnsureVitalPage0VC : PageboyViewControllerDataSource {
    
    func numberOfViewControllers(in pageboyViewController: PageboyViewController) -> Int {
        return orderedViewControllers.count
    }
    
    func viewController(for pageboyViewController: PageboyViewController, at index: PageboyViewController.PageIndex) -> UIViewController? {
        return orderedViewControllers[index]
    }
    
    func defaultPage(for pageboyViewController: PageboyViewController) -> PageboyViewController.Page? {
        return nil
    }
    
}
