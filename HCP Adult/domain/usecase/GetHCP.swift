//
//  GetHCP.swift
//  HCP Adult
//
//  Created by Bình Phạm on 6/6/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import Foundation
class GetHCP {
    func params(_ id : String) -> GetHCPRequestModel{
        let getHCPRequest = GetHCPRequestModel(JSONString: "{}")!
        getHCPRequest.userid = id
        return getHCPRequest
    }
    
    func postHCP(id : String, callBack:@escaping (Bool, HCPListModel) -> Void){   
        NetworkService.post(endPoint: API.downloadHCP, requestBody: params(id).toJSON()) { success,response in
            if let data = HCPListModel(JSONString: response) {
                callBack(success, data)
            } else {
                callBack(false, HCPListModel(JSONString: "{}")!)
            }
        }
    }
}
