//
//  DetailView.swift
//  EnsureHMP
//
//  Created by Bình Phạm on 2/22/18.
//

import Foundation

protocol DetailView {
    
    func loadImages()
    
    func resetAnimation()
    
    func startAnimation()
    
}
