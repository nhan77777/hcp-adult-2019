 //
 //  MenuVC.swift
 //  Glucerna
 //
 //  Created by Long Quách Phi on 3/26/18.
 //  Copyright © 2018 Fractal. All rights reserved.
 //
 
 import UIKit
 
 class MenuVC: UIViewController, DetailView{
    
    @IBOutlet weak var contraintHeightTyp2: NSLayoutConstraint!
    @IBOutlet var btnDetail: [UIButton]!
    public var detailViewDelagete : PageView?
    var isHidenTyp2 = false
    public var targetFlowVC : UIViewController?
    public var targetFlowName : String?
    public var targetFlow : Int?
    @IBOutlet var changeFlowButton: [UIButton]!
    
    @IBAction func abtnSection3(_ sender: Any) {
        let vcTarget = Page8VC(nibName: "Page8VC", bundle: nil) as Page8VC
        vcTarget.isHiden = false
        showSubSlide([vcTarget], detailViewDelagete!)
    }
    
    @IBAction func onGotoEnsureFlow(_ sender: Any) {
        let button = sender as! UIButton
        detailViewDelagete?.pauseTimer()
        switch button.tag {
        case 0:
            targetFlowVC =  EnsureHomeVC.instantiate(appStoryboard: .Main)
            (targetFlowVC as! EnsureHomeVC).flow = .MAIN
            break
        case 1 :
            targetFlowVC =  ProsureHomeVC.instantiate(appStoryboard: .Main)
            break
        default:
            break
        }
        
        present(targetFlowVC!, animated: true, completion: nil)
    }
    
    func loadImages() {
    }
    
    func resetAnimation() {
    }
    
    func startAnimation() {
    }
    
    func setupAction(){
        for i in 0...btnDetail.count - 1{
            btnDetail[i].addTarget(self, action: #selector(btnAction), for: .touchUpInside)
            btnDetail[i].tag = i
        }
    }
    func setupFlowChange() {
        
        for i in 0...changeFlowButton.count - 1{
            changeFlowButton[i].tag = i
            
            switch i{
            case 0:
                targetFlowName = "Chuyển đến Ensure Gold"
                 changeFlowButton[i].backgroundColor = UIColor.orange
                break
            case 1 :
                targetFlowName = "Chuyển đến Prosure"
                changeFlowButton[i].backgroundColor = UIColor(hexString: "#8d65a5")
                break
            default:
                targetFlowName = "Untarget"
                break
            }
            
            changeFlowButton[i].setTitle(targetFlowName!, for: .normal)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupAction()
        setupFlowChange()
        loadImages()
        navigationController?.navigationBar.isHidden = true
    }
    
    @objc func btnAction(_ sender : UIButton){
        dismiss(animated: true, completion: nil)
        let index = sender.tag
        if isHidenTyp2{
            switch index{
            case 4:
                detailViewDelagete?.moveToPage(index:   1)
            case 5:
                detailViewDelagete?.moveToPage(index:   2)
            case 6:
                detailViewDelagete?.moveToPage(index:   4)
            case 7:
                detailViewDelagete?.moveToPage(index:   6)
            case 8:
                detailViewDelagete?.moveToPage(index:   10)
            case 9:
                detailViewDelagete?.moveToPage(index:   11)
            default:
                detailViewDelagete?.moveToPage(index: 0)
            }
        }
        else{
            switch index{
            case 0:
                detailViewDelagete?.moveToPage(index:   1)
            case 1:
                detailViewDelagete?.moveToPage(index:   2)
            case 2:
                detailViewDelagete?.moveToPage(index:   3)
            case 3:
                detailViewDelagete?.moveToPage(index:   4)
            case 4:
                detailViewDelagete?.moveToPage(index:   6)
            case 5:
                detailViewDelagete?.moveToPage(index:   7)
            case 6:
                detailViewDelagete?.moveToPage(index:   9)
            case 7:
                detailViewDelagete?.moveToPage(index:   11)
            case 8:
                detailViewDelagete?.moveToPage(index:   15)
            case 9:
                detailViewDelagete?.moveToPage(index:   16)
            default:
                detailViewDelagete?.moveToPage(index: 0)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        resetAnimation()
        if isHidenTyp2 {
            contraintHeightTyp2.constant = 0
        }
        else{
            contraintHeightTyp2.constant = 167
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        startAnimation()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
 }
