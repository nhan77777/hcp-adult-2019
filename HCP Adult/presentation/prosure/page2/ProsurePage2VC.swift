//
//  ProsurePage2VC.swift
//  HCP Adult
//
//  Created by Bình Phạm on 7/25/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import UIKit
import Async

class ProsurePage2VC : BaseDetailVC, DetailView {
    
    @IBOutlet weak var imgBg: UIImageView!
    @IBOutlet var imgIcons: [UIImageView]!
    
    func loadImages() {
        loadResourceToImage("pro_bg_page_2", imgBg)
        for i in 0...1 {
            loadResourceToImage("pro_ic_page_2_\(i)", imgIcons[i])
        }
    }
    
    func resetAnimation() {
        for ic in imgIcons {
            ic.layer.removeAllAnimations()
            ic.stopGlowing()
        }
    }
    
    func startAnimation() {
        for ic in imgIcons {
            ic.startGlowing()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadImages()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Async.main(after: 0.1, {
            self.startAnimation()
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
