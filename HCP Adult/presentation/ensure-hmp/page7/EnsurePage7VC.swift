//
//  EnsurePage7VC.swift
//  HCP Adult
//
//  Created by Le Nhan on 1/7/19.
//  Copyright © 2019 Bình Phạm. All rights reserved.
//

import UIKit
import Async

class EnsurePage7VC: BaseDetailVC, DetailView {
    @IBOutlet weak var imgIcon: UIImageView!
    
    @IBOutlet weak var imgBg: UIImageView!
    
    func loadImages() {
        loadResourceToImage("ensure_bg_page_7", imgBg)
    }
    
    func resetAnimation() {
        imgIcon.layer.removeAllAnimations()
    }
    
    func startAnimation() {
        imgIcon.twinkle()
        boucingViewForever(imgIcon, 1.15)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadImages()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        resetAnimation()
    }
    
    @IBAction func ongImageTap(_ sender: UIButton) {
        
        let vc = ImagePopupViewerVC.init(nibName: "ImagePopupViewerVC", bundle: nil)
        vc.modalPresentationStyle = .overCurrentContext
        
        switch sender.tag {
        case 1:
            vc.image = UIImage(named: "ic_ensure_image1_page7")
        case 2:
            vc.image = UIImage(named: "ic_ensure_image2_page7")
        case 3:
            vc.image = UIImage(named: "ic_ensure_image3_page7")
        default:
            break
        }
        present(vc, animated: true, completion: nil)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Async.main(after: 0.1, {
            self.startAnimation()
        })
    }
}
