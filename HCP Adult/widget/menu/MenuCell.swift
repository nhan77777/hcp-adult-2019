//
//  MenuCell.swift
//  Pediatric
//
//  Created by Bình Phạm on 9/4/18.
//  Copyright © 2018 Fractal. All rights reserved.
//

import UIKit

class MenuCell: UICollectionViewCell {
    var tapAction : (()->())?
    @IBAction func onItemTap(_ sender: Any) {
        tapAction?()
    }
    @IBOutlet weak var btnItem: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
