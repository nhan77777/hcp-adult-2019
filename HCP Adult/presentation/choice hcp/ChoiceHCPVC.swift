//
//  ChoiceHCPVC.swift
//  HCP Adult
//
//  Created by Long Quách Phi on 6/1/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import UIKit
import Async
import PopupDialog
import SearchTextField
import EVReflection
import SwiftSpinner
import CoreLocation

class ChoiceHCPVC: UIViewController, UIPopoverPresentationControllerDelegate {
    
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var searchHCP: SearchTextField!
    @IBOutlet weak var searchHos: SearchTextField!
    @IBOutlet var lbHCPInfos: [UILabel]!
    @IBOutlet var viewHCPInfos: [HCPInfoView]!
    @IBOutlet weak var btnSendDuration: UIButton!
    @IBOutlet weak var btnReloadHCP: HCPInfoView!
    
    private var isHospitalSelected = false {
        didSet {
            if searchHos.text != nil && searchHos.text != "" {
                searchHCP.isEnabled = true
            } else {
                clearHCPInfo()
                searchHCP.isEnabled = false
            }
        }
    }
    
    fileprivate var hcpList = [HCPModel]()
    let userData : UserData = UserData(JSONString: Utils.getFromCache(key: CacheKey.userdata)!)!
    var selectedHCP : HCPModel? = HCPModel(JSONString: "{}")!
    var selectedHosp = ""
    var glucerna : ConfirmDialogVC?
    var ensure : ConfirmDialogVC?
    var prosure : ConfirmDialogVC?
    var hcpListDynamic = [HCPModel]()
    fileprivate let presenter = ChoiceHCPPresenter(getHcp: GetHCP(), durationService: PushDuration(), hcpSurveyService: PushFeedback(), checkVersionService: CheckAppVersion())
    private var durationsRecords : [DurationRecord] = []
    private var hcpSurveysRecords : [SurveyRecord] = []
    var completeSendDuration = 0
    var totalDurationRecord = 0
    var locationManager = CLLocationManager()
    var userLocation = CLLocationCoordinate2D(latitude: 0, longitude: 0)
    
    @IBAction func onHospitalEditing(_ sender: Any) {
        selectedHCP = nil
        clearHCP()
    }
    
    @IBAction func onHospitalEdited(_ sender: SearchTextField) {
        sender.text = selectedHosp
        isHospitalSelected = true
    }
    @IBAction func onHCPEditing(_ sender: Any) {
        selectedHCP = nil
        clearHCPInfo()
    }
    
    @IBAction func onDurationButtonTap(_ sender: Any) {
        if (durationsRecords.count>0) || (hcpSurveysRecords.count>0){
            showLoading("record duration uploaded : \(completeSendDuration)/\(durationsRecords.count + hcpSurveysRecords.count)")
            if durationsRecords.count > 0 {
                presenter.pushDuration(durationRequestModel: prepareDurationRequest(durationsRecords[durationsRecords.count-1])!)
                return
            }
            if hcpSurveysRecords.count > 0 {
                presenter.pushFeedback(hcpSurvey: prepareSurveyRequest(hcpSurveysRecords.last!))
                return
            }
        }
    }
    
    func showDialogWith(message: String){
        let vc = MessagerDialogVC(nibName: "MessagerDialogVC", bundle: nil)
        vc.messenge = message
        let popup = PopupDialog(viewController: vc, buttonAlignment: .vertical, transitionStyle: .bounceUp, preferredWidth: 450, tapGestureDismissal: true, hideStatusBar: false, completion: nil)
        present(popup, animated: true, completion: nil)
    }
    
    func prepareDurationRequest(_ durationRecord : DurationRecord) -> Dictionary<String, Any>?{
        var durationRequest : Dictionary<String, Any>?
        durationRequest = durationRecord.durations
        durationRequest!["clientid"] = durationRecord.clientid
        durationRequest!["appid"] = durationRecord.appid
        durationRequest!["appver"] = durationRecord.appver
        durationRequest!["usersysid"] = durationRecord.usersysid
        durationRequest!["starttime"] = durationRecord.starttime
        durationRequest!["endtime"] = durationRecord.endtime
        durationRequest!["usersysid"] = durationRecord.usersysid
        durationRequest!["hcpid"] = durationRecord.hcpid
        return durationRequest
    }
    
    func prepareSurveyRequest(_ record : SurveyRecord) -> HCPSurvey {
        let request = HCPSurvey(JSONString: "{}")
        request?.appid = record.appid
        request?.clientid = record.clientid
        request?.hcpid = record.hcpid
        request?.usersysid = record.usersysid
        request?.appver = record.appver
        request?.createAt = record.createAt
        request?.feedback = [Feedback](JSONString: (record.feedback?.toJsonString())!)
        request?.consent = record.consent
        request?.signature = "empty_image"
        //        print(record.signatureUrl)
        if let path = record.signatureUrl {
            
            let fileURL = URL(fileURLWithPath: path)
            var imageData : Data?
            do {
                imageData = try Data(contentsOf: fileURL)
            } catch {
                print(error.localizedDescription)
            }
            if let dataOfImage = imageData{
                let image = UIImage(data: dataOfImage)
                request?.signature = Utils.exportSignToBase64(image!)
            }
        }
        return request!
    }
    
    
    @IBAction func abtnOK(_ sender: Any) {
        if isLocationServiceEnabled(){
            if let dept = selectedHCP?.dept{
                print(dept)
                Utils.saveToCache(key: CacheKey.selectedHCP, value: (selectedHCP?.toJSONString()!)!)
                if (FlowCase.getEnsureFlow(dept: dept) == .ICU){
                    Utils.saveFlowableData(arr: [4, 0])
                    glucerna?.messenge = "GỢI Ý NỘI DUNG\n Thứ tự hiển thị nội dung Vital - Ensure Gold?"
                    glucerna?.callbackPositive = {vc in
                        self.createNewRecord()
                        
                        let vcTarget = EnsureHomeVC.instantiate(appStoryboard: .Main)
                        vcTarget.flow = .U
                        self.present(vcTarget, animated: true, completion: nil)
                    }
                    
                    glucerna?.callbackNegative = {vc in
                        self.createNewRecord()
                        let vcTarget =  EnsureHomeVC.instantiate(appStoryboard: .Main)
                        vcTarget.flow = .I3
                        self.present(vcTarget, animated: true, completion: nil)
                    }
                    
                    let glucernaConfirmDialog = PopupDialog(viewController: glucerna!, buttonAlignment: .vertical, transitionStyle: .bounceUp, preferredWidth: 600, tapGestureDismissal: true, hideStatusBar: false, completion: nil)
                    present(glucernaConfirmDialog, animated: true, completion: nil)
                } else if (FlowCase.getEnsureFlow(dept: dept) == .NOI_TIEU_HOA) {
                    Utils.saveFlowableData(arr: [4, 0])
                    glucerna?.messenge = "GỢI Ý NỘI DUNG\n Thứ tự hiển thị nội dung Vital - Ensure Gold?"
                    glucerna?.callbackPositive = {vc in
                        self.createNewRecord()
                        
                        let vcTarget = EnsureHomeVC.instantiate(appStoryboard: .Main)
                        vcTarget.flow = .I1
                        self.present(vcTarget, animated: true, completion: nil)
                    }
                    
                    glucerna?.callbackNegative = {vc in
                        self.createNewRecord()
                        let vcTarget =  EnsureHomeVC.instantiate(appStoryboard: .Main)
                        vcTarget.flow = .I3
                        self.present(vcTarget, animated: true, completion: nil)
                    }
                    
                    let glucernaConfirmDialog = PopupDialog(viewController: glucerna!, buttonAlignment: .vertical, transitionStyle: .bounceUp, preferredWidth: 600, tapGestureDismissal: true, hideStatusBar: false, completion: nil)
                    present(glucernaConfirmDialog, animated: true, completion: nil)
                } else if (FlowCase.getEnsureFlow(dept: dept) == .NGOAI_TIEU_HOA) {
                    Utils.saveFlowableData(arr: [4, 0])
                    glucerna?.messenge = "GỢI Ý NỘI DUNG\n Thứ tự hiển thị nội dung Vital - Ensure Gold?"
                    glucerna?.callbackPositive = {vc in
                        self.createNewRecord()
                        
                        let vcTarget = EnsureHomeVC.instantiate(appStoryboard: .Main)
                        vcTarget.flow = .S1
                        self.present(vcTarget, animated: true, completion: nil)
                    }
                    
                    glucerna?.callbackNegative = {vc in
                        self.createNewRecord()
                        let vcTarget =  EnsureHomeVC.instantiate(appStoryboard: .Main)
                        vcTarget.flow = .I3
                        self.present(vcTarget, animated: true, completion: nil)
                    }
                    
                    let glucernaConfirmDialog = PopupDialog(viewController: glucerna!, buttonAlignment: .vertical, transitionStyle: .bounceUp, preferredWidth: 600, tapGestureDismissal: true, hideStatusBar: false, completion: nil)
                    present(glucernaConfirmDialog, animated: true, completion: nil)
                    
                } else if (FlowCase.getEnsureFlow(dept: dept) == .TIM_MACH_HO_HAP){
                    Utils.saveFlowableData(arr: [3, 1])
                    glucerna?.messenge = "GỢI Ý NỘI DUNG\n Thứ tự hiển thị nội dung Ensure Plus - Glucerna?"
                    glucerna?.callbackPositive = {vc in
                        self.createNewRecord()
                        let vcTarget = EnsureHomeVC.instantiate(appStoryboard: .Main)
                        vcTarget.flow = .I2
                        self.present(vcTarget, animated: true, completion: nil)
                    }
                    glucerna?.callbackNegative = {vc in
                        self.createNewRecord()
                        let vcTarget = Page0VC.instantiate(appStoryboard: .Main)
                        vcTarget.isHidenTyp2 = false
                        self.present(vcTarget, animated: true, completion: nil)
                    }
                    
                    let glucernaConfirmDialog = PopupDialog(viewController: glucerna!, buttonAlignment: .vertical, transitionStyle: .bounceUp, preferredWidth: 600, tapGestureDismissal: true, hideStatusBar: false, completion: nil)
                    present(glucernaConfirmDialog, animated: true, completion: nil)
                    
                } else if (FlowCase.getEnsureFlow(dept: dept) == .NGOAI_CHAN_THUONG_CHINH_HINH){
                    Utils.saveFlowableData(arr: [3, 0])
                    glucerna?.messenge = "GỢI Ý NỘI DUNG\n Thứ tự hiển thị nội dung Ensure Plus - Ensure Gold?"
                    glucerna?.callbackPositive = {vc in
                        self.createNewRecord()
                        let vcTarget = EnsureHomeVC.instantiate(appStoryboard: .Main)
                        vcTarget.flow = .S2
                        self.present(vcTarget, animated: true, completion: nil)
                    }
                    glucerna?.callbackNegative = {vc in
                        self.createNewRecord()
                        let vcTarget = EnsureHomeVC.instantiate(appStoryboard: .Main)
                        vcTarget.flow = .I3
                        self.present(vcTarget, animated: true, completion: nil)
                    }
                    
                    let glucernaConfirmDialog = PopupDialog(viewController: glucerna!, buttonAlignment: .vertical, transitionStyle: .bounceUp, preferredWidth: 600, tapGestureDismissal: true, hideStatusBar: false, completion: nil)
                    present(glucernaConfirmDialog, animated: true, completion: nil)
                    
                } else if (FlowCase.getEnsureFlow(dept: dept) == .CHUYEN_KHOA_SAN){
                    Utils.saveFlowableData(arr: [5, 0])
                    glucerna?.messenge = "GỢI Ý NỘI DUNG\n Thứ tự hiển thị nội dung Glucerna - Ensure Gold?"
                    glucerna?.callbackPositive = {vc in
                        self.createNewRecord()
                        let vcTarget = Page0VC.instantiate(appStoryboard: .Main)
                        vcTarget.pageWhileColor = 6
                        vcTarget.isHidenTyp2 = false
                        self.present(vcTarget, animated: true, completion: nil)
                    }
                    glucerna?.callbackNegative = {vc in
                        self.createNewRecord()
                        let vcTarget = EnsureHomeVC.instantiate(appStoryboard: .Main)
                        vcTarget.flow = .I3
                        self.present(vcTarget, animated: true, completion: nil)
                    }
                    
                    let glucernaConfirmDialog = PopupDialog(viewController: glucerna!, buttonAlignment: .vertical, transitionStyle: .bounceUp, preferredWidth: 600, tapGestureDismissal: true, hideStatusBar: false, completion: nil)
                    present(glucernaConfirmDialog, animated: true, completion: nil)
                } else if (FlowCase.getEnsureFlow(dept: dept) == .NOI_TIET) {
                    Utils.saveFlowableData(arr: [1, 0])
                    glucerna?.messenge = "GỢI Ý NỘI DUNG\n Thứ tự hiển thị nội dung Glucerna - Ensure Gold?"
                    glucerna?.callbackPositive = {vc in
                        self.createNewRecord()
                        let vcTarget = Page0VC.instantiate(appStoryboard: .Main)
                        vcTarget.isHidenTyp2 = false
                        self.present(vcTarget, animated: true, completion: nil)
                    }
                    glucerna?.callbackNegative = {vc in
                        self.createNewRecord()
                        let vcTarget = EnsureHomeVC.instantiate(appStoryboard: .Main)
                        vcTarget.flow = .I3
                        self.present(vcTarget, animated: true, completion: nil)
                    }
                    
                    let glucernaConfirmDialog = PopupDialog(viewController: glucerna!, buttonAlignment: .vertical, transitionStyle: .bounceUp, preferredWidth: 600, tapGestureDismissal: true, hideStatusBar: false, completion: nil)
                    present(glucernaConfirmDialog, animated: true, completion: nil)
                    
                } else if (FlowCase.getEnsureFlow(dept: dept) == .UNG_THU){
                    Utils.saveFlowableData(arr: [2, 0])
                    glucerna?.messenge = "GỢI Ý NỘI DUNG\n Thứ tự hiển thị nội dung Prosure - Ensure Gold?"
                    glucerna?.callbackPositive = {vc in
                        self.createNewRecord()
                        let vcTarget = ProsureHomeVC.instantiate(appStoryboard: .Main)
                        self.present(vcTarget, animated: true, completion: nil)
                    }
                    glucerna?.callbackNegative = {vc in
                        self.createNewRecord()
                        let vcTarget = EnsureHomeVC.instantiate(appStoryboard: .Main)
                        vcTarget.flow = .I3
                        self.present(vcTarget, animated: true, completion: nil)
                    }
                    
                    let glucernaConfirmDialog = PopupDialog(viewController: glucerna!, buttonAlignment: .vertical, transitionStyle: .bounceUp, preferredWidth: 600, tapGestureDismissal: true, hideStatusBar: false, completion: nil)
                    present(glucernaConfirmDialog, animated: true, completion: nil)
                } else if (FlowCase.getEnsureFlow(dept: dept) == .NGOAI_KHOA_KHAC){
                    Utils.saveFlowableData(arr: [0, 1])
                    glucerna?.messenge = "GỢI Ý NỘI DUNG\n Thứ tự hiển thị nội dung Ensure Gold - Glucerna?"
                    glucerna?.callbackPositive = {vc in
                        self.createNewRecord()
                        let vcTarget = EnsureHomeVC.instantiate(appStoryboard: .Main)
                        vcTarget.flow = .S3
                        self.present(vcTarget, animated: true, completion: nil)
                    }
                    glucerna?.callbackNegative = {vc in
                        self.createNewRecord()
                        let vcTarget = Page0VC.instantiate(appStoryboard: .Main)
                        vcTarget.isHidenTyp2 = false
                        self.present(vcTarget, animated: true, completion: nil)
                    }
                    
                    let glucernaConfirmDialog = PopupDialog(viewController: glucerna!, buttonAlignment: .vertical, transitionStyle: .bounceUp, preferredWidth: 600, tapGestureDismissal: true, hideStatusBar: false, completion: nil)
                    present(glucernaConfirmDialog, animated: true, completion: nil)
                } else {
                    
                    Utils.saveFlowableData(arr: [0, 1])
                    glucerna?.messenge = "GỢI Ý NỘI DUNG\n Thứ tự hiển thị nội dung Ensure Gold - Glucerna?"
                    glucerna?.callbackPositive = {vc in
                        self.createNewRecord()
                        let vcTarget = EnsureHomeVC.instantiate(appStoryboard: .Main)
                        vcTarget.flow = .I3
                        self.present(vcTarget, animated: true, completion: nil)
                    }
                    glucerna?.callbackNegative = {vc in
                        self.createNewRecord()
                        let vcTarget = Page0VC.instantiate(appStoryboard: .Main)
                        vcTarget.isHidenTyp2 = false
                        self.present(vcTarget, animated: true, completion: nil)
                    }
                    
                    let glucernaConfirmDialog = PopupDialog(viewController: glucerna!, buttonAlignment: .vertical, transitionStyle: .bounceUp, preferredWidth: 600, tapGestureDismissal: true, hideStatusBar: false, completion: nil)
                    present(glucernaConfirmDialog, animated: true, completion: nil)
                }
            } else {
                showDialogWith(message: "Không có HCP nào được chọn")
            }
        }
        else {
            showMessageIsLocationService()
        }
    }
    
    @IBAction func onMenuTap(_ sender: UIButton) {
        let viewController = MenuOptionalVC(nibName: "MenuOptionalVC", bundle: nil)
        viewController.preferredContentSize = CGSize(width: 482.5, height: 440)
        viewController.base = self
        let navController = UINavigationController(rootViewController: viewController)
        navController.modalPresentationStyle = .popover
        
        if let pctrl = navController.popoverPresentationController {
            pctrl.delegate = self
            pctrl.sourceView = sender
            pctrl.sourceRect = sender.bounds
            self.present(navController, animated: true, completion: nil)
        }
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle{
        return .none
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Utils.clearFromCache(key: CacheKey.selectedHCP)
        Utils.clearFromCache(key: CacheKey.FLOWABLE)
        presenter.attachView(self)
        locationManager.delegate = self
        loadImage()
        setupUI()
        updateBtnSendTitle()
        btnReloadHCP.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onReloadHCPTap)))
        reparingUserLocation()
        presenter.checkVersion(getAppidString(), getVersionString())
    }
    
    @objc func onReloadHCPTap(){
        clearData()
        presenter.doGetHCP(id: userData.userid!)
    }
    
    func reparingUserLocation(){
        locationManager.startUpdatingLocation()
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
    }
    
    func isLocationServiceEnabled() -> Bool{
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted:
                return false
            case .denied:
                return false
            case .authorizedAlways, .authorizedWhenInUse:
                return true
            }
        }
        return false
    }
    
    func showMessageIsLocationService(){
        let vc = MessagerDialogVC(nibName: "MessagerDialogVC", bundle: nil)
        let containerAppearance = PopupDialogContainerView.appearance()
        containerAppearance.cornerRadius = 15
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted:
                vc.messenge = "Không xác định được vị trí người sử dụng"
                vc.callbackPositive = {vc in
                    self.locationManager.requestAlwaysAuthorization()
                    self.locationManager.requestWhenInUseAuthorization()
                }
                let popup = PopupDialog(viewController: vc, buttonAlignment: .vertical, transitionStyle: .bounceUp, preferredWidth: 450, tapGestureDismissal: true, hideStatusBar: false, completion: nil)
                present(popup, animated: true, completion: nil)
            case .denied:
                vc.messenge = "Chưa cấp quyền xác định vị trí, để tiếp tục vui lòng vào Cài đặt → Quyền riêng tư → Dịch vụ định vị → HCPAdult và cấp quyền cho ứng dụng"
                vc.callbackPositive = {vc in
                    self.locationManager.requestAlwaysAuthorization()
                    self.locationManager.requestWhenInUseAuthorization()
                }
                let popup = PopupDialog(viewController: vc, buttonAlignment: .vertical, transitionStyle: .bounceUp, preferredWidth: 450, tapGestureDismissal: true, hideStatusBar: false, completion: nil)
                present(popup, animated: true, completion: nil)
            case .authorizedAlways, .authorizedWhenInUse:
                vc.messenge = "Lấy lại vị trí người dùng thành công, ấn đồng ý để tiếp tục"
                let popup = PopupDialog(viewController: vc, buttonAlignment: .vertical, transitionStyle: .bounceUp, preferredWidth: 450, tapGestureDismissal: true, hideStatusBar: false, completion: nil)
                present(popup, animated: true, completion: nil)
            }
        }
        else{
            vc.messenge = "Chưa bật dịch vụ định vị, để tiếp tục vui lòng vào Cài đặt, Quyền riêng tư và bật dịch vụ định vị"
            vc.callbackPositive = {vc in
                self.locationManager.requestAlwaysAuthorization()
                self.locationManager.requestWhenInUseAuthorization()
            }
            let popup = PopupDialog(viewController: vc, buttonAlignment: .vertical, transitionStyle: .bounceUp, preferredWidth: 450, tapGestureDismissal: true, hideStatusBar: false, completion: nil)
            present(popup, animated: true, completion: nil)
        }
        reparingUserLocation()
    }
    
    func createNewRecord(){
        let record = DurationRecord()
        locationManager.delegate = self
        userLocation.latitude = (locationManager.location?.coordinate.latitude)!
        userLocation.longitude = (locationManager.location?.coordinate.longitude)!
        var durations = Dictionary<String,Double>()
        for i in 1...PushDuration.ALL_PAGE_COUNT {
            durations["page\(i)"] = 0.0
        }
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            record.appver = version
        }
        record.appid = Utils.APP_ID
        record.usersysid = selectedHCP?.id!
        record.hospid = "101771"
        record.hcpid = selectedHCP?.id
        record.clientid = ((UIDevice.current.identifierForVendor?.uuidString)!+String(Date().ticks)).replacingOccurrences(of: "-", with: "")
        record.usersysid = userData.sysid!
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
        let startTime = formatter.string(from: Date())
        record.starttime = startTime
        record.endtime = startTime
        record.startTick = Int(Date().timeIntervalSince1970*1000)
        record.durations = durations
        
        record.longitude = "\(userLocation.longitude)"
        record.latitude = "\(userLocation.latitude)"
        var data = Utils.getDurationData()
        data.append(record)
        Utils.saveDurations(durations: data)
    }
    
    func setupUI(){
        for lb in lbHCPInfos {
            setupHCPView(lb)
        }
        setupHCPHeader(viewHCPInfos[0],"Danh sách HCP", UIImage(named: "ic_hcp_name")!)
        setupHCPHeader(viewHCPInfos[1],"Chuyên khoa", UIImage(named: "ic_hcp_spec")!)
        setupHCPHeader(viewHCPInfos[2],"Khoa", UIImage(named: "ic_hcp_dept")!)
        setupHCPHeader(viewHCPInfos[3],"Loại HCP", UIImage(named: "ic_hcp_type")!)
        setupHCPHeader(viewHCPInfos[4],"Số điện thoại", UIImage(named: "ic_hcp_phone")!)
        setupHCPHeader(viewHCPInfos[5],"Email", UIImage(named: "ic_hcp_email")!)
        setupHCPHeader(viewHCPInfos[6], "Bệnh viện", UIImage(named: "ic_hcp_hosp")!)
        
        let gradient:CAGradientLayer = CAGradientLayer()
        gradient.frame.size = lbTitle.frame.size
        gradient.colors = [UIColor(hexString: "#0075bc")?.cgColor,UIColor(hexString: "#1e3f94")?.cgColor]
        gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        lbTitle.layer.addSublayer(gradient)
        
        let containerAppearance = PopupDialogContainerView.appearance()
        containerAppearance.cornerRadius = 15
        
        glucerna = ConfirmDialogVC(nibName: "ConfirmDialogVC", bundle: nil)
        glucerna?.negativeText = "Không"
        glucerna?.positiveText = "Đồng ý"
        glucerna?.messenge = "GỢI Ý NỘI DUNG\n Thứ tự hiển thị nội dung Glucerna - Ensure?"
        glucerna?.callbackPositive = {vc in
            self.createNewRecord()
            let vcTarget = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GlucernaVC")
            self.present(vcTarget, animated: true, completion: nil)
        }
        glucerna?.callbackNegative = {vc in
            self.createNewRecord()
            let vcTarget = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EnsureVC")
            self.present(vcTarget, animated: true, completion: nil)
        }
        
        ensure = ConfirmDialogVC(nibName: "ConfirmDialogVC", bundle: nil)
        ensure?.negativeText = "Không"
        ensure?.positiveText = "Đồng ý"
        ensure?.messenge = "GỢI Ý NỘI DUNG\n Thứ tự hiển thị nội dung Ensure - Glucerna?"
        ensure?.callbackPositive = {vc in
            self.createNewRecord()
            let vcTarget = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EnsureVC")
            self.present(vcTarget, animated: true, completion: nil)
        }
        ensure?.callbackNegative = {vc in
            self.createNewRecord()
            let vcTarget = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GlucernaVC")
            self.present(vcTarget, animated: true, completion: nil)
        }
        
        prosure = ConfirmDialogVC(nibName: "ConfirmDialogVC", bundle: nil)
        prosure?.negativeText = "Không"
        prosure?.positiveText = "Đồng ý"
        prosure?.messenge = "GỢI Ý NỘI DUNG\n Thứ tự hiển thị nội dung Prosure - Ensure?"
        prosure?.callbackPositive = {vc in
            self.createNewRecord()
            let vcTarget = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProsureHomeVC")
            self.present(vcTarget, animated: true, completion: nil)
        }
        prosure?.callbackNegative = {vc in
            self.createNewRecord()
            let vcTarget = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EnsureVC")
            self.present(vcTarget, animated: true, completion: nil)
        }
        
        setupHCPView(searchHCP)
        searchHCP.theme = SearchTextFieldTheme.darkTheme()
        searchHCP.theme.font = UIFont.systemFont(ofSize: 21)
        searchHCP.theme.bgColor = UIColor(hexString: "#006EB4")!
        searchHCP.theme.separatorColor = UIColor.white
        searchHCP.theme.cellHeight = 50
        searchHCP.startVisible = true
        searchHCP.highlightAttributes = [NSAttributedString.Key.foregroundColor: UIColor.yellow]
        searchHCP.itemSelectionHandler = {items, itemPosition in
            self.view.endEditing(true)
            self.setDataRow(index: (items[itemPosition] as! HCPFieldItem).id!)
        }
        
        setupHCPView(searchHos)
        searchHos.theme = SearchTextFieldTheme.darkTheme()
        searchHos.theme.font = UIFont.systemFont(ofSize: 21)
        searchHos.theme.bgColor = UIColor(hexString: "#006EB4")!
        searchHos.theme.separatorColor = UIColor.white
        searchHos.theme.cellHeight = 50
        searchHos.startVisible = true
        searchHos.highlightAttributes = [NSAttributedString.Key.foregroundColor: UIColor.yellow]
        searchHos.itemSelectionHandler = {items, itemPosition in
            self.view.endEditing(true)
            self.selectedHosp = items[itemPosition].title
            self.setDataRowHosName(index: (items[itemPosition] as! HCPFieldItem).id!)
        }
    }
    
    func updateBtnSendTitle(){
        durationsRecords = Utils.getDurationData()
        hcpSurveysRecords = Utils.getSurveyRecords()
        totalDurationRecord = durationsRecords.count + hcpSurveysRecords.count
        if (durationsRecords.count + hcpSurveysRecords.count > 0){
            btnSendDuration.isHidden = false
            btnSendDuration.setTitle("Còn \(durationsRecords.count + hcpSurveysRecords.count) record chưa gửi, nhấn để gửi lên Server", for: .normal)
        }
    }
    
    func setupHCPView(_ lbHCP : UIView){
        lbHCP.layer.borderColor = UIColor(hexString: "#165CA6")?.cgColor
        lbHCP.layer.borderWidth = 2
        lbHCP.layer.cornerRadius = 5
    }
    
    func setupHCPHeader(_ view : HCPInfoView, _ title : String, _ image : UIImage){
        view.contentView.backgroundColor = UIColor(hexString: "#006EB4")
        view.lbTitle.textColor = UIColor.white
        view.lbTitle.text = title
        view.lbTitle.font = UIFont.boldSystemFont(ofSize:20)
        view.imgTitle.image = image
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        clearData()
        isHospitalSelected = false
        presenter.doGetHCP(id: userData.userid!)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    func setDataRow(index: Int){
        if hcpListDynamic.count > 0{
            selectedHCP = hcpListDynamic[index]
            searchHCP.text = hcpListDynamic[index].name
            lbHCPInfos[0].text = hcpListDynamic[index].specialty
            lbHCPInfos[1].text = hcpListDynamic[index].dept
            lbHCPInfos[2].text = hcpListDynamic[index].hcptype
            lbHCPInfos[3].text = hcpListDynamic[index].phone
            lbHCPInfos[4].text = hcpListDynamic[index].email
        }
        else{
            selectedHCP = hcpList[index]
            searchHCP.text = hcpList[index].name
            lbHCPInfos[0].text = hcpList[index].specialty
            lbHCPInfos[1].text = hcpList[index].dept
            lbHCPInfos[2].text = hcpList[index].hcptype
            lbHCPInfos[3].text = hcpList[index].phone
            lbHCPInfos[4].text = hcpList[index].email
        }
    }
    
    func clearData(){
        searchHos.text = ""
        clearHCP()
    }
    
    func clearHCP(){
        searchHCP.text = ""
        clearHCPInfo()
    }
    
    func clearHCPInfo(){
        lbHCPInfos[0].text = ""
        lbHCPInfos[1].text = ""
        lbHCPInfos[2].text = ""
        lbHCPInfos[3].text = ""
        lbHCPInfos[4].text = ""
    }
    
    func setDataRowHosName(index: Int){
        searchHos.text = hcpList[index].hospname
        var items : [SearchTextFieldItem] = []
        
        for i in 0...hcpList.count - 1{
            if (hcpList[i].hospname?.elementsEqual(selectedHosp))!{
                let item = HCPFieldItem(title: hcpList[i].name!, id: i)
                items.append(item)
            }
        }
        if items.count != 0{
            searchHCP.becomeFirstResponder()
            searchHCP.startVisibleWithoutInteraction = true
        }
        clearHCP()
        searchHCP.filterItems(items)
    }
    
    func loadImage(){
    }
    
}

extension ChoiceHCPVC : ChoiceHCPView {
    
    func checkVersionSuccess(_ data: ResponseCheckVersion) {
        if data.appid?.elementsEqual(getAppidString()) ?? false{
            if data.updaterequired?.elementsEqual("1") ?? false{
                showAlertWith(message: data.msg ?? "Cần update"){
                    if let url = URL(string: API.linkInstallr){
                        UIApplication.shared.openURL(url)
                    }
                }
            }
        }
    }
    
    func checkVersionFailed(_ message: String) {}
    
    func pushHCPSurveySuccess(_ data: ReturnDurationModel){
        Utils.removeHCPSurvey(id: data.clientid!)
        hcpSurveysRecords.remove(at: hcpSurveysRecords.count - 1)
        completeSendDuration = completeSendDuration + 1
        btnSendDuration.setTitle("Còn \(durationsRecords.count + hcpSurveysRecords.count) record chưa gửi, nhấn để gửi lên Server", for: .normal)
        showLoading("\(completeSendDuration)/\(totalDurationRecord) record uploaded ...")
        
        if durationsRecords.count>0{
            presenter.pushDuration(durationRequestModel: prepareDurationRequest(durationsRecords.last!)!)
        } else {
            Async.main(after: 1, {
                self.hideLoading()
            })
            btnSendDuration.isHidden = true
        }
    }
    
    func pushHCPSurveyFailed(_ message : String){
        print(message)
        hideLoading()
        //        btnSendDuration.setTitle("Còn \(durationsRecords.count + hcpSurveysRecords.count) record chưa gửi, nhấn để gửi lên Server", for: .normal)
        //        if (durationsRecords.count + hcpSurveysRecords.count)<=0{
        //            btnSendDuration.isHidden = true
        //        }
    }
    
    func pushDurationSuccess(_ data: ReturnDurationModel) {
        Utils.clearDuration(data.clientid!)
        durationsRecords.remove(at: durationsRecords.count-1)
        completeSendDuration = completeSendDuration + 1
        btnSendDuration.setTitle("Còn \(durationsRecords.count + hcpSurveysRecords.count) record chưa gửi, nhấn để gửi lên Server", for: .normal)
        showLoading("\(completeSendDuration)/\(totalDurationRecord) record uploaded ...")
        
        if durationsRecords.count>0{
            presenter.pushDuration(durationRequestModel: prepareDurationRequest(durationsRecords.last!)!)
        } else {
            if hcpSurveysRecords.count>0 {
                presenter.pushFeedback(hcpSurvey: prepareSurveyRequest(hcpSurveysRecords.last!))
            } else {
                Async.main(after: 1, {
                    self.hideLoading()
                })
                btnSendDuration.isHidden = true
            }
        }
    }
    func pushDurationFailed(_ message : String){
        print(message)
        hideLoading()
    }
    
    func showLoading(_ message: String) {
        SwiftSpinner.show(message)
    }
    
    func hideLoading() {
        SwiftSpinner.hide()
    }
    
    func isContain(item : SearchTextFieldItem, in items:[SearchTextFieldItem]) -> Bool{
        for obj in items{
            if obj.title == item.title{
                return true
            }
        }
        return false
    }
    
    func onGetHCP(_ data: [HCPModel]) {
        Utils.saveToCache(key: CacheKey.HCP_DATA_CACHE, value: data.toJSONString()!)
        print(data.toJSONString())
        processData(data)
    }
    
    func processData(_ data : [HCPModel]){
        hcpList = data
        if data.count > 0{
            selectedHCP = hcpList[0]
            var hosName: [SearchTextFieldItem] = []
            var items : [SearchTextFieldItem] = []
            for i in 0...data.count - 1 {
                let item = HCPFieldItem(title: data[i].name!, id: i)
                let itemHosName = HCPFieldItem(title: data[i].hospname!, id: i)
                if  !isContain(item: itemHosName, in: hosName){
                    hosName.append(itemHosName)
                }
                items.append(item)
            }
            searchHCP.filterItems(items)
            searchHos.filterItems(hosName)
            //            self.setDataRow(index: 0)
        }
    }
    
    func onGetHCPFailed(_ message: String) {
        if let json = Utils.getFromCache(key: CacheKey.HCP_DATA_CACHE) {
            let data = [HCPModel](JSONString: json)
            let vc = MessagerDialogVC(nibName: "MessagerDialogVC", bundle: nil)
            vc.messenge = "\(message). Ứng dụng sẽ sử dụng dữ liệu đã lưu trong bộ nhớ tạm"
            vc.callbackPositive = { _ in
                self.processData(data!)
            }
            
            let popup = PopupDialog(viewController: vc, buttonAlignment: .vertical, transitionStyle: .bounceUp, preferredWidth: 450, tapGestureDismissal: true, hideStatusBar: false, completion: nil)
            present(popup, animated: true, completion: nil)
        } else {
            showDialogWith(message: "\(message)")
        }
        
    }
    
}

class HCPFieldItem: SearchTextFieldItem {
    public var id: Int?
    public init(title: String, id: Int?) {
        super.init(title: title)
        self.title = title
        self.id = id
    }
}

extension ChoiceHCPVC: CLLocationManagerDelegate{
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == CLAuthorizationStatus.authorizedAlways || status == CLAuthorizationStatus.authorizedWhenInUse{
            SwiftSpinner.hide()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        userLocation = locValue
    }
    
}



