//
//  ConsentPageVC.swift
//  HCP Adult
//
//  Created by Bình Phạm on 6/1/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import UIKit
import M13Checkbox
import SwiftSpinner
import PopupDialog
import Photos

class ConsentPageVC: UIViewController {
    
    @IBOutlet weak var signView: SignatureView!
    @IBOutlet weak var imgSignHolder: UIImageView!
    @IBOutlet weak var scrollMain: UIScrollView!
    @IBOutlet weak var lbHCPName: UILabel!
    @IBOutlet weak var tfHCPEmail: UITextField!
    @IBOutlet weak var lbHCPTitle: UILabel!
    @IBOutlet weak var tfHCPPhone: UITextField!
    @IBOutlet weak var lbHCPWorkplace: UILabel!
    @IBOutlet weak var lbHCPJob: UILabel!
    @IBOutlet var checkBox: [M13Checkbox]!
    let arrAnswerData = ["E-Mail","Tin nhắn","Điện thoại"]
    public var surveyRecord : SurveyRecord?
    public var hcpSurvey = HCPSurvey(JSONString: "{}")
    fileprivate let presenter = PushFeedbackPresenter(feedbackService: PushFeedback())
    
    @IBAction func onClearTap(_ sender: Any) {
        signView.clear()
        imgSignHolder.isHidden = false
    }
    
    @IBAction func onCancelTap(_ sender: Any) {
        let mh = ChoiceHCPVC(nibName: "ChoiceHCPVC", bundle: nil)
        present(mh, animated: true, completion: nil)
    }
    
    func createAPopupDialogWith(message: String){
        let vc = MessagerDialogVC(nibName: "MessagerDialogVC", bundle: nil)
        vc.messenge = message
        let containerAppearance = PopupDialogContainerView.appearance()
        containerAppearance.cornerRadius = 15
        let popup = PopupDialog(viewController: vc, buttonAlignment: .vertical, transitionStyle: .bounceUp, preferredWidth: 450, tapGestureDismissal: true, hideStatusBar: false, completion: nil)
        present(popup, animated: true, completion: nil)
    }
    
    func checkValueCheckBox() -> Bool{
        if checkBox[0].checkState == .checked{
            if var email = tfHCPEmail.text{
                email = email.replacingOccurrences(of: " ", with: "")
                if email == ""{
                    createAPopupDialogWith(message: "Bạn vui lòng nhập email")
                    return false
                }
            }
        }
        if checkBox[1].checkState == .checked{
            if var phone = tfHCPPhone.text{
                phone = phone.replacingOccurrences(of: " ", with: "")
                if phone == ""{
                    createAPopupDialogWith(message: "Bạn vui lòng nhập số điện thoại")
                    return false
                }
            }
            
        }
        if checkBox[2].checkState == .checked{
            if var phone = tfHCPPhone.text{
                phone = phone.replacingOccurrences(of: " ", with: "")
                if phone == ""{
                    createAPopupDialogWith(message: "Bạn vui lòng nhập số điện thoại")
                    return false
                }
            }
        }
        return true
    }
    
    @IBAction func onSaveTap(_ sender: Any) {
        if signView.doesContainSignature && checkValueCheckBox(){
            let signUrl = Utils.saveImage(image: signView.getSignature()!, fileName: (surveyRecord?.clientid)!)
            var arrAnswers : [String] = []
            for i in 0...checkBox.count - 1{
                if checkBox[i].checkState == .checked{
                    arrAnswers.append(arrAnswerData[i])
                }
            }
            surveyRecord?.consent = arrAnswers
            surveyRecord?.signatureUrl = signUrl
            Utils.saveSurveyRecord((surveyRecord?.clientid)!, surveyRecord!)
            hcpSurvey?.appid = surveyRecord?.appid
            hcpSurvey?.clientid = surveyRecord?.clientid
            hcpSurvey?.hcpid = surveyRecord?.hcpid
            hcpSurvey?.usersysid = surveyRecord?.usersysid
            hcpSurvey?.appver = surveyRecord?.appver
            hcpSurvey?.createAt = surveyRecord?.createAt
            hcpSurvey?.feedback = [Feedback](JSONString: (surveyRecord?.feedback?.toJsonString())!)
            hcpSurvey?.consent = surveyRecord?.consent
            hcpSurvey?.signature = Utils.exportSignToBase64(signView.getSignature()!)
            hcpSurvey?.phone = tfHCPPhone.text
            hcpSurvey?.email = tfHCPEmail.text
            presenter.pushFeedback(hcpSurvey: hcpSurvey!)
        } else {
            createAPopupDialogWith(message: "Bạn vui lòng kí tên")
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        signView.delegate = self
        presenter.attachView(self)
        let selectedHCP = HCPModel(JSONString: Utils.getFromCache(key: CacheKey.selectedHCP)!)
        lbHCPName.text = selectedHCP?.name
        tfHCPEmail.text = selectedHCP?.email
        tfHCPPhone.text = selectedHCP?.phone
        lbHCPTitle.text = selectedHCP?.hcptype
        lbHCPJob.text = selectedHCP?.dept
        PHPhotoLibrary.requestAuthorization({
            (newStatus) in
            if newStatus ==  PHAuthorizationStatus.authorized {}
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}

extension ConsentPageVC : SignatureDelegate {
    
    func didStart() {
        scrollMain.isScrollEnabled = false
        imgSignHolder.isHidden = true
        
    }
    
    func didFinish() {
        scrollMain.isScrollEnabled = true
    }
}

extension ConsentPageVC : PushFeedbackView{
    func pushFeedbackSuccess(_ data: ReturnDurationModel) {
        Utils.removeHCPSurvey(id: data.clientid!)
        let mh = ChoiceHCPVC(nibName: "ChoiceHCPVC", bundle: nil)
        present(mh, animated: true, completion: nil)
    }
    
    func pushFeedbackFailed(_ message: String) {
        print(message)
        let mh = ChoiceHCPVC(nibName: "ChoiceHCPVC", bundle: nil)
        present(mh, animated: true, completion: nil)
    }
    
    func showLoading(_ message: String) {
        SwiftSpinner.show(message)
    }
    
    func hideLoading() {
        SwiftSpinner.hide()
    }
}


