//
//  ViewController.swift
//  HCP Adult
//
//  Created by Long Quách Phi on 7/3/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import UIKit

protocol PushFeedbackView: BaseView {
    func pushFeedbackSuccess(_ data: ReturnDurationModel)
    func pushFeedbackFailed(_ message : String)
}
