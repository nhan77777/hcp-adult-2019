//
//  MenuOptionalVC.swift
//  HCP Adult
//
//  Created by Bình Phạm on 6/6/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import UIKit

private let reuseIdentifier = "MenuCell"

class MenuItem {
    var title : String
    var type : Int
    var color: UIColor
    init(title : String, type : Int, color: UIColor = .black) {
        self.title = title
        self.type = type
        self.color = color
    }
}

class MenuChangeVC: UIViewController {
    
    public var items : [MenuItem] = []
    public var base : PageView?
    public var tapAction : ((Int) -> ())?
    @IBOutlet weak var clvMenu: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        clvMenu.register(UINib.init(nibName: reuseIdentifier,bundle:nil), forCellWithReuseIdentifier: reuseIdentifier)
        clvMenu.dataSource = self
        if let flowLayout = clvMenu?.collectionViewLayout as? UICollectionViewFlowLayout{
            flowLayout.minimumLineSpacing = 2
            flowLayout.minimumInteritemSpacing = 0
            flowLayout.itemSize = CGSize(width: 650, height: 49)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
}

extension MenuChangeVC : UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = clvMenu.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! MenuCell
        cell.layer.addBorder(edge: .bottom, color: .black, thickness: 1)
        if indexPath.row < items.count {
            switch items[indexPath.row].type {
            case 0 :
                cell.btnItem.setTitle("CHUYỂN ĐẾN ENSURE GOLD", for: .normal)
                cell.backgroundColor = UIColor.orange
                cell.btnItem.setTitleColor(UIColor.white, for: .normal)
                cell.tapAction = {() in
                    self.dismiss(animated: true, completion: { () in
                        self.base?.pauseTimer()
                        let vc = EnsureHomeVC.instantiate(appStoryboard: .Main)
                        vc.flow = .MAIN
                        self.base?.onChangeFlow(vc: vc)
                    })
                }
                break
            case 1:
                cell.btnItem.setTitle("CHUYỂN ĐẾN GLUCERNA", for: .normal)
                cell.btnItem.setTitleColor(UIColor.white, for: .normal)
                cell.backgroundColor = UIColor(hexString: "#4f0090")
                cell.tapAction = {() in
                    self.dismiss(animated: true, completion: { () in
                        self.base?.pauseTimer()
                        let vc = Page0VC.instantiate(appStoryboard: .Main)
                        vc.isHidenTyp2 = false
                        self.base?.onChangeFlow(vc: vc)
                    })
                }
            case 2:
                cell.btnItem.setTitle("CHUYỂN ĐẾN PROSURE", for: .normal)
                cell.btnItem.setTitleColor(UIColor.white, for: .normal)
                cell.backgroundColor = UIColor(hexString: "#8d65a5")
                cell.tapAction = {() in
                    self.dismiss(animated: true, completion: { () in
                        self.base?.pauseTimer()
                        self.base?.onChangeFlow(vc: ProsureHomeVC.instantiate(appStoryboard: .Main))
                    })
                }
            case 3:
                cell.btnItem.setTitle("CHUYỂN ĐẾN ENSURE PLUS ADVANCE", for: .normal)
                cell.btnItem.setTitleColor(UIColor.white, for: .normal)
                cell.backgroundColor = UIColor(hexString: "#194275")
                cell.tapAction = {() in
                    self.dismiss(animated: true, completion: { () in
                        self.base?.pauseTimer()
                        self.base?.onChangeFlow(vc: EnsurePlusPage0VC(nibName: "EnsurePlusPage0VC", bundle: nil))
                    })
                }
            case 4:
                cell.btnItem.setTitle("CHUYỂN ĐẾN VITAL", for: .normal)
                cell.btnItem.setTitleColor(UIColor.white, for: .normal)
                cell.backgroundColor = UIColor(hexString: "#d84185")
                cell.tapAction = {() in
                    self.dismiss(animated: true, completion: { () in
                        self.base?.pauseTimer()
                        self.base?.onChangeFlow(vc: EnsureVitalPage0VC(nibName: "EnsureVitalPage0VC", bundle: nil))
                    })
                }
            
            default:
                cell.btnItem.setTitle(items[indexPath.row].title, for: .normal)
                cell.btnItem.setTitleColor(items[indexPath.row].color, for: .normal)
                cell.tapAction = {() in
                    self.dismiss(animated: true, completion: {() in
                        self.tapAction?(indexPath.row)
                    })
                }
                
            }
        } else {
            cell.btnItem.setTitle("KẾT THÚC", for: .normal)
            cell.btnItem.setTitleColor(UIColor.red, for: .normal)
            cell.tapAction = { () in
                self.dismiss(animated: true, completion: {() in
                    self.base?.onRequestEndCall()
                })
                
            }
        }
        return cell
    }
    
    
}
