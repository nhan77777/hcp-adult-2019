//
//  MenuOptionalVC.swift
//  HCP Adult
//
//  Created by Bình Phạm on 6/6/18.
//  Copyright © 2018 Bình Phạm. All rights reserved.
//

import UIKit

class MenuOptionalVC: UIViewController {
    
    @IBOutlet weak var lbAppVersion: UILabel!
    public var base : UIViewController?
    
    @IBAction func onHomeTap(_ sender: Any) {
        if !(self.base?.isKind(of: ChoiceHCPVC.self))! {
            present(ChoiceHCPVC(nibName: "ChoiceHCPVC", bundle: nil), animated: true, completion: nil)
        }
    }
    
    @IBAction func onChangeServerTap(_ sender: UIButton) {
        present(ChangeServerVC(nibName: "ChangeServerVC", bundle: nil), animated: true, completion: nil)
    }
    
    @IBAction func onLogoutTap(_ sender: Any) {
        Utils.saveToCache(key: CacheKey.userdata, value: "")
        let vc = UIStoryboard(name: "Main", bundle: nil) .
            instantiateViewController(withIdentifier: "LoginViewController")
        present(vc, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lbAppVersion.text = "Version \(getVersionString())"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
}
