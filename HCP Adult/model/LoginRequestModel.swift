//
//  LoginRequest.swift
//
//  Created by Long Quách Phi on 5/29/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class LoginRequestModel: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let userid = "userid"
    static let pwd = "pwd"
  }

  // MARK: Properties
  public var userid: String?
  public var pwd: String?
  

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    userid <- map[SerializationKeys.userid]
    pwd <- map[SerializationKeys.pwd]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = userid { dictionary[SerializationKeys.userid] = value }
    if let value = pwd { dictionary[SerializationKeys.pwd] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.userid = aDecoder.decodeObject(forKey: SerializationKeys.userid) as? String
    self.pwd = aDecoder.decodeObject(forKey: SerializationKeys.pwd) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(userid, forKey: SerializationKeys.userid)
    aCoder.encode(pwd, forKey: SerializationKeys.pwd)
  }

}
